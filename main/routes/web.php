<?php
use Illuminate\Http\Request;
use App\User;
use App\AgentCustomer;
use App\Receiver;
use App\Transaction;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/  
 

Route::get('/payment/{type}/{id}/', ['as'=>'payments.pay', 'uses'=>'PaymentsController@pay']);
Route::get('/payment/part/user/{id}/{amount}', ['as'=>'payments.part_pay', 'uses'=>'PaymentsController@part_pay']);
Route::post('/paypal/payments', ['as'=>'payments.paypal', 'uses'=>'PaymentsController@paypal_store']);
Route::post('/paypal/strip', ['as'=>'payments.strip', 'uses'=>'PaymentsController@stripe_store']);
Route::get('status', 'PayPalController@getPaymentStatus');


Route::group(['middleware' =>'isLogin'], function(){

    Route::get('/message/{type}/{id}/validate_mobile', ['as'=>'message.validate_mobile', 'uses'=>'MessageController@validate_mobile']);
    
    Route::post('/mobile/confirm_code',  'MessageController@confirm_code');
 
     Route::get('/',['as'=>'members.home','uses'=>'UsersController@home']);
     
     Route::get('/home',['as'=>'members.home','uses'=>'UsersController@home']);

     Route::get('/currency/sender/{id}',['as'=>'currencies.sender','uses'=>'CurrenciesController@sender']);
       
 /*
    |--------------------------------------------------------------------------
    | Admin
    |--------------------------------------------------------------------------
     
            */
         Route::group(['middleware' =>'admin'], function(){
             /*
              |--------------------------------------------------------------------------
              | Payments 
              |--------------------------------------------------------------------------
          */
            Route::resource('payments', 'PaymentsController');
            Route::get('/{type}/user/payment', ['as'=>'payments.user_type', 'uses'=>'PaymentsController@user_type']);
            Route::post('/post_search/payment', ['as'=>'payments.post_search', 'uses'=>'PaymentsController@post_search']);
            /*
              |--------------------------------------------------------------------------
              | Subscription 
              |--------------------------------------------------------------------------
          */
          Route::resource('subscription', 'SubscriptionController');

                Route::resource('admin', 'AdminController');
                  /*
                    |--------------------------------------------------------------------------
                    | Subscription 
                    |--------------------------------------------------------------------------
                */
                Route::resource('subscription', 'SubscriptionController');

                /*
                    |--------------------------------------------------------------------------
                    | Rate 
                    |--------------------------------------------------------------------------
                */
                Route::resource('rate', 'RateController');

                   /*
                    |--------------------------------------------------------------------------
                    | Currencies 
                    |--------------------------------------------------------------------------
                */
                Route::resource('currencies', 'CurrenciesController');
                
                /*
                    |--------------------------------------------------------------------------
                    | Commission 
                    |--------------------------------------------------------------------------
                */
                Route::resource('commission', 'CommissionController');
                /*
                    |--------------------------------------------------------------------------
                    | Bank 
                    |--------------------------------------------------------------------------
                */
                Route::resource('bank', 'BankController');
                /*
                    |--------------------------------------------------------------------------
                    | Business Setup
                    |--------------------------------------------------------------------------
                */

                Route::resource('setup-business', 'SetupBusinessController');
                /*
                    |--------------------------------------------------------------------------
                    | Transaction Setup
                    |--------------------------------------------------------------------------
                */

                Route::resource('setup-transaction', 'SetupTransactionController');

        /*
                    |--------------------------------------------------------------------------
                    | Message
                    |--------------------------------------------------------------------------
         */

        Route::resource('message', 'MessageController');
        Route::get('/user/{id}/message', ['as'=>'message.user', 'uses'=>'MessageController@index']);

                
        /*
            |--------------------------------------------------------------------------
            | Payment 
            |--------------------------------------------------------------------------
         */
            
        Route::resource('partpayment', 'PartPaymentController');
        Route::get('/partpayment/{id}/agent/transactions', ['as'=>'partpayment.transactions', 'uses'=>'PartPaymentController@transactions']);
        Route::get('/partpayment/{id}/agent/commission', ['as'=>'partpayment.commissions', 'uses'=>'PartPaymentController@commissions']);
        Route::post('/post_search/partpayment', ['as'=>'partpayment.post_search', 'uses'=>'PartPaymentController@post_search']);
       
        
        /*
            |--------------------------------------------------------------------------
            |Managers Payment 
            |--------------------------------------------------------------------------
         */
        Route::resource('managerpayments', 'ManagerPaymentsController');
        
        /*
            |--------------------------------------------------------------------------
            | Outstanding 
            |--------------------------------------------------------------------------
         */
            
        Route::resource('outstandings', 'OutstandingController');
        Route::post('/outstandings/commission/store', ['as'=>'outstandings.store_commissions', 'uses'=>'OutstandingController@store_commissions']);
        Route::match(['put', 'patch'], '/commission/store/{id}','OutstandingController@update_commissions');
        Route::get('/outstandings/{id}/agent/modify/{type}', ['as'=>'outstandings.modify', 'uses'=>'OutstandingController@status']);  
        Route::get('/outstandings/{id}/agent/paid/{type}', ['as'=>'outstandings.paid', 'uses'=>'OutstandingController@paid']);    
    });
            Route::get('/outstandings/agent/{id}/transactions/{status}', ['as'=>'outstandings.transactions', 'uses'=>'OutstandingController@transactions']);
            Route::get('/outstandings/agent/{id}/commission/{status}', ['as'=>'outstandings.commissions', 'uses'=>'OutstandingController@commissions']);

        /* Admin Middleware ends here
            |--------------------------------------------------------------------------
            | Members/Users 
            |--------------------------------------------------------------------------
         */
        Route::resource('members', 'UsersController');
        Route::get('/member/agent', ['as'=>'members.agent', 'uses'=>'UsersController@agent']);
        Route::get('/member/customer',['as'=>'members.customer','uses'=>'UsersController@customer']);
        Route::get('/member/search/{id}',['as'=>'members.search','uses'=>'UsersController@search']);
        Route::get('/member/password/edit/{id}',['as'=>'members.password','uses'=>'UsersController@password_edit']);
        Route::match(['put', 'patch'], '/password/modify/{id}','UsersController@passwordUpdate');
        Route::get('/member/{id}/status', ['as'=>'member.status', 'uses'=>'UsersController@status']);
        Route::match(['put', 'patch'], '/member/updatestatus/{id}','UsersController@updatestatus');
        
        /*
            |--------------------------------------------------------------------------
            | Home index 
            |--------------------------------------------------------------------------
         */
        
  
        /*
            |--------------------------------------------------------------------------
            | Agent 
            |--------------------------------------------------------------------------
         */

        Route::group(['middleware' =>['adminWithAgent']], function(){
        			Route::prefix('agent/')->group(function () {
        		    	Route::resource('senders', 'AgentCustomerController');
                        Route::get('/customers/{id}/list', ['as'=>'customer.list', 'uses'=>'AgentCustomerController@index']);
                        Route::get('/senders/autocomplete/{id}/{value}', ['as'=>'senders.autocomplete', 'uses'=>'AgentCustomerController@autocomplete']);
        		    	Route::resource('customer-transactions', 'AgentTransactionsController');
        		});

        });


        
       


        /*
            |--------------------------------------------------------------------------
            | Customer 
            |--------------------------------------------------------------------------
         */

        // Route::group(['middleware' =>'customer'], function(){
        // 			Route::prefix('customer/')->group(function () {
        // 		    	Route::resource('beneficiaries', 'CustomerReceiversController');
        // 		});

        // });

  /*
   |--------------------------------------------------------------------------
   | Receivers -- Agent & Customer 
   |--------------------------------------------------------------------------
 */
 // Route::prefix('agent/customer')->group(function () {
 //          Route::resource('receivers', 'AgentCustomerReceiversController');
 //         });

    Route::resource('receivers', 'ReceiversController');
            
        	Route::get('/sender/{id}/receivers', ['as'=>'receivers.sender', 'uses'=>'ReceiversController@index']);
        	Route::get('/customer/{id}/receivers', ['as'=>'receivers.customer', 'uses'=>'ReceiversController@index']);
            Route::get('/sender/{id}/create/receiver', ['as'=>'receivers.new', 'uses'=>'ReceiversController@create']);
            Route::get('{type}/{id}/receivers/autocomplete/{req}', ['as'=>'receivers.autocomplete', 'uses'=>'ReceiversController@autocomplete']);
            
        	

        


        /*
            |--------------------------------------------------------------------------
            | Transactions
            |--------------------------------------------------------------------------
         */

      Route::resource('transactions', 'TransactionsController');
      Route::get('/transaction/user/{id}', ['as'=>'transactions.user', 'uses'=>'TransactionsController@index']);
         Route::get('/transaction/agent', ['as'=>'transactions.agent', 'uses'=>'TransactionsController@agentsTransactions']);
          Route::get('/transaction/customer', ['as'=>'transactions.customer', 'uses'=>'TransactionsController@customersTransactions']);
          Route::get('/transaction/{id}/status', ['as'=>'transactions.status', 'uses'=>'TransactionsController@status']);
          
          Route::get('/transaction/search/{type}/{value}', ['as'=>'transactions.search', 'uses'=>'TransactionsController@search']);
          Route::get('/transaction/search/{value}', ['as'=>'transactions.autocompletetips', 'uses'=>'TransactionsController@autocompleteTips']);
          Route::match(['get', 'post'],'/transaction/post_search', ['as'=>'transactions.post_search', 'uses'=>'TransactionsController@post_search']);

          Route::get('/transaction/sender/{sender_id}/receiver/{receiver_id}/create', 
              ['as'=>'transaction.create', 'uses'=>'TransactionsController@create']);
        Route::get('/transaction/{id}/receipt', ['as'=>'transactions.receipt', 'uses'=>'TransactionsController@receipt']);
          
        Route::get('/transactions/{id}/turnover', ['as'=>'transactions.turnover', 'uses'=>'TransactionsController@turnover']);
        Route::get('/transactions/{id}/commission', ['as'=>'transactions.commission', 'uses'=>'TransactionsController@commission']);
          //custom update route
          Route::match(['put', 'patch'], '/transaction/updatestatus/{id}','TransactionsController@updatestatus');
          Route::post('/transactions/summary', ['as'=>'transactions.summary', 'uses'=>'TransactionsController@summary']);
         
         


        /*
            |--------------------------------------------------------------------------
            |                       N2UK  
            |--------------------------------------------------------------------------
         */
            /* Bank 
            |-------------------------------------
         */
        Route::resource('banks_n2uk', 'N2UK\BanksController');
        /*
          /* Rate 
            |-------------------------------------
         */
        Route::resource('rates_n2uk', 'N2UK\RatesController');

         /* customers 
            |-------------------------------------
         */
        Route::resource('customers_n2uk', 'N2UK\CustomersController');
        Route::get('/customers_n2uk/{id}/customers', ['as'=>'customers_n2uk.search', 'uses'=>'N2UK\CustomersController@customerReceivers']);
        Route::get('/customer_n2uk/{id}/receivers', ['as'=>'receivers_n2uk.receivers', 'uses'=>'N2UK\ReceiversController@index']);
        Route::get('customer_n2uk/autocomplete', ['as'=>'customer_n2uk.autocomplete', 'uses'=>'N2UK\CustomersController@autocomplete']);

        /* Receivers 
            |-------------------------------------
         */
        Route::resource('receivers_n2uk', 'N2UK\ReceiversController');
        Route::get('sender/{id}/receivers_n2uk/autocomplete', ['as'=>'receivers_n2uk.autocomplete', 'uses'=>'N2UK\ReceiversController@autocomplete']);
        Route::get('/customer_n2uk/{id}/create/receiver', ['as'=>'receivers_n2uk.create', 'uses'=>'N2UK\ReceiversController@create']);

        /* Transactions 
            |-------------------------------------
         */
        Route::resource('transactions_n2uk', 'N2UK\TransactionsController');

        Route::get('/transactions_n2uk/{id}/list',
                             ['as'=>'transactions_n2uk.list', 'uses'=>'N2UK\TransactionsController@list']);

        Route::get('/transactions_n2uk/customer/{id}/receiver/{id2}/transfer', 
                                ['as'=>'transactions_n2uk.transfer', 
                                'uses'=>'N2UK\TransactionsController@transfer']);

        Route::get('/transactions_n2uk/{id}/receipt',
                             ['as'=>'transactions_n2uk.receipt',
                              'uses'=>'N2UK\CustomersController@receipt']);
        
      Route::post('/transactions_n2uk/post_search',
                              ['as'=>'transactions_n2uk.post_search',
                               'uses'=>'N2UK\TransactionsController@post_search']);
        

         
                
         

 // Api Agent customers list
        Route::get('/{id}/customer',
                 'AgentCustomerController@listCustomers');

         			 /* --- Api receivers list */
          Route::get('/transactions/sender/{id}/receivers', ['as'=>'sender.receivers',
        	   function($id){
        			return response()->json(Receiver::whereSender_id($id)->get());
        	}]);
           			/* --- Api receiver details */
          Route::get('/transactions/receiver/{id}', ['as'=>'sender.receivers',
        	   function($id){
        			return response()->json(Receiver::whereId($id)->first());
        	}]);
          			 /* --- Api rate-commission */
          Route::get('/transactions/{amount}/findratecommission', ['as'=>'receiver.calculation', function($amount){
        		return response()->json(['rate'=>Auth::user()->rate(),'commission'=>Auth::user()->commission($amount)]);
        }]);
     

});


Auth::routes();
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
//Route::get('/home', 'HomeController@index')->name('home');

