<?php

use Illuminate\Http\Request;
use App\User;
use App\Currency;
use App\Commission;

/*
|-----------------------------------------------------------------
| API Routes
|-----------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



/*
|-----------------------------------------------------------------
| N2UK 
|-----------------------------------------------------------------
*/
Route::get('/n2uk/customers', ['as'=>'customers_n2uk.all',
     function(){
      return response()->json(App\N2UK\Customer::get());
  }]);

Route::get('/n2uk/customer/{id}', ['as'=>'customers_n2uk.single',
     function($id){
      return response()->json(App\N2UK\Customer::whereId($id)->first());
  }]);

//Receivers
//Api ---N2UK----
  
   Route::get('/n2uk/customer/{id}/receivers', ['as'=>'receivers.all',
     function($id){
      return response()->json(App\N2UK\Receiver::whereSender_id($id)->get());
  }]);
        
  Route::get('/n2uk/receiver/{id}', ['as'=>'receivers.single',
     function($id){
      return response()->json(App\N2UK\Receiver::whereId($id)->first());
  }]);

   Route::get('/n2uk/bou_rate',
     function(){
      return response()->json(App\N2UK\Rate::bou_rate());
  });;


   Route::get('/n2uk/sold_rate',
     function(){
      return response()->json(App\N2UK\Rate::sold_rate());
  });




/*
|-----------------------------------------------------------------
|* For Externals
|-----------------------------------------------------------------
*/
Route::get('/currencies/rate',function(){
    return Currency::rates();
});

  Route::get('/{amount}/findratecommission',  function($amount){
            return response()->json(['rate'=>App\Rate::todays_rate()->rate,'commission'=>App\Commission::findCommission($amount)]);
        });

Route::get('/todaysrate',function(){
  return response()->json(App\Rate::todays_rate());
});
Route::resource('senders','Api\SendersController');
Route::get('/{id}/senders','Api\SendersController@index');

Route::resource('receivers','Api\ReceiversController');
Route::get('{id}/{type}/receivers','Api\ReceiversController@index');

Route::resource('rates','Api\RateController');
Route::get('/currency/{currency_id}/rate',function($currency_id){
    $user = new User; return $user->rate($currency_id);   
});


Route::resource('banks','Api\BanksController');

Route::resource('commissions','Api\CommissionsController');

Route::resource('currencies','Api\CurrenciesController');
//Route::get('{userid}/{amount}/commission','Api\CommissionsController@userCommission');
Route::get('user/{userid}/currency/{currency_id}/amount/{amount}/commission',function($user_id,$currency_id,$amount){
  return response()->json(Commission::userCommission($user_id,$currency_id,$amount));
});

Route::resource('transactions','Api\TransactionsController');
Route::get('/{id}/transactions','Api\TransactionsController@index');

Route::resource('users','Api\UsersController');


