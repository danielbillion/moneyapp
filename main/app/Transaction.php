<?php

namespace App;
use App\helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\AgentCustomer;
use App\Address;
use App\Receiver;
use App\User;
use Illuminate\Support\Carbon;
use Auth;
use Illuminate\Support\Arr;
class Transaction extends Model
{
    //
  protected $table = "laravel_transactions";
  private static $table2 = "laravel_transactions";
    protected $fillable = [
'receipt_number',
'receiver_phone',
'type',
'user_id',
'sender_id',
'receiver_id',
'amount',
'commission',
'agent_commission',
'exchange_rate',
'currency_id',
'currency_income',
'bou_rate',
'sold_rate',
'note',
'status'
    ]; 

/*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
 */
    public  function user(){
    	return $this->belongsTo('App\User')->withDefault(function ($user) {
            $user->name= 'unavailable';});;
    }

    public function agentCustomer(){
    	return $this->belongsTo('App\AgentCustomer','customer_id');
    }
    public function sender(){
        if($this->attributes['type'] ==='agent'){
            return $this->belongsTo('App\AgentCustomer','sender_id')->withDefault(function ($sender) {
                $sender->name= 'unavailable';});
        }else{
            return $this->belongsTo('App\User','user_id')->withDefault(function ($sender) {
                $sender->name= 'unavailable';});
        }
    
    }

    public function receiver(){
      return $this->belongsTo('App\Receiver','receiver_id','id')->withDefault(function ($receiver) {
        $receiver->name= 'unavailable';
            });
    }

    public function currency(){
    	return $this->belongsTo('App\Currency','currency_id');
    }

    public function Address_amChecking(){
        return $this->hasManyThrough('App\Address','App\AgentCustomer','sender_id','user_id');
    }

    public function message(){
        return $this->morphMany('App\Message','messageable');
    }
    public function payment(){
        return $this->morphMany('App\Payments','paymentable');
    }

 /*
    |--------------------------------------------------------------------------
    | Attributes 
    |--------------------------------------------------------------------------
 */

    

    //Attributes Assessors
     public function getSenderIdAttribute($value){
        return $this->sender_name = $value;
    }
    


     public function getSenderNameAttribute($value){
       if($this->type === 'agent'){
            if(\App\AgentCustomer::whereId($this->sender_id)){
                $sender_name =\App\AgentCustomer::whereId($this->sender_id)->value('name');
            }
        }else{
            if(\App\User::whereId($this->user_id)){
                $sender_name = $this->belongsTo('App\user','user_id')->first()->name;
            }else{
                $sender_name = "unavailable";
            }

        }
        return ucfirst($sender_name?$sender_name:'none');   
    }

     public function getReceiverNameAttribute($value){
        
        $name = Receiver::whereId($this->receiver_id)->select('fname','lname')->first();
       
        return isset($name->fname)?$name->fname." ".$name->lname:"" ;
    }
  
     public function getSenderPhoneAttribute($value){
            $sender = AgentCustomer::find($this->sender_id)->first();
                return $sender->phone;
    }

   
    public function setNoteAttribute($value){
        $this->attributes['note'] = "unavailable";
    }

    public function setIdentityAttribute($value){
        $this->attributes['identity'] =!isset($value)?"None":$value; 
    } 

    public function setReceiverPhoneAttribute($value){
        $this->attributes['receiver_phone'] =!isset($value)?"None":$value;
    } 

    public function getBankAttribute($value){
    	return ucFirst($value);
    }

    public function getTransferTypeAttribute($value){
    	return ucFirst($value);
    }
    // public function setAmountAttribute($value){
    //   return number_format($value,2);
    // }

    public function getTotalAttribute(){
    	return number_format(($this->amount + $this->commission),2);
    }

    public function getLocalPaymentAttribute(){
    	return number_format(($this->amount * $this->exchange_rate),2);
    }
//Actual value of agent comssion  = agent % commission * commission
    
//business commision = commission - agent commsion
    public function getBusinessCommissionAttribute(){
    	return number_format(($this->commission - $this->agent_commission),2);
    }

    public function getAddressAttribute(){
         if($this->type === 'agent'){
            $address= Address::where('agent_customer_id',$this->sender_id)
                            ->value('address');
           }else{
            $address= Address::where('user_id',$this->user_id)
                            ->value('address');
           }
           return $address?$address:'unavailable';

    }

    public function getPostcodeAttribute(){
        if($this->type === 'agent'){
            $postcode= Address::where('agent_customer_id',$this->sender_id)
                            ->value('postcode');
           }else{
            $postcode= Address::where('user_id',$this->user_id)
                            ->value('postcode');
           }
           return $postcode?$postcode:'unavailable';
    }

    public function getCountryAttribute(){
        if($this->type === 'agent'){
            $country= Address::where('agent_customer_id',$this->sender_id)
                            ->value('country');
           }else{
            $country= Address::where('user_id',$this->user_id)
                            ->value('country');
           }
           return $country?$country:'unavailable';
    }
    public function getBodyAttribute($value){
        if($this->morphMany('App\Message','messageable')->exists())
            {
             return $this->morphMany('App\Message','messageable')
                         ->orderBy('id','desc')->first()->body;
             }else return '';
     }
    

/*
    |--------------------------------------------------------------------------
    | Methods 
    |--------------------------------------------------------------------------
 */



    //  public function address(){
    //         return $this->hasOne('App\Address','agent_customer_id','sender_id')->withDefault(function ($user) {
    //         $user->address = 'unavailable';
    //         $user->county = 'unavailable'; 
    //          $user->country = 'unavailable';
    //          $user->postcode = 'unavailable';
    //          $user->town = 'unavailable';
    //          });
    //      }
    
        //list of receivers for edit
    public function senderReceivers(){
        return Receiver::whereSender_id($this->sender_id)->pluck('fname','id')->toArray();
    }

    public function getSender(){
        return $this->sender_id;
    }

     public static function pendingAgentTransactions(){
       return Transaction::where('status','pending')->where('type','agent')->count();
    }
     public static function pendingCustomerTransactions(){
       return Transaction::where('status','pending')->where('type','customer')->count();
    }

     public static function adminTransSearch($type, $value){
       
        switch($type){
            case "id":  
             $transactions = Transaction::
                        whereId($value)->paginate(15); break; 
            case "today":  
             $transactions = Transaction::
                        whereDate('created_at', Carbon::today())->orderBy('created_at','desc')->paginate(15); break;
            case "yesterday":  
             $transactions = Transaction::
                        whereDate('created_at', Carbon::yesterday())->orderBy('created_at','desc')->paginate(15); break;
            case "month":  
             $transactions = Transaction::
                        where('created_at', '>=', Carbon::now()->subMonth())->orderBy('created_at','desc')->paginate(15); break;
             case "payments":  
              $transactions = Transaction::whereUser_id($value)->
                        whereAgent_payment_id('none')->paginate(15); break;
            case "date":  
                $date = explode(',', $value);

               $transactions = Transaction::
                        whereBetween('created_at',[$date[0],$date[1]])->paginate(15); break;
             case "post_search":
                $from = date($value['from']); $to = date($value['to']);
            
                 if($value['query_string']===null || $value['query_string']  ==="" ){
                            
                    $transactions = 
                            Transaction::whereDate('created_at','>=', $from)
                                                    ->whereDate('created_at','<=', $to)
                                                    ->where('status',$value['status'])
                                                    ->where('currency_id',$value['currency_id'])
                                                    ->orderBy('created_at','desc')->paginate(15); break;
                                                        
                        }else{
                            $transactions =
                                Transaction::Where('receipt_number','like', '%'. $value['query_string']. '%')
                                            ->orWhere('receiver_phone','like', '%' . $value['query_string']. '%' )
                                            ->orWhere('sender_id', \App\AgentCustomer::whereName($value['query_string'])->value('id') )
                                            ->orWhere('sender_id', \App\AgentCustomer::whereMobile($value['query_string'])->value('id') )
                                            ->orWhere('sender_id', \App\AgentCustomer::wherePhone($value['query_string'])->value('id') )
                                             ->orWhere('user_id', \App\User::whereName($value['query_string'])->value('id') )
                                            ->orWhere('user_id', \App\User::whereMobile($value['query_string'])->value('id') )
                                            ->orWhere('user_id', \App\User::wherePhone($value['query_string'])->value('id') )
                                            ->where('currency_id',$value['currency_id'])
                                            ->orderBy('created_at','desc')->paginate(60); break;
                                }
            }
           
            return $transactions;   
    }

     public static function membersTransSearch($type, $value){
            $role = Auth::user()->type;
            $id = Auth::id();
            $model = $role =='agent'?'\App\AgentCustomer':'\App\User';  

        switch($type){
             case "id":  
             $transactions = Transaction::whereId($value)->
                        whereType($role)->paginate(15); break;
            case "today":  
                $transactions = Transaction::whereDate('created_at', Carbon::today())
                                           ->whereUser_id($id)->where('type',$role)->paginate(15); break;
            case "yesterday":  
             $transactions = Transaction:: whereDate('created_at', Carbon::yesterday())
                                        ->whereUser_id($id)->where('type',$role)->paginate(15); break;
            case "month":  
             $transactions = Transaction::where('created_at', '>=', Carbon::now()->subMonth())
                                         ->whereUser_id($id)->where('type',$role)->paginate(15); break;
            case "date":  
                $date = explode(',', $value);
               $transactions = Transaction:: whereBetween('created_at',[$date[0],$date[1]])
                                         ->whereUser_id($id)->where('type',$role)->paginate(15); break;
            case "post_search":
                    $from = date($value['from']); $to = date($value['to']);

                if($value['query_string']===null || $value['query_string']  ==="" ){
                
                    $transactions = Transaction::whereDate('created_at','>=', $from)
                                                ->whereDate('created_at','<=', $to)
                                                ->where('status',$value['status'])
                                                ->where('currency_id',$value['currency_id'])
                                                ->where('user_id',$id)->
                                                where('type',$role)
                                                ->orderBy('created_at','desc')->paginate(15); break;
                                            
                    }else{
                            $transactions = Transaction::where('receipt_number','like', '%'. $value['query_string'])
                                                ->orWhere('receiver_phone','like', '%' . $value['query_string'] )
                                                ->orWhere('sender_id', $model::whereName($value['query_string'])->value('id') )
                                                ->orWhere('sender_id', $model::whereMobile($value['query_string'])->value('id') )
                                                ->orWhere('sender_id', $model::wherePhone($value['query_string'])->value('id') )
                                                ->where('currency_id',$value['currency_id'])
                                                ->whereUser_id($id)->where('type',$role)
                                                ->orderBy('created_at','desc')->paginate(60); break;
                    }
            }
            return $transactions;   
    }

    public static function aggregate(){
            $role = Auth::user()->type;
            $id = Auth::id();

            $totalAmount  = Transaction::whereType($role)->whereUser_id($id)
                                    ->sum('amount');
            $commission  = Transaction::whereType($role)->whereUser_id($id)
                                         ->sum('commission');
            $total  = $totalAmount + $commission;

            if($role ==='admin' || $role ==='manager' ){
                $totalAmount  = Transaction::sum('amount');
                $commission  = Transaction::sum('commission');
                 $total  = $totalAmount + $commission;
            }
                   
            
                return [
                        'total' =>number_format($total,2),
                        'totalAmount'=>number_format($totalAmount,2),
                        'commission'=>number_format($commission,2)
                ];
                    
    }
    public static function chart(){
        $chart = new \stdClass();
        if(Auth::user()->type ==='admin'){
             $results =   DB::table(self::$table2)->select(DB::raw('YEAR(created_at) as year'),
                            DB::raw('SUM(amount) AS total'))
                            ->orwhere('status', 'pending')
                            ->orWhere('status', 'paid')
                            ->groupBy('year')
                            ->orderBy(DB::raw('YEAR(created_at)'))
                            ->get();
                    }else{

                        $results =   DB::table(self::$table2)->select(DB::raw('YEAR(created_at) as year'),
                            DB::raw('SUM(amount) AS total'))
                            ->orWhere('user_id', Auth::id())
                            ->groupBy('year')
                            ->orderBy(DB::raw('YEAR(created_at)'))
                            ->get();
                    }
                  
                    $label=""; $data="";
                    foreach($results as $key=>$result){
                        $label =$key!==0?$label.','.$result->year:$label.$result->year;
                        $data=$key!==0?$data.','.$result->total:$data.$result->total;
                    }

                    $chart->data = $data;
                    $chart->label = $label;
                return $chart;
                //return $results;

    }

    public static function turnover(){
        
       if(Auth::user()->type ==='admin'){
             return Transaction::
                    select(DB::raw('YEAR(created_at) as year,MONTH(created_at) AS month'),
                            DB::raw(' SUM(amount + commission) AS total'), 
                            DB::raw(' SUM(amount) AS amount'),
                            DB::raw(' SUM(agent_commission) AS agent_commission'),
                            DB::raw(' SUM(commission) AS commission'),
                            DB::raw('COUNT(amount) AS counted '))
                        ->where(DB::raw('created_at BETWEEN DATE(NOW()) - INTERVAL (DAY(NOW()) - 1) 
                        DAY - INTERVAL 100 MONTH AND NOW()'))
                        ->orWhere('status', 'pending')
                        ->orWhere('status', 'paid')
                        ->groupBy('year','month')
                        ->orderBy(DB::raw('YEAR(created_at), MONTH(created_at)'))
                        ->simplePaginate('25');
                    
        }else{
            return Transaction::
            select(DB::raw('YEAR(created_at) as year,MONTH(created_at) AS month'),
                    DB::raw(' SUM(amount + commission) AS total'), 
                    DB::raw(' SUM(amount) AS amount'),
                    DB::raw(' SUM(agent_commission) AS agent_commission'),
                    DB::raw(' SUM(commission) AS commission'),
                    DB::raw('COUNT(amount) AS counted '))
                ->where(DB::raw('created_at BETWEEN DATE(NOW()) - INTERVAL (DAY(NOW()) - 1) 
                DAY - INTERVAL 100 MONTH AND NOW()'))
                ->orWhere('user_id',Auth::id())
                ->groupBy('year','month')
                ->orderBy(DB::raw('YEAR(created_at), MONTH(created_at)'))
                ->simplePaginate('25');
                
        }
        
    }
//Senders list for transactions
//     public static function senders($id){

//             $null = new \stdClass();                    //empty object for no value
//             $null->name = "Pease Select"; $null->id = 0;
            
// //list out all agents sender
//             $customer = Auth::user()->agentcustomer()
//                             ->select(DB::raw("CONCAT(fname,' ',lname) AS value"),'id')
//                             ->pluck('value','id')
//                             ->toArray();

//            $sender = AgentCustomer::find($id);
//            $sender =!isset($id)?$null:AgentCustomer::whereId($id)->first();

//     //merge default sender & sender list
//            $customer = array_merge( [$sender->id => $sender->name],[$customer]);
//          return  $customer;

//     }

// //receiver list for transactions
//     public static function receivers($sender,$receiver){

//             $null = new \stdClass();        //empty object for no value
//             $null->fname = "";
//             $null->lname = "";
//             $null->id = 0;
//             $null->transfer_type = "";
//             $null->bank = "";
//             $null->account_number = "";
//             $null->phone = "";
// //get selected receiver
//          $selected= !isset($receiver) || $receiver == "null"?
//             ['0'=>'Please Select']
//                                     :Receiver::whereId($receiver)
//                                     ->select(DB::raw("CONCAT(fname,' ',lname) AS value"),'id')
//                                     ->pluck('value','id')
//                                     ->toArray();
// //new receiver option to be added for all option
//           $default = []; $default['2'] = "New Receiever";
// //senders receiver list   
//         $whereId = Auth::user()->type=='agent'?'whereSender_id':'whereUser_id';  
//           $senderReceivers =Receiver::$whereId($sender)
//                             ->select(DB::raw("CONCAT(fname,' ',lname) AS value"),'id')
//                             ->pluck('value','id')
//                             ->toArray();
//         $list = array_merge( [$selected],[$senderReceivers],[$default]);
//         $result = new \stdClass();                //new object receiver
//         $result->list = $list;                    //array of receiver & option
//         $result->receiver =!isset($receiver) || $receiver == "null"?$null
//         : Receiver::whereId($receiver)->first(); //single receier object
//          return  $result;
        
//     }

    
}
