<?php

namespace App;
use App\Agent_Payment;
use App\Manager_Payment;
use Illuminate\Database\Eloquent\Model;

class Manager extends Model
{
    //
    protected $table = "laravel_users";

     /*
    |--------------------------------------------------------------------------
    |Relationships
    |--------------------------------------------------------------------------
 */

    public function managerPayments(){
    	return $this->hasMany('App\Manager_Payment','manager_id');
    }

 /*
    |--------------------------------------------------------------------------
    |Managers Payment 
    |--------------------------------------------------------------------------
 */
    //calculate agent outsnading payment/remittance
    //Agent_Payment Model(payment db2_tables(connection)) for Manager List of Payments

    public function managerTotalPayment(){
      $outstanding = Agent_Payment::whereManager_id($this->id)
                                    ->whereAdmin_payment_id(0)->sum('amount');

    $previousBalance = Agent_Payment::whereManager_id($this->id)
                            ->orderBy('id', 'DESC')->limit(1)->value('balance');

    return $outstanding + $previousBalance;
    }
    

     public function countManagerPendingPayment(){
      $countPending = Agent_Payment::whereManager_id($this->id)
                                    ->whereAdmin_payment_id(0)->count();

    return $countPending;
    }
}
