<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //
    protected $table = "laravel_roles";
    protected $fillable = ['name'];
}
