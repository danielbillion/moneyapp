<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receiver extends Model
{
    //
    protected $table = "laravel_receivers";
    protected $fillable = [
    	'fname',
    	'lname',
    	'user_id',
    	'sender_id',
    	'phone',
    	'bank',
    	'transfer_type',
    	'identity_type',
    	'account_number',

    ];

    public function agentcustommer(){
    	return $this->belongsTo('App\AgentCustomer');
    }
      public function getNameAttribute($value){
        
        return ucfirst($this->fname." ".$this->lname);
	}
	
	public function transactions(){
		return $this->hasMany('App\Transaction');
	}

   
}
