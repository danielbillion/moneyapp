<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trans extends Model
{
    //
    protected $table = "laravel_transactions";
    
    public function payment(){
        return $this->morphMany('App\Payments','paymentable');
    }
}
