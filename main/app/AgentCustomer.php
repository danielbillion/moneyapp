<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Transaction;
use App\Receiver;
use App\currency;
class AgentCustomer extends Model

{
    //

    //public $timestamps = false;
    protected $table = "laravel_agent_customers";
	 protected $fillable = ['user_id','fname','lname',
							'mname','name','email','mobile','phone',
							'dob','address','postcode','title','currency_id',
							'address_id','photo_id'

	 ];
	
   // protected $guarded = ['hash'];

   // -- Relationships ------

   public function user(){
	return $this->belongsTo('App\User');
}

	public function address(){			
		return $this->HasOne('App\Address','agent_customer_id');
	}

	public function photo(){
		return $this->belongsTo('App\Photo');
	}

	public function receiver(){
		return $this->hasMany('App\Receiver');
	}

	public function currency(){
        return $this->belongsTo('App\Currency','currency_id')->first();
    }

	public function photoExist(){
		return $this->hasOne('App\Photo', 'id');
	}

	public function validPhone(){
		return $this->morphOne('App\Phone','phoneable')->withDefault();
	}

   // ***********  Assessor Atributes && Mutator /
   public function getFnameAttribute($value){
	return ucfirst($value);
	}
	public function getnameAttribute($value){
		return ucfirst($value);
	}
	public function getMnameAttribute($value){
		return strtoupper($value);
		}
   public function setPhoneAtrribute($value){
		$this->attributes['phone'] = $value ==null?'none':$value;
	}

	public function setMnameAtrribute($value){
		$this->attributes['mname'] = $value !==null?$value:'none';
	 }
	 
	

 	public function setEmailAtrribute($value){
		$this->attributes['email'] = isset($value)?$value:'none';
 	}
	

	public  function getTransactionsAttribute(){
		return Transaction::whereSender_id($this->id)
											->count();
	}
	public function getReceiverssAttribute(){
		return Receiver::whereSender_id($this->id)
											->count();
	}	

	

	  

	

	

}


