<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartPayment extends Model
{
	protected $table = "laravel_partpayments";
    //
    protected $fillable = [
    'user_id',
    'manager_id',
    'payment_type',
    'amount',
    'admin_payment_id',
    'fully_paid'
    ];
    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function transaction(){
    	return $this->hasOne('App\Transaction','transaction_id');
    }

    public function paymentable(){
        return $this->morphTo();
    }

    public static function PaymentSearch($type, $value){
		      
        switch($type){
             
            case "commission":
                    $from = date($value['from']); $to = date($value['to']);
                    
                    $payment = PartPayment::whereDate('created_at','>=', $from)
                                                ->whereDate('created_at','<=', $to)
                                                ->where('payment_type','commission')
                                                ->paginate(15); break;
            case "transaction":
                    $from = date($value['from']); $to = date($value['to']);
                    
                    $payment = PartPayment::whereDate('created_at','>=', $from)
                                                ->whereDate('created_at','<=', $to)
                                                ->where('payment_type','transaction')
                                                ->paginate(15); break;
                  

            }
            return $payment;   
    }

  
}
