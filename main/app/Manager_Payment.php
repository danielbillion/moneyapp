<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manager_Payment extends Model
{
    //
    protected $table = "laravel_payment_managers"; 
     protected $fillable = [
    'manager_id',
    'payment_type',
    'manager_name',
    'amount',
    'balance',
    'total',
    'fully_paid'
    ];
}
