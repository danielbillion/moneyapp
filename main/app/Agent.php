<?php

namespace App;
use App\Agent_Payment;
use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    //manual table
    protected $table = "laravel_users";

    //calculate agent outsnading payment/remittance
    public function agentTotalPayment(){
      $outstanding = Transaction::whereUser_id($this->id)
                                    ->whereAgent_payment_id('none')->sum('amount');

    $previousBalance = Agent_Payment::whereUser_id($this->id)
                            ->orderBy('id', 'DESC')->limit(1)->value('balance');

    return $outstanding + $previousBalance;
    }
    

     public function countPendingPayment(){
      $countPending = Transaction::whereUser_id($this->id)
                                    ->whereAgent_payment_id('none')->count();

    return $countPending;
    }
}
