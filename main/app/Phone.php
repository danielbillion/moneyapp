<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    //
    protected $table = "laravel_phones";

    protected $fillable = ['phoneable_id','phoneable_type',
            'mobile','phone','code','is_valid'];
    public function phoneable(){
        return $this->morphTo;
    }
}
