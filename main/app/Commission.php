<?php

namespace App;
use Auth;
use Illuminate\Database\Eloquent\Model;

class Commission extends Model
{
    //
    protected $table = "laravel_commissions";
    public $fillable = [
'start_from','end_at','value','agent_quota','user_id','currency_id'
    ];

	 public function user()
    {
        return $this->belongsTo('App\User')->withDefault(function ($user) {
			$user->name = 'All';
		});;
	}
	
	public function currency()
    {
			return $this->belongsTo('App\Currency','currency_id')->withDefault(function ($user) {
			$currency->code = 'None';
		});;
    }
    public static function findCommission($amount){
	    	if(Auth::check()){
	    			return  Commission::
	        					where('user_id',$this->id)
	        					->whereRaw('? between start_from and end_at', [$amount])
	        					->select('value','agent_quota')
	        					->first();
	    	}
        

       	else{
	       	 return Commission::
	        					where('user_id',0)
	        					->whereRaw('? between start_from and end_at', [$amount])
	        					->select('value','agent_quota')
	        					->first();
	       		}

      
			}
	//Api use	
//set commisssion value for 
//amount sent based on commission currency
	private static function setCommission($user_id,$currency_id,$amount){
		return  Commission::
							where('user_id',$user_id)
							->where('currency_id',$currency_id)
							->whereRaw('? between start_from and end_at', [$amount])
							->select('value','agent_quota')
							->first();
	}

	public static function userCommission($user_id = 0,$currency_id = 1,$amount){
				//$user_id = $user_id === 'null'?0:$user_id;
				$commission =   self::setCommission($user_id,$currency_id,$amount);
				
		//Default commission, with no user_id set /currency_id
		if(!$commission){										
						$commission = self::setCommission(0,$currency_id,$amount);	
					}
		//check for decimal commission value
		if($commission){
			$commission->value = $commission->value < 1?
									number_format($commission->value * $amount,2) 
									: number_format($commission->value,2);
					return $commission;
				}else{
					return 	['value'=> 0, 'agent_quota'=> 0];
				}
			
		}
}
