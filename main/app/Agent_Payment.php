<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agent_Payment extends Model
{
	protected $table = "laravel_payments";
    //
    protected $fillable = [
    'user_id',
    'manager_id',
    'payment_type',
    'agent_name',
    'amount',
    'balance',
    'total',
    'admin_payment_id',
    'fully_paid'
    ];
    public function user(){
    	return $this->belongsTo('App\User');
    }

  
}
