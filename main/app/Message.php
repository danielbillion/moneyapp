<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    //
    protected $table = "laravel_messages";

    protected $fillable = [
        'messageable_id',
        'messageable_type',
        'body',
        'phone',
        'email'
    ];

    public function messageable(){
        return $this->morphTo();
    }

    public function setPhoneAttribute($value){
        $this->attributes['phone'] = $value !==null?$value:'none';
    }

    public function setEmailAttribute($value){
        $this->attributes['email'] = $value !==null?$value:'none';
    }

    public function setSubjectAttribute($value){
        $this->attributes['subject'] = $value !==null?$value:'none';
    }

    public function setBodyAttribute($value){
        $this->attributes['body'] = $value !==null?$value:'';
    }

   
}
