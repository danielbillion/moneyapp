<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class transactionEmail extends Mailable
{
    use Queueable, SerializesModels;
    protected $transaction;
    protected $business;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($transaction)
    {
        //
        $this->transaction = $transaction;
        $this->business = \App\Setting_business::setting();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return  $this->from(env('MAIL_FROM_ADDRESS'))
                        ->subject('Transaction Receipt')
                        ->with(['transaction'=>$this->transaction,'business'=>$this->business])
                        ->view('transactions.email')
                        ;
    }
}
