<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminToUserMail extends Mailable
{
    use Queueable, SerializesModels;
    public $mail;
    public $subject;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mail)
    {
        //
        $this->mail = $mail;
        $this->subject = $mail->subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_FROM_ADDRESS'))
                    ->subject($this->subject)
                    ->view('mails.admintouser')
                    ->text('mails.admintouser_plain')
                    ->with(
                      [
                            'mail' => $this->mail,
                            'testVarTwo' => '2',
                      ]);
       // return $this->view('view.name');
    }
}
