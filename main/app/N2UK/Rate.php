<?php

namespace App\N2UK;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    //
    protected $table = "laravel__n2uk_rates";
    protected $fillable= [
    	'bou',
    	'sold',
    	'user_id'
    ];

    public static function sold_rate(){
    	return self::orderBy('id','des')->pluck('sold')->take(1)->first();
    }

     public static function bou_rate(){
    	return self::orderBy('id','des')->pluck('bou')->take(1)->first();
    }
}
