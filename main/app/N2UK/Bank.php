<?php

namespace App\N2UK;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    //
    protected $table = "laravel__n2uk_banks";
    protected $fillable = [
    	'name',
    	'type'
    ];
}
