<?php

namespace App\N2UK;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //
    protected $table = "laravel__n2uk_customers";
    protected $fillable = [
    	'user_id',
    	'fname',
    	'lname',
    	'mname',
    	'name',
    	'email',
    	'mobile',
    	'phone',
    	'dob',
    	'address',
    	'postcode',
    	'title'


    	
];

        public function transaction(){
                return $this->hasMany('App\Transaction','user_id');
        }

        

}
