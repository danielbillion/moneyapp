<?php

namespace App\N2UK;

use Illuminate\Database\Eloquent\Model;
use App\N2UK\Customer;
class Receiver extends Model
{
    //
    protected $table = "laravel__n2uk_receivers";
    protected $fillable = [
		'user_id',
		'sender_id',
    	'fname',
    	'lname',
    	'phone',
    	'transfer_type',
    	'identity_type',
    	'uk_bank',
    	'uk_account_no',
    	'uk_sort_code',
    	'ng_account_name',
    	'ng_bank',
    	'ng_account_no'
    ];

    public function customer(){
        return $this->belongsTo('App\N2UK\Customer','sender_id');
	}
	
	public function senderReceivers(){
        return Receiver::whereSender_id($this->id)->pluck('name','id')->toArray();
    }

    public function getNameAttribute(){
        return $this->fname." ".$this->lname;
    }
}
