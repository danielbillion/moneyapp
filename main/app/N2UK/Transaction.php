<?php

namespace App\N2UK;
use App\N2UK\Receiver;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    //
    protected $table = "laravel__n2uk_transactions";
    protected $fillable = [
    	'transfer_code',
    	'user_id',
    	'sender_id',
    	'receiver_id',
    	'receiver_name',
    	'receiver_phone',
    	'amount_pounds',
    	'amount_naira',
    	'bou_rate',
    	'sold_rate',
    	'margin',
    	'note',
    	'status'
    ];

    public function customer(){
    	return $this->belongsTo('App\N2UK\Customer','sender_id')->withDefault(function ($customer) {
			$customer->name= 'unavailable';
				});
    }

     public function receiver(){
    	return $this->belongsTo('App\N2UK\Receiver','receiver_id')->withDefault(function ($receiver) {
			$receiver->fname= 'unavailable';
				});;;
	}
	
	public function senderReceivers(){
        return Receiver::whereSender_id($this->sender_id)->pluck('fname','id')->toArray();
	}
	
	public static function TransSearch($type, $value){
		      
	switch($type){
		 
		case "post_search":
				$from = date($value['from']); $to = date($value['to']);

			if($value['query_string']===null || $value['query_string']  ==="" ){
			
				$transactions = Transaction::whereDate('created_at','>=', $from)
											->whereDate('created_at','<=', $to)
											->paginate(15); break;
										
				}else{
						$transactions = Transaction::whereDate('created_at','>=', $from)
											->whereDate('created_at','<=', $to)
											->orwhere('transfer_code','like', '%'. $value['query_string']. '%')
											->paginate(15); break;
				}
		}
		return $transactions;   
}
}
