<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Yadahan\AuthenticationLog\AuthenticationLogable;

use Auth;
use App\Address;
use App\Transaction;
use App\Payment;
use App\PartPayment;
use App\Role_User;
use DB;

class User extends Authenticatable
//class User
{
    use Notifiable;
    use Notifiable, AuthenticationLogable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'laravel_users';
    protected $fillable = [
        'name', 'email', 'password',
        'fname','lname','email',
        'phone','mobile','photo_id',
        'address_id','currency_id',
        'is_active','dob','title','mname','credit','type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
 /*
    |--------------------------------------------------------------------------
    |Rrelationship
    |--------------------------------------------------------------------------
 */ /*
    |--------------------------------------------------------------------------
    | 
    |--------------------------------------------------------------------------
 */
   
    public function type(){

        $role = Auth::user()->role;
        return $role['0']->name;
    }

      public function address(){
       return $this->hasOne('App\Address')->withDefault(function ($user) {
        $user->county = 'unavailable';
        $user->country = 'unavailable';
        $user->postcode = 'unavailable';
        $user->town = 'unavailable';
    });
    }

     public function transactions(){
        return $this->hasMany('App\Transaction');
    }
    public function member(){
        return $this->hasMany('App\Member');
    }

     public function agentcustomer(){
        return $this->hasMany('App\AgentCustomer');
    }

     public function transaction(){
        return $this->hasMany('App\Transaction');
    }

    public function role(){
        return $this->belongsToMany('App\Role','laravel_role_user');
    }
    public function currency(){
        return $this->belongsTo('App\Currency','currency_id')->first();
    }

    public function transaction_payments(){
        return $this->hasMany('App\PartPayment')->where('payment_type','transaction');
    }
    public function transaction_part_payments(){
        return $this->hasMany('App\PartPayment')->where('payment_type','transaction')->where('fully_paid',0);
    }

    public function commission_payments(){
        return $this->hasMany('App\PartPayment')->where('payment_type','commission');
    }
    public function commission_part_payments(){
        return $this->hasMany('App\PartPayment')->where('payment_type','commission')->where('fully_paid',0);
    }

   

    public function pending_outstanding(){     // outstanding payment by agent
        return $this->hasMany('App\Outstanding','user_id')->where('transaction_paid',0);
    }
    public function pending_commission(){      //outstanding payment by admin
        return $this->hasMany('App\Outstanding','user_id')->where('commission_paid',0);
    }

    public function message(){
        return $this->morphMany('App\Message','messageable');
    }

    public function payment(){
        return $this->hasMany('App\Payment');
    }

    public function validPhone(){
        return $this->morphOne('App\Phone','phoneable')->withDefault();
        
    }

    /*
    |--------------------------------------------------------------------------
    | Attributes Mutator
    |--------------------------------------------------------------------------
 */
public function setMnameAttribute($value){
    $this->attributes['mname'] =!isset($value)?"-":$value;
}

public function getBodyAttribute($value){
   if($this->morphMany('App\Message','messageable')->exists())
       {
        return $this->morphMany('App\Message','messageable')
                    ->orderBy('id','desc')->first()->body;
        }else return '';
}

public function getNameAttribute($value){
    return ucFirst($value);
}


    

    
 /*
    |--------------------------------------------------------------------------
    |Authentitcation Type
    |--------------------------------------------------------------------------
    */

    public function isCustomer(){

        $type = Auth::user()->type;
        if($type === "customer" && ($this->is_active == 1 || $this->is_active == 2 )){
            return true;
        }
        return false;
    }

    public function isAgent(){

        $type = Auth::user()->type;
        if($type === "agent" && ($this->is_active == 1 || $this->is_active == 2 )){
            return true;
        }
        return false;
    }
      public function isManager(){
     $type = Auth::user()->type;
        if($type === "manager" && ($this->is_active == 1 || $this->is_active == 2 )){
            return true;
        }
        return false;
    }

    public function isAdmin(){
     $type = Auth::user()->type;
        if($type === "admin" && ($this->is_active == 1 || $this->is_active == 2 )){
            return true;
        }
        return false;
    }



 /*
    |--------------------------------------------------------------------------
    |Transaction
    |--------------------------------------------------------------------------
 */

        //find agent customers transactions
    public function agentTransactions(){
        return $this->hasMany('App\Transaction');
    }
    
       
    public function rate($currency_id = null){ 
             $currency_id = !$currency_id?1:$currency_id;
        $rate = Rate::where('user_id',$this->id)->where('currency_id',$currency_id)
                        ->orderBy('id','desc')->select('rate','bou_rate','sold_rate')->first();
        $rate = isset($rate)?$rate:Rate::where('user_id',0)->where('currency_id',$currency_id)
                            ->orderBy('id','desc')->select('rate','bou_rate','sold_rate')->first();
        
        return $rate?$rate:['rate' =>0,'bou_rate'=>0,'sold_rate=>0'];
    }

        //find user commission
    public function commission($amount){
        $userCommission = Commission::where('user_id',$this->id)->whereRaw
                                    ('? between start_from and end_at', [$amount])->select('value','agent_quota')->first();

        $defaulCommission = Commission::where('user_id',0)->whereRaw
                                    ('? between start_from and end_at', [$amount])->select('value','agent_quota'     )->first();

        $commission = isset($userCommission)?$userCommission:$defaulCommission;
            return $commission;
    }

        //user is allow to make transaction

    public function canMakeTransaction(){
        //user status of 3 can not trade or are restricted
        if($this->is_active==2){
            return false;
        } else {
             return true;
        }
    }

    public function idUploaded(){
        if($this->photo_id== 0 || $this->photo_id == null){
            return false;
        }else{
            return true;
        }

    }

    public function credit($amount){
//current amount
         $newAmount = $this->credit - $amount;

        if($this->credit < $amount){ 
            return true;
        }else{ 
            $user = Auth::user()->update(['credit' => $newAmount]);;
            return false;
        }
    }

     public  function countTransaction(){

       return Transaction::where('user_id',$this->id)->count();

    }
 /*
    |--------------------------------------------------------------------------
    |Admin
    |--------------------------------------------------------------------------
 */
     public static function totalCustomers(){

       return User::where('type','customer')->count();

    }

     public static function activeCustomers(){

       return User::where('type','customer')->where('is_active','1')->count();

    }

     public static function inactiveCustomers(){

       return User::where('type','customer')->where('is_active','0')->count();

    }

      public static function totalAgents(){
        return User::where('type','agent')->count();
    }

      public static function activeAgents(){
        return User::where('type','agent')->where('is_active','1')->count();
    }
     public static function inactiveAgents(){
        return User::where('type','agent')->where('is_active','0')->count();
    }


     




    
}
