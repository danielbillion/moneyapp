<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Setting_transaction extends Model
{
    //
    protected $table = "laravel_setting_transactions";
    protected $fillable = [
        'two_copies',
        'allow_credit',
        'send_email',
        'agentname_onreceipt',
        'cap_transaction',
        'max_transaction',
        'allow_sms',
        'sms_email',
        'sms_hash',
        'allow_payment',
        'max_payment'

    ];

    public static function setting(){
        return Setting_transaction::orderBy('id','desc')->limit(1)->first();
    }

    public function setSendEmailAttribute($value){
        $this->attributes['send_email'] = $value==='on'?1:0;
    }
    public function setAllowCreditAttribute($value){
        $this->attributes['allow_credit'] = $value==='on'?1:0;
    }  
    public function setCapTransactionAttribute($value){
        $this->attributes['cap_transaction'] = $value==='on'?1:0;
    }   
    public function setAgentnameOnreceiptAttribute($value){
        $this->attributes['agentname_onreceipt'] = $value==='on'?1:0;
    }
    
    public function setAllowSmsAttribute($value){
        $this->attributes['allow_sms'] = $value==='on'?1:0;
    } 
    public function setAllowPaymentAttribute($value){
        $this->attributes['allow_payment'] = $value==='on'?1:0;
    } 
    public function setMaxTransactionAttribute($value){
        $this->attributes['max_transaction'] = $value===''?1500:$value;
    } 
    public function setTwoCopiesAttribute($value){
        $this->attributes['two_copies'] = $value==='on'?1:0;
    } 


    /* ==========================================================
                    Methods        ========================== */

    static public function capTransaction(){
    	return setting_transaction::orderBy('id', 'desc')->pluck('cap_transaction')->first();
    }

    static public function capValue(){
    	return setting_transaction::orderBy('id', 'desc')->pluck('max_transaction')->first();
    }
     static public function allowCredit(){
    	if(setting_transaction::orderBy('id', 'desc')->pluck('allow_credit')->first() == 1){
    		return true;
    	}else{
    		return false;
    	}
    	
    }



}
