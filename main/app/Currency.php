<?php
use App\Rate;
namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    //
    protected $table = "laravel_currency";
   protected  $fillable = [
        'user_id',
        'origin',
        'origin_symbol',
        'destination',
        'destination_symbol',
        'code',
        'income_category',
        'status'
    ];

    public static function rates(){
        $currencies = Currency::whereStatus(1)
                    ->select('id','destination_symbol','destination','income_category')
                    ->get();
        $rates = [];
        foreach($currencies as $currency){
          if($currency->income_category === 'commission'){
                $currency_rate = Rate::where('currency_id', $currency->id)
                        ->orderBy('id','desc')->limit(1)->pluck('rate')->first();
                 $rateItem = new \stdClass();       
                $rateItem->rate =  $currency_rate;
                $rateItem->destination_symbol =  $currency->destination_symbol;               
                $rateItem->destination =$currency->destination;
                $rates[] = $rateItem;
             }else{
                $currency_rate = Rate::where('currency_id', $currency->id)
                ->orderBy('id','desc')->limit(1)->pluck('bou_rate')->first();

                $rateItem = new \stdClass();       
                $rateItem->rate =  $currency_rate;
                $rateItem->destination_symbol =  $currency->destination_symbol;               
                $rateItem->destination =$currency->destination;
                $rates[] = $rateItem;
             }
        }

        return $rates;
        
    }
    public static function list(){
        return Currency::whereStatus(1)->pluck('destination','id')->toArray();
    }
    
}
