<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    //
    protected $table = "laravel_banks";
   protected  $fillable = [
   	'name','status'
    ];
}
