<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    //
    protected $table = "laravel_payments";
    protected $fillable = ['ref','amount','for','mode','user_type','user_id'];


    //Relationship
    public function paymentable(){
        return $this->morphTo;
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public static function search($value){
                 $from = date($value['from']); $to = date($value['to']);

                if($value['ref']===null || $value['ref']  ==="" ){
                
                    $Payments = Payments::whereDate('created_at','>=', $from)
                                                ->whereDate('created_at','<=', $to)
                                                ->where('mode',$value['type'])
                                                ->paginate(15);
                                            
                    }else{
                            $Payments = Payments::whereDate('created_at','>=', $from)
                                                ->where('ref',$value['ref'])
                                                    ->paginate(15);
                    }
            return $Payments;   
    
    }

}
