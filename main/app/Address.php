<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    //

    protected $table = "laravel_addresses";
    protected $fillable = [
    	'postcode','town','county','country','address','user_id'
    ];

    public function agentcustomer(){
    	return $this->hasOne('App\AgentCustomer');
    }
}
