<?php

namespace App\Http\Middleware;
use Auth;
use Closure;

class adminWithCustomerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         if(Auth::check()){
            if(Auth::user()->type==='admin' || Auth::user()->type==='agent'){
                 return $next($request);
            }
        }

        return redirect('/');
    }
}
