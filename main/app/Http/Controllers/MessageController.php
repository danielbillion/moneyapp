<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Mail\AdminToUserMail;
use Illuminate\Support\Facades\Mail;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null)
    {
        $member = User::whereId($id)->first();
        return view('admin.message',compact('member'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function validate_mobile($type,$id){
        if(isset($type) && isset($id)){ 
            $ref= str_random(6).\Auth::id();
            $request = new \stdClass();
                if($type === 'user'){
                    $request->mobile = \App\User::whereId($id)->value('mobile');
                    \App\User::find($id)->validPhone()->update(['code'=>$ref]);
                }else{
                    $request->mobile = \App\AgentCustomer::whereId($id)->value('mobile');
                    \App\AgentCustomer::find($id)->validPhone()->update(['code'=>$ref]);
                }
            $request->body = "Your verification code: ".$ref;
            $this->sendSMS($request);
            \Session::flash('message','sms successfully sent, please check your phone');
        }
                return redirect('/');
    }

    // public function enter_code($type,$id){
    //         return "entering code";
    // }

    public function confirm_code(Request $request){
       //return $request->all();
            if($request->type === 'user'){
                $value = \App\User::findorfail($request->id)
                 ->validPhone()->whereCode($request->code)->update(['is_valid'=>1]);
                 
                 
            }else{
                $value = \App\AgentCustomer::findorfail($request->id)
                    ->validPhone()->whereCode($request->code)->update(['is_valid'=>1]);
               
            }
            $value?\Session::flash('message','sms update sucessful'):\Session::flash('error','Value entered is incorrect');
           return redirect('/');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
     {
             $input = $request->all();
                if($request->type==='EMAIL'){
                   $this->sendEmail($request);      
                }else{
                    $this->sendSMS($request);
                }
            return redirect('members');
        }

        public function sendEmail($request){
            $mail = new \stdClass();
            $mail->subject =  $request->subject;
            $mail->body =  $request->body;
            $mail->sender = env('MAIL_FROM_NAME');
            $mail->receiver = $request->receiver;

         Mail::to($request->email)->send(new AdminToUserMail($mail));
        }

        public function sendSMS($request){
                // Authorisation details.
            $username = env('CLIENT_SMS_USERNAME');
            $hash = env('CLIENT_SMS_HASH');

	        // Config variables. Consult http://api.txtlocal.com/docs for more info.
	        $test = "0";

                    // Data for text message. This is the text message data.
            $sender = env('APP_NAME'); // This is who the message appears to be from.
            $numbers = $request->mobile; // A single number or a comma-seperated list of numbers
            $message = $request->body;;

            // 612 chars or less
            // A single number or a comma-seperated list of numbers

            $message = urlencode($message);
            $data = "username=".$username."&hash=".$hash."&message=".$message."&sender=".$sender."&numbers=".$numbers."&test=".$test;
            $ch = curl_init('http://api.txtlocal.com/send/?');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch); // This is the result from the API
            curl_close($ch);
    

        }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
