<?php
namespace App\Http\Controllers;
use App\Http\Requests\CurrencyCreateRequest;
use App\Http\Requests\CurrencyRequest;
use Illuminate\Http\Request;

use App\Currency;
use Auth;
use App\AgentCustomer;
class CurrenciesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $currencies =Currency::whereStatus(1)->paginate();
        $destinations = Currency::whereStatus(0)->pluck('destination','id')->toArray();
        
        $income_types = ['commission'=>'commission','profit'=>'profit'];
        return view('admin.currency.index',compact('currencies','income_types','destinations'));
      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CurrencyCreateRequest $request)
    {
       Currency::findorfail($request->destination)->update(['status' => 1]);
       \Session::flash('message','New  Currency Succesfully Added');
       return redirect('/');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return response()->json(Currency::whereId($id)->first());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $currencies = Currency::whereStatus(1)->paginate();
        $income_types = ['commission'=>'commission','profit'=>'profit'];
        $currency_value = Currency::find($id);
        return view('admin.currency.edit',compact('currencies','currency_value','income_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CurrencyRequest $request, $id)
    {
        
        Currency::findorfail($request->id)
                                    ->update(['status' =>$request->status,
                                    'income_category'=>$request->income_category]);
       \Session::flash('message',' Succesfully Updated');
       return redirect('/');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Currency::findorfail($id)->delete();
        \Session::flash('currency successfully deleted');
       return  redirect('/');
    }

    public function sender($id){
        if(Auth::user()->type ==='customer'){
            return response()->json(Auth::user()->currency() );
        }
        else{
            $sender = AgentCustomer::find($id);
            return response()->json( $sender->currency() );
        }
    }

    
}
