<?php

namespace App\Http\Controllers\N2UK;
use App\N2UK\Transaction;
use App\N2UK\Receiver;
use App\N2UK\Bank;
use App\N2UK\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\n2uk\TransactionRequest;
use Auth;
use Illuminate\Support\Str;

class TransactionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
         $transactions = Transaction::orderBy('created_at','DESC')->paginate(25);
        return view('n2uk.transactions.index',compact('transactions'));
       
    }
    
    public function post_search(Request $request)
    {
      $value = ['from'=>$request->from,'to'=>$request->to,'query_string'=>$request->query_string];
        $transactions =Transaction::TransSearch('post_search', $value);

        return view('n2uk.transactions.index',compact('transactions'));
        return $value;
    }

      public function list($id)
    {
        //
        $transactions = Transaction::whereUser_id($id)->paginate(25);
        return view('n2uk.transactions.index',compact('transactions'));
    }

      public function receipt($id)
    {
        //

        $transactions = Transaction::whereUser_id($id)->paginate(25);
        return view('n2uk.transactions.index',compact('transactions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
 
      public function transfer($id=null,$id2=null)
    {
      

        $senderId = $id == 0?0:$id;
        $receiverId = $id2 == 0?0:$id2;
        $sender =$id == 0?[]:
                Customer::whereId($id)->pluck('name','id')->toArray();
        $receiver =$id2 == 0?[]:
                Receiver::whereId($id2)->pluck('fname','id')->toArray();
       
        return view('n2uk.transactions.create',
                compact('receiverId','senderId','sender','receiver'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TransactionRequest $request)
    {
        //
        $Input = $request->all();
        
        // $receiver = firstOrCreate([
        //                 'fname'=>$input['receiver_fname'],
        //                 'lname'=>$input['receiver_lname'],
        //                 'phone'=>$input['receiver_phone'],
        //                 'ng_bank'=>$input['ng_bank'],
        //                 'ng_account_no'=>$input['ng_account_no'],
        //                 'ng_account_name'=>$input['ng_account_name'],
        //                 'uk_bank'=>$input['uk_bank'],
        //                 'uk_account_no'=>$input['uk_account_no'],
        //                 'uk_sort_code'=>$input['uk_sort_code'],
        //                  ]);
        Transaction::create([
            'transfer_code'=>Str::Random(8),
            'user_id'=>Auth::id(),
            'sender_id'=>$Input['sender_id'],
            'receiver_id' =>$Input['receiver_id'],
            'amount_pounds'=>$Input['amount_pounds'],
            'amount_naira'=>$Input['amount_naira'],
            'bou_rate'=>$Input['bou_rate'],
            'sold_rate'=>$Input['sold_rate'],
            'margin'=>$Input['margin'],
        ]);

        \Session::flash('message','Successsfuly sent');
        return redirect('transactions_n2uk');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $bank_ng = $bank = Bank::whereType('ng')->pluck('name','name')->toArray();
        $bank_uk = $bank = Bank::whereType('uk')->pluck('name','name')->toArray();
        $transaction = Transaction::findorfail($id);
        return view('n2uk.transactions.edit',compact('transaction','bank_ng','bank_uk'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $input  = $request->all();
        Transaction::whereId($id)->first()->update($input);
        
        
        \Session::flash('','N2UK Transaction Successfully Updated');
        return redirect('transactions_n2uk');
    }

    public function turnover($id){
        $turnovers= Transaction::turnover();
        $chart= Transaction::chart();
               
         return view('transactions.turnover',compact('turnovers','chart'));
       //return $chart;
   }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $transaction = Transaction::findorfail($id);
        $transaction->delete();
        \Session::flash('message','Transaction N2UK Successfully Deleted');
        return redirect('transactions_n2uk');
    }
}
