<?php

namespace App\Http\Controllers\N2UK;
use App\N2UK\Rate;
use Illuminate\Http\Request;
use App\Http\Requests\n2uk\RateRequest;
use App\Http\Controllers\Controller;

class RatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user_id ="0";
        $rates = Rate::orderBy('created_at', 'DESC')->paginate(15);
        return view('n2uk.rate',compact('rates','user_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RateRequest $request)
    {
        //

        $input = $request->all();
        Rate::create($input);
        \Session::flash('message','New Rate Added');
        return redirect('rates_n2uk');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $rate = Rate::findorfail($id);
        $rate->delete();
        \Session::flash('message',' Rate Successfully Deleted');
        return redirect('rate');
    }
}
