<?php

namespace App\Http\Controllers\N2UK;
use App\N2UK\Customer;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests\n2uk\CustomerRequest;
use App\Http\Controllers\Controller;
use DB;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
         $customers = Customer::paginate('10');
        return view('n2uk.customers.index',compact('customers'));
    }

    //autocomplete for search
    public function autocomplete($id = null )
    {
        return                     
        response()->json(Customer::select(DB::raw("CONCAT(fname,'-',lname) AS value"),'id')
                        ->get());
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('n2uk.customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     private function checkemptyadress($input){
            $input['postcode'] = $input['postcode'] ==""?"none": $input['postcode'];
            $input['number'] = $input['number'] ==""?"": $input['number'];
            $input['address'] = $input['address'] ==""?$input['search']: $input['address'];
            $input['town'] =   $input['town'] ==""?"": $input['town'];
            $input['county'] = $input['county'] ==""?"": $input['county'];
            $input['country'] = $input['country'] ==""?"": $input['country'];
            $input['address'] =  $input['number']. " ".$input['address']. " ".$input['town']." ".$input['county']." ".$input['country'];
            return $input;

    }
    public function store(CustomerRequest $request)
    {
        //
        //check for empty inputs
            $input = $request->all();
            $input['name']= $input['fname']." ".$input['lname'];
            $input = $this->checkemptyadress($input);
             
           $input['user_id'] = Auth::id();

            Customer::create($input);
            \Session::flash('message','Your new Customer sucessfully created');
             return redirect('customers_n2uk');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customers = Customer::whereId($id)->paginate('10');
        return view('n2uk.customers.index',compact('customers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $customer = Customer::findorfail($id);
        return view('n2uk.customers.edit',compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
            $input = $request->all();
            $input = $this->checkemptyadress($input);
            Customer::whereId($id)->first()->update($input);


           // $agentcustomer->update($input);
            \Session::flash('message','successfully updated');
             return redirect('customers_n2uk');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $customer = Customer::findorfail($id);
        $customer->delete();
        \Session::flash('message','Customer Successfully Deleted');
        return redirect('customers_n2uk');
    }
}
