<?php

namespace App\Http\Controllers\N2UK;
use App\N2UK\Bank;
use App\N2Uk\Receiver;
use App\N2Uk\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class ReceiversController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null)
    {
        $receivers = Receiver::whereSender_id($id)->paginate(25);
        return view('n2uk.receivers.index',compact('receivers'));
    }

    public function autocomplete($id)
    {
        return response()->json(Receiver::whereSender_id($id)
                                      ->select(DB::raw("CONCAT(fname,'-',lname) AS value"),'id')          
                                        ->get());
       
      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        //
        $bank_ng = $bank = Bank::whereType('ng')->pluck('name','name')->toArray();
         $bank_uk = $bank = Bank::whereType('uk')->pluck('name','name')->toArray();
        $customer = Customer::findorfail($id);
        return view('n2uk.receivers.create',compact('customer','bank_ng','bank_uk'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input = $request->all();
        Receiver::create($input);
        \Session::flash('message','New N2UK Receiver Successfully Created');
        return redirect('customers_n2uk');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $receivers = Receiver::whereId($id)->paginate(25);
        return view('n2uk.receivers.index',compact('receivers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $bank_ng = $bank = Bank::whereType('ng')->pluck('name','name')->toArray();
        $bank_uk = $bank = Bank::whereType('uk')->pluck('name','name')->toArray();
        $receiver = Receiver::findorfail($id);
        return view('n2uk.receivers.edit',compact('receiver','bank_ng','bank_uk'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $input = $request->all();
        Receiver::findorfail($id)->first()->update($input);
        \Session::flash('message','New N2UK Receiver Successfully Created');
        return redirect('customers_n2uk');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
         $receiver = Receiver::findorfail($id);
        $receiver->delete();
        \Session::flash('message','Receiver N2UK Successfully Deleted');
        return redirect('customers_n2uk');
    }
}
