<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Currency;
use App\Address;
use App\Transaction;
use App\AgentCustomer;
use App\Receiver;
use App\Subscription;
use Auth;
use App\Http\Requests\UsersRequest;

class UsersController extends Controller
{
    public $members;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response7
     */

    public function __construct(){
        $this->members = User::paginate('10');  
    }

    public function home(){
       $subscription =  Subscription::orderBy('id','desc')->limit(1)->first();
         $id = Auth::id();$type = Auth::user()->type;
      
      if($type ==='admin' || $type ==='manager'){
            $customers =AgentCustomer::take(10)->orderBy('created_at','DESC')->get(); 
            $transactions =Transaction::take(10)->orderBy('created_at','DESC')->get();
            $receivers = [];
        }else{
            
        $transactions =Transaction::whereUser_id($id)->
                                    take(10)->orderBy('created_at','DESC')->get();
        $customers =$type=='agent'?
                                    AgentCustomer::whereUser_id($id)->take(10)
                                    ->orderBy('created_at','DESC')->get():[];
              
          $receivers =$type=='customer'?
              Receiver::whereUser_id($id)->take(10)->orderBy('created_at','DESC')->get():[];   
        }
    return view('landing_page',compact('customers','transactions','receivers','subscription'));
    }


    public function index($id = null)
    {
         $header = null;
         $members = $id == null? User::orderBy('type')->paginate('25'):
         User::whereId($id)->paginate('25');
        return view('admin.members',compact('members','header'));
    }

     public function agent()
    {
         $header = "agent";
         $members = User::where('type','agent')->paginate('10');
        return view('admin.members',compact('members','header'));
    }

     public function customer()
    {
         $header = "customer";
         $members = User::where('type','customer')->paginate('10'); 
        return view('admin.members',compact('members','header'));
    }

    public function search($id){
        return response()->json(User::
            where('fname','like', '%'. $id. '%')
            ->orWhere('lname','like', '%' . $id. '%' )
            ->select(DB::raw("CONCAT(fname,' ',lname) AS value"),'id')
            ->get());
    //     $results = array();
    //      $queries =   User::where('fname','like', '%'. $id. '%')->
    //             orWhere('lname','like', '%' . $id. '%' )->
    //             get();

    //             foreach ($queries as $query)
    // {
    //     $results[] = [ 'id' => $query->id, 'value' => $query->fname.' '.$query->lname ];
    // }
    //         return ($results);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    public function password_edit($id)
    {
        return view('user.password');
    }
    public function passwordUpdate(Request $request, $id)
    {
         $email = User::findorfail($id)->email;

        $credentials = ['email'=>$email,'password'=>$request->password];

            if (Auth::attempt($credentials)) {
                 $input = ['password' => bcrypt($request->new_password)];
                 User::whereId($id)->first()->update($input);
                    
         \Session::flash('message','New Password Successfully Updated'); 
        return redirect('/');
            }
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $header = null;
         $members =!isset($id)?User::paginate('10'):
         User::whereId($id)->paginate('10');
        return view('admin.members',compact('members','header'));

        
  }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $currencies = Currency::pluck('code','id')->toArray();
        $user = User::find($id);
        return view('user.edit',compact('user','currencies'));
      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    private function addressInput($request){
        return array(
                    
                    'postcode'=>isset($request->postcode)?$request->postcode:'',
                    'address'=>isset($request->address1)?
                             $request->number." ".$request->address1:'',
                    'town'=>isset($request->town)?$request->town:'',
                     'county'=>isset($request->county)?$request->county:'',
                    'country'=>isset($request->country)?$request->country:'',

                            );
    }
    public function update(UsersRequest $request, $id)
    {
        $input = $request->all();
        $address = Address::updateOrCreate(['user_id'=>$id],
                                $this->addressInput($request)); // Address mode

        User::findorfail($id)->validPhone()->updateOrCreate(['phoneable_id'=>$id],$input); //phone model
        User::whereId($id)->first()->update($input);
         \Session::flash('message','Successfully Updated'); 
        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

         User::findorfail($id)->delete();
         \Session::flash('message','Successfully Deleted'); 
        return redirect('admin');
    }

    public function status($id){
        $member =User::findorfail($id);
        $notes = $member->message()->paginate('25');
        return view('user.status',compact('member','notes'));
    }

    public function updatestatus(Request $request, $id){
         if  (Auth::attempt(['email' => Auth::user()->email, 'password' => $request->pass])) {
                    $input  = $request->all();
    //Update
                User::whereId($id)->first()->update($input);
    //Add note
                $user = User::find($id)->message()->create($input);
                //User::whereId($id)->message()->create($input);
                    \Session::flash('message',' Successfully Updated Member');
                        return redirect('members');
            }
        else{
                \Session::flash('error','Incorrect Password');
                         return redirect()->back();
                }
    }
}
