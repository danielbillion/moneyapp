<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
/** All Paypal Details class **/
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Redirect;
use Session;
use URL;
use App\Agent_Payment;
use App\Payments;
use App\Outstanding;
use App\User;
use App\Subscription;
use App\Agent;
use Auth;

class PaymentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $_api_context;
    private $_api_context_client;
    public $details;

    public function __construct()
    {
        $this->details = null;
        /** PayPal api context for developer **/
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);

         /** PayPal api context for client **/
         $paypal_conf_client= \Config::get('paypal_client');
         $this->_api_context_client = new ApiContext(new OAuthTokenCredential(
             $paypal_conf_client['client_id'],
             $paypal_conf_client['secret'])
         );
         $this->_api_context_client->setConfig($paypal_conf_client['settings']);
    
}
    public function index($user_type = null)
    {
        $header = "";
        $payments = Payments::orderBy('created_at','desc')->paginate('25');
        return view('payments.index', compact('payments','header'));
        
    }
    public function user_type($type)
    {
        $header = $type;
        $payments = Payments::where('user_type',$type)->orderBy('created_at','desc')->paginate('25');
        return view('payments.index', compact('payments','header'));
          
    }

    public function post_search(Request $request){
        $header = null;
       $value = ['from'=>$request->from,'to'=>$request->to,'type'=>$request->type,'ref'=>$request->ref];
        $payments =Payments::search( $value);
        
        return view('payments.index', compact('payments','header'));
       // return $request->all();
    }

    

    public function pay($type,$id){
          if($type === 'transaction' ){
            $user_id = \App\Trans::whereId($id)->value('user_id');
            $amount = \App\Trans::whereId($id)->value('amount') + \App\Trans::whereId($id)->value('commission');
          }
          else if($type === 'subscription'){
                $user_id = Subscription::whereId($id)->value('user_id');
                $amount = Subscription::whereId($id)->value('amount');
              }
            else if($type === 'outstanding'){
                $user_id = Outstanding::whereId($id)->value('user_id');
                $amount = Outstanding::whereId($id)->value('amount');
              }
              else{
                        $user_id = null;
                        $amount = null;
                        $type = null;
                        $paid_by = "unknown";
                }
          
          $ref = str_random(6).Auth::id();
        $paid_by = User::whereId($user_id)->value('name');
        return view('payments.pay',compact('user_id','amount','id','type','paid_by','ref'));
    }

    public function part_pay($user_id,$amount){
    
          $type = "Part";
          $paid_by = User::whereId($user_id)->value('name');
      
      return view('payments.pay',compact('user_id','amount','type','paid_by'));
  }

    public function stripe_store(Request $request){
       $subscription = Subscription::findorfail(1);
       
       \Stripe\Stripe::setApiKey ( $request->for == 'subscription'?env('STRIPE_PRIVATE_KEY'):env('CLIENT_STRIPE_PRIVATE_KEY') );
	try {
		\Stripe\Charge::create ( array (
				"amount" => $request->amount * 100,
				"currency" => "GBP",
				"source" => $request->stripeToken, // obtained with Stripe.js
				"description" => $request->for 
        ) );
        $this->createPayment($request);
		Session::flash ( 'message', 'Payment successfully !' );
		return redirect('/');
	} catch ( \Exception $e ) {
		Session::flash ( 'error', "Error! Please Try again." );
		return Redirect::back ();
	}
     
    }

    public function paypal_store(Request $request)
    {
        $this->details = $request; 
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
       
        $item_1 = new Item();
        $item_1->setName('Subscription-Paypal') /** item name **/
            ->setCurrency('GBP')
            ->setQuantity(1)
            ->setPrice($request->get('amount')); /** unit price **/
        $item_list = new ItemList();
        $item_list->setItems(array($item_1));
       
        $amount = new Amount();
        $amount->setCurrency('GBP')
            ->setTotal($request->get('amount'));
        
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Your transaction description');
        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::to('/status')) /** Specify return URL **/
            ->setCancelUrl(URL::to('/status'));
        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
        /** dd($payment->create($this->_api_context));exit; **/
        try {
            $payment->create($request->for == 'subscription'?
                        $this->_api_context:$this->_api_context_client);
                        $this->details = $request;
    //create payment
                 // $this->createPayment($request); 
                 //$this->getPaymentStatus($request);
                  //Session::flash ( 'message', 'Payment successfully !' );
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                \Session::put('error', 'Connection timeout');
                return Redirect::to('/');
            } else {
                \Session::put('error', 'Some error occur, sorry for inconvenient');
                return Redirect::to('/');
            }
        }
        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());
        if (isset($redirect_url)) {
            /** redirect to paypal **/
            return Redirect::away($redirect_url);
        }
        \Session::put('error', 'Unknown error occurred');
        return Redirect::to('/');
           
    }

    public function getPaymentStatus($request = null)
    {
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');
        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            \Session::put('error', 'Payment failed');
            return Redirect::to('/');
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);
        if ($result->getState() == 'approved') {
            //create payment
            $this->createPayment($this->details); 
            \Session::flash('message', 'Payment Successful');
            return Redirect::to('payments');
        }
        \Session::flash('error', 'Payment failed');
        return Redirect::to('payments');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function createPayment($request){
     if($request->for === 'subscription'){
             $subscription = Subscription::findorfail($request['id']);
                    
        $subscription->payment()->create($request->all()); //create new payment
                         
            $subscription->update(['paid'=> 1]);        //update
        //new subscription
         Subscription ::create(['user_id'=>0,'start_at'=>Date('d-m-Y'), 
            'ref'=>str_random(6).Date('Y'), 'others'=>0, 'payment_mode'=>'cash', 'paid'=>0,
            'months_duration'=>'24','notice'=>0,'amount'=>$subscription->amount, ]);
            }
    else if($request->for === 'transaction'){
                    $transaction = \App\Trans::findorfail($request['id']);
                     $transaction->payment()->create($request->all());
                 }
     else{
                    $outstanding = \App\Outstanding::findorfail($request['id']);
                     $outstanding->payment()->create($request->all());  
                     $outstanding->update(['transaction_paid'=>1]);
             }
             
        
         \Session::flash('message','Payment Successful'); 
      return true;    }
    public function store(Request $request)
    {
        //
        //      $input = $request->all();
        //      $amount = $request->amount;
        //      $total = $request->total;
        
        //     $input['fully_paid'] = $amount >= $total?1:0;
        //     $input['balance'] = $total - $amount;
        //      $input['fully_paid'] = 0;
         
        //      $payment = Agent_Payment::create($input);
        //       Transaction::whereUser_id($request->user_id)
        //       ->whereAgent_payment_id('none')
        //       ->update(['agent_payment_id'=>$payment->id]);
        
        // \Session::flash('message','Successfully Updated'); 
        // return redirect('payments');
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        // $user = User::find($id)->first();
        // $payments = User::find($id)->payments()->get();
        // return view('payments.paid',compact('payments','user'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        // $member = Agent::find($id);
        // return view('payments.create',compact('member','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
