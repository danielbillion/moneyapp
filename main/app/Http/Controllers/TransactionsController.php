<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use Auth;
use App\Transaction;
use App\Currency;
use App\Bank;
use App\Photo;
use App\User;
use App\AgentCustomer;
use App\Receiver;
use App\Outstanding;
use App\Setting_transaction;
use App\Setting_business;
use App\Http\Requests\TransactionsRequest;
use App\Mail\transactionEmail;
use Illuminate\Support\Facades\Mail;
use MessageController;

class TransactionsController extends Controller
{
    public $where;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($user_id = null)
    {
         $role =  Auth::user()->type;
         $currencies = Currency::pluck('destination','id')->toArray();

         $header = null;
         if($role == 'admin' || $role == 'manager' ){
            $transactions =Transaction::orderBy('created_at', 'DESC')->paginate(15);
            
        }else{
         $transactions =Transaction::whereType($role)->whereUser_id($user_id?$user_id:Auth::id())
                        ->orderBy('created_at', 'DESC')->paginate(15);  
                        $total_trans = Transaction::sum('amount');  
            }
            
        return view('transactions.index',compact('transactions','header','currencies'));
    }



     public function agentsTransactions(){
        $header = "agent";
        $currencies = Currency::pluck('destination','id')->toArray();

        $transactions =Transaction::where('type','agent')->orderBy('created_at', 'DESC')->paginate(15);
        return view('transactions.index',compact('transactions','header','currencies'));
    }



    public function customersTransactions(){
        $header = "customer";
        $currencies = Currency::pluck('destination','id')->toArray();

       $transactions =Transaction::where('type','customer')->orderBy('created_at', 'DESC')->paginate(15);
        return view('transactions.index',compact('transactions','header','currencies'));
    }



    public function search($type = null, $value = null)
    {
         $header = null;
         $currencies = Currency::pluck('destination','id')->toArray();

        $transactions =Auth::user()->type=='admin' || Auth::user()->type=='manager'?
                                    Transaction::adminTransSearch($type, $value)
                                    :Transaction::membersTransSearch($type, $value);

        return view('transactions.index',compact('transactions','header','currencies'));
    }
    private function sourceData($request){
        $value = [
            'from'=>$request['from'],
            'to'=>$request['to'],
            'status'=>$request['status'],
            'query_string'=>$request['query_string'],
            'currency_id'=>$request['currency_id']];

        return $value;
    }

    public function post_search(Request $request)
    {   
        
        $header = null;
        $currencies = Currency::pluck('destination','id')->toArray();
        //method for search
        $type = Auth::user()->type;
        $modelMethod =$type =='admin' || $type=='manager'?'adminTransSearch':'membersTransSearch';
       
     
    
                if ($request->search ==='new') {
                    $request->session()->forget(['from', 'to','query_string','currency_id']);
                    $value = $this->sourceData($request->all());
            
                    session($value);
                    $transactions=  Transaction::$modelMethod('post_search', $value);
                    
                  
                 }else{
                    $value = $this->sourceData($request->session()->all());
                    $transactions=  Transaction::$modelMethod('post_search', $value);
                    }
                
                      
              
       return view('transactions.index',compact('transactions','header','currencies'));
     
    }


    public function autocompleteTips($value){
             return response()->json(Transaction::
            where('receipt_number','like', '%'. $value. '%')
            ->orWhere('receiver_phone','like', '%' . $value. '%' )
            ->select(DB::raw("CONCAT(receipt_number,' ',created_at,'  £',amount) AS value"),'id')
            ->get());
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function create($sender = null, $receiver = null)
    {
     
        $senderId = $sender == 0?0:$sender;
        $receiverId = $receiver == 0?0:$receiver;
        return view('transactions.create',
                compact('receiverId','senderId'));
  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

   
    public function summary(TransactionsRequest $request)
    {
        $transaction = $request->all();
        
        return redirect()->route('transactions.show', $transaction);
        
    }

    public function show(Request $request, $id)
    {
     $transaction = $request->all();
          // return $request->all();
      return view('transactions.summary.transaction_summary',compact('transaction'));
    }

    public function store(TransactionsRequest $request)
    {
        
        $user = Auth::user();
          $id = Auth::id();
          $newCredit = $request->amount - $user->credit;
         if(!$this->validateTransactionSetting($request)){   
           return redirect()->back()->withInput();
         }else{
            $input = $request->all();
            $input['receipt_number'] = str_random(6).Auth::id();
                
            $transaction =  $user->transaction()->create($input );
            $this->notification($transaction->id);  //send notification
            $this->createOutstanding($transaction->id,$input);
             
                if($input['allow_payment']== 1){
                    return redirect()->route('payments.pay',['transaction', $transaction->id]);
                }else{   \Session::flash('message','New Trasaction successfully completed');
                    return redirect('transactions');}
            
                
         }
         
       
    }

    private function notification($id){
        $transaction = \App\Transaction::find($id);
        $sender =  $transaction->sender->name;
        if(\App\Setting_transaction::setting()->send_email == '1'){

            $title = "Transaction Receipt";
         Mail::to($transaction->sender->email)->send(new transactionEmail($transaction));
            return response()->json(['message' => 'Request completed']);
        }
        else if (\App\Setting_transaction::setting()->allow_sms ===1){
                    //Authorisation details.
                    $message = new MessageController;
                    $items = new \stdClass();
                    $item->mobile = $transaction->sender()->mobile;
                    $item->body = "This is to confirm a transaction of £".$transaction->first()->amount.
                                        " was sent with receipt no: ".$transaction->first()->receipt_number
                                        ." thank you for your patronage ";
                        $message->sendSMS($item);
        }

    }

    private function createOutstanding($id,$input){
        Outstanding::create([
                'transaction_id'=> $id,
                'user_id'=> $input['user_id'],
                'amount'=> $input['amount'],
                'agent_commission'=> $input['agent_commission'],
                'manager_id' => 0,
                'admin_id' => 0,
                'transaction_paid'=>0,
                'commission_paid' =>0
        ]);
    }

     private function validateTransactionSetting($request){
                 $user = Auth::user();
                 $sender_id = $request->sender_id;
//is agent allow to make transaction
            if(!$user->canMakeTransaction()){
                 \Session::flash('error','You can are not allow to perform any transaction,Please contact admin');
                 return false;
//check if transaction is capped and customer uplod identity
            }else if(Setting_transaction::capTransaction()==1 && $this->agentCustomerHasPhoto($sender_id) ){
                 \Session::flash('error','Transaction is capped to '
                    .Setting_transaction::capValue().', please upload id');
                 return false;
            }else if (Setting_transaction::allowCredit()==1 && $user->credit($request->amount)){
                    //$user->update(['credit' => 512]);
                \Session::flash('error','You have insufficient credit to perform this transaction,Please contact admin');
            }


            else{
                return true;
            }
        
    }
//if customer hs got photo in photo table
    private function agentCustomerHasPhoto($sender_id){
            $photo = Photo::whereId($sender_id)->first();
            if($photo == null){
                return true;
            }else{
                return false;
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         //
        $bank = Bank::where('status', 'b')->pluck('name','name')->toArray();
        $transaction = Transaction::findorfail($id);
        $customer = $transaction->agentCustomer;
        return view('transactions.edit',compact('transaction','customer','bank'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         //
        $input  = $request->all();
        Receiver::whereId($request
                    ->receiver_id)->first()
                    ->update(['phone'=>$request->receiver_phone,
                                'id'=>$request->receiver_id,
                                'transfer_type'=>$request->transfer_type,
                                'bank' =>$request->bank,
                                'account_number'=>$request->account_number]);
        Transaction::whereId($id)->update(['receiver_id'=>$request->receiver_id]);
        \Session::flash('message','Transaction Successfully Updated');
        return redirect('transactions');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $transaction = Transaction::findorfail($id);
        $transaction->delete();
        \Session::flash('danger','Transaction  Successfully Cancelled');
        return redirect('/agent/customer-transactions');
    }





   

    public function status($id){
        $transaction =Transaction::findorfail($id);
        return view('transactions.status',compact('transaction'));
    }



    public function updatestatus(Request $request, $id){
        $transaction =Transaction::findorfail($id);
         if  (Auth::attempt(['email' => Auth::user()->email, 'password' => $request->password])) {
                    $input  = $request->all();
//update transaction
                Transaction::whereId($id)->first()->update($input);

  //create message
                $transaction->message()->create($input);
                    \Session::flash('message',' Successfully Updated Transaction');
                        return redirect('transactions');
            }
        else{
                \Session::flash('error','Incorrect Password');
                         return redirect()->back();
                }
    }

    public function turnover($id){
         $turnovers= Transaction::turnover();
         $chart= Transaction::chart();
                
          return view('transactions.turnover',compact('turnovers','chart'));
        //return $chart;
    }

    public function commission($id){
        return "commission here";
    }

    public function receipt($id){
        $business = Setting_business::orderBy('id', 'desc')->first();
        $transaction =Transaction::findorfail($id);
        return view('transactions.receipt',compact('transaction','business'));
    }
}
