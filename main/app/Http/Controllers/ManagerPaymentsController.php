<?php

namespace App\Http\Controllers;
use App\Manager;
use App\Manager_Payment;
use App\Agent_Payment;
use Illuminate\Http\Request;
use App\Http\Requests\ManagerPaymentRequest;

class ManagerPaymentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
          return view('payment-manager.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ManagerPaymentRequest $request)
    {
        //
             $input = $request->all();
             $amount = $request->amount;
             $total = $request->total;
            
             $input['fully_paid'] = $amount >= $total?1:0;
             $input['balance'] = $total - $amount;
           
        $payment = Manager_Payment::create($input);
         Agent_Payment::whereManager_id($request->manager_id)
                      ->whereAdmin_payment_id(0)
                      ->update(['admin_payment_id'=>$payment->id]);

        \Session::flash('message','New Manager Payment Successful'); 
        return redirect('managerpayments');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
         $user = Manager::find($id)->first();
            $payments = Manager::find($id)->managerPayments()->get();
            return view('payments.paid',compact('payments','user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $manager = Manager::find($id);
        return view('payment-manager.create',compact('manager'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
