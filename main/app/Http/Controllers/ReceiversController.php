<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\AgentCustomer;
use App\Receiver;
use App\Bank;
use Auth;
use DB;
use App\Http\Requests\ReceiverRequest;



class ReceiversController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null, $return = null)
    {
        $id = isset($id)?$id:Auth::id();
        $type = Auth::user()->type;
       if(Auth::user()->type === 'customer'){
            $receivers = Receiver::where('user_id', Auth::id())
                                ->orderBy('created_at','DESC')->paginate(15); 
       } else{
             $receivers = Receiver::where('sender_id', $id)
                            ->orderBy('created_at','DESC')->paginate(15);
       }

         return $return?response()->json($receivers) 
                    : view('receivers.index',compact('receivers','id','type')) ;
    }

    public function autocomplete(Request $request,$type = null, $id = null,$req = null)
    {
        if($type === 'customer'){
            $receivers = Receiver::
            where('fname','like','%'.$req.'%')
            ->orwhere('lname','like','%'.$req.'%')
            ->orwhere('phone','like','%'.$req.'%')
            ->where('user_id', Auth::id())
                    ->select(DB::raw("concat(fname,'-',lname) as value") ,'id')->get(); 
       } else{
             $receivers = Receiver:: where('fname','like','%'.$req.'%')
                                    ->orwhere('lname','like','%'.$req.'%')
                                    ->orwhere('phone','like','%'.$req.'%')
                                    ->select(DB::raw("CONCAT(fname,'-',lname) as value") ,'id')
                                     ->where('sender_id', $id)
                            ->get(); 
       }

         return response()->json($receivers ) ;

  
     }

   

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id = null)
    {
        //
        $user =Auth::user(); 
        $bank = Bank::whereStatus('b')
                    ->pluck('name','name')->toArray();
        
        if($user->type ==='customer'){
            $sender = [Auth::id()=>Auth::user()->name];
        }else{
         $sender =AgentCustomer::
                whereId($id)->pluck('name','id')->toArray();
        }
    return view('receivers.create',compact('bank','id','sender'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReceiverRequest $request)
    {
        //
        $data = $request->all();
        //for customer act
        $data['sender_id'] =  !isset($data['sender_id'])?Auth::id():$data['sender_id'];

         Receiver::create($data);
         if(Auth::user()->type === 'customer'){
            return redirect()->route('receivers.customer',Auth::id());
         }else{
            return redirect()->route('receivers.sender',$data['sender_id']);
         }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = isset($id)?$id:'';
        $type = Auth::user()->type;
         $receivers = Receiver::where('id', $id)->paginate(10);
         return view('receivers.index',compact('receivers','type','id')) ;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         //
        $receiver = Receiver::findorfail($id);
        $bank = Bank::where('status', 'b')->pluck('name','name')->toArray();
        return view('receivers.edit',compact('bank','receiver'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ReceiverRequest $request, $id)
    {
        //
        //
         $receiver = Receiver::findorfail($id);
         $data = $request->all();
         $receiver->whereId($id)->first()->update($data);
         
         if(Auth::user()->type === 'customer'){
            return redirect()->route('receivers.customer',Auth::id());
         }else{
            return redirect()->route('receivers.sender',$data['sender_id']);
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
         $receiver = Receiver::findorfail($id)->delete();
         \Session::flash('message','Successfully Deleted');
         return redirect()->back();
    }

     /* __________ Api _________ */

    public function showCustomerReceivers(){

        $receivers = Receiver::where('agent_customer_id', $id)->take(10)->get();
        return view('agent.customers.receivers.index',compact('receivers'));
    
    }
}
