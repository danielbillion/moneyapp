<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AgentCustomer;
use App\User;
use App\Currency;
use App\Address;
use App\Photo;
use App\Http\Requests\AgentCustomerRequest;
use Carbon\Carbon;
use Auth;
use DB;

class AgentCustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null)
    {
        //
        $id = isset($id)?$id:Auth::id();

        $senders = AgentCustomer::where('user_id', $id)->orderBy('created_at','DESC')->paginate('30');

        
        return view('agent.customers.index',compact('senders'));
      
    }

     // Display a listing of the autocomplete request.
  
    public function autocomplete(Request $request,$id = null,$value)
    {

       return AgentCustomer::
            where('fname','like','%'.$value.'%')
            ->orwhere('lname','like','%'.$value.'%')
            ->orwhere('phone','like','%'.$value.'%')
            ->orwhere('mobile','like','%'.$value.'%')
            ->where('user_id', $id)
            ->select(DB::raw("CONCAT(fname,'-',lname) AS value"),'id')
            ->orderBy('fname','ASC')
             ->get();   
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $currencies = Currency::whereStatus(1)->pluck('destination','id')->toArray();
       
        return view('agent.customers.create',compact('currencies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     //set Attributes

     
    public function checkemptyadress($input){
             $input['number'] = $input['number'] ==""?"": $input['number'];
            $input['address'] = $input['address'] ==""?$input['search']: $input['address'];
            $input['town'] =   $input['town'] ==""?"": $input['town'];
            $input['county'] = $input['county'] ==""?"": $input['county'];
            $input['country'] = $input['country'] ==""?"": $input['country'];
            $input['address'] =  $input['number']. " ".$input['address']. " ".$input['town']." ".$input['county']." ".$input['country'];
            return $input;

    }

    public function ProcessPhoto($request){
          if($file_identity = $request->file('identity_proof') || $file_address = $request->file('address_proof') ){  
                if($file_identity = $request->file('identity_proof')){
                       $identity_name = time().$file_identity->getClientOriginalName();
                       $file_identity->move('images',$identity_name);
                     }
                if($file_address = $request->file('address_proof')){
                       $address_name = time().$file_address->getClientOriginalName();
                       $file_address->move('images',$address_name);
                     }
                     $identity_name= isset($identity_name)?$identity_name:"";
                     $address_name= isset($address_name)?$address_name:"";


            $photo = Photo::create(['identity_proof'=>$identity_name,'address_proof'=>$address_name]);
              $input['photo_id'] = $photo->id;

                return $photo->id;
            }

           
    }
    public function store(AgentCustomerRequest $request)
    {
           
           //check for empty inputs
            $input = $request->all();
            $input['name']= $input['fname']." ".$input['lname'];

            $input['mname']= $input['mname'] !==null?$input['mname']:'-';
            $input['phone']= $input['phone'] !==null?$input['phone']:'-';
            $input['email']= $input['email'] !==null?$input['email']:'-';

            $input = $this->checkemptyadress($input);
             
            $input['photo_id'] = '1';
           
            $address = Address::create($input);
            $input['address_id'] = $address->id;

            
            AgentCustomer::create($input);
            \Session::flash('message','Your new Customer sucessfully created');
             return redirect('/');
        // return $request->all();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $senders = AgentCustomer::where('id', $id)->paginate('30');
        return view('agent.customers.index',compact('senders'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) 
    {
        //
       
        $agentcustomer = AgentCustomer::findorfail($id);
        $address = $agentcustomer->address()->first();

        $currencies = Currency::whereStatus(1)->pluck('code','id')->toArray();
        return view('agent.customers.edit',compact('agentcustomer','address','currencies'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AgentCustomerRequest $request, $id)
    {
        //
            $input = $request->all();
            $input = $this->checkemptyadress($input);

            if($file_identity = $request->file('identity_proof') || $file_address = $request->file('address_proof') ){ 
                    $input['photo_id'] = $this->ProcessPhoto($request);
                }

            $agentcustomer = AgentCustomer::findorfail($id);
             $user = User::findorfail(1);
             
          
             if($agentcustomer->address_id !== 0){
                Address::whereId($agentcustomer->address_id)->first()->update($input);
             }
             
             $agentcustomer=$agentcustomer->whereId($id)->first()->update($input);


           // $agentcustomer->update($input);
            \Session::flash('message','successfully updated');
        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //

        AgentCustomer::findorfail($id)->delete();
         \Session::flash('message','Successfully Deleted'); 
        return redirect('/');
    }
    public function listCustomers($id){
        return response()->json(
            AgentCustomer::whereUser_id($id)->
            select('id','name','fname','lname')->get());
    }
}
