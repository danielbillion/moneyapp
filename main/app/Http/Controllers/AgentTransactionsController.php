<?php

namespace App\Http\Controllers;
use App\Transaction;
use Auth;
use App\Bank;
use App\Photo;
use App\User;
use App\AgentCustomer;
use App\setting_transaction;
use App\Http\Requests\agentTransactionRequest;
use Illuminate\Http\Request;

class AgentTransactionsController extends Controller
{
    public $user;
       
    /*
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
         $this->user = Auth::user();
    }



   
    public function index()
    {
        //
        $user = Auth::user();
        $transactions = $user->transactions()->orderBy('created_at', 'DESC')->paginate(5);
        return view('transactions.agent.index',compact('user','transactions'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()  { 
        $customer = Auth::user()->agentcustomer()->pluck('name','id')->toArray();
        return view('transactions.agent.create',compact('customer'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(agentTransactionRequest $request)
    {
        //
       
         $user = Auth::user();
          $id = Auth::id();
          $newCredit = $request->amount - $user->credit;
         if(!$this->validateTransactionSetting($request)){   
           return redirect()->back()->withInput();
         }else{
            $input = $request->all();
           // $user->update(['credit' => 412]);
            $input['receipt_number'] = str_random(6).Auth::id();
            $user->transactions()->create($input);
            \Session::flash('message','New Trasaction successfully completed');
            return redirect('agent/customer-transactions');
         }

    }

    private function validateTransactionSetting($request){
                 $user = Auth::user();
                 $sender_id = $request->sender_id;
//is agent allow to make transaction
            if(!$user->canMakeTransaction()){
                 \Session::flash('error','You can are not allow to perform any transaction,Please contact admin');
                 return false;
//check if transaction is capped and customer uplod identity
            }else if(setting_transaction::capTransaction()==1 && $this->agentCustomerHasPhoto($sender_id) ){
                 \Session::flash('error','Transaction is capped to '
                    .setting_transaction::capValue().', please upload id');
                 return false;
            }else if (setting_transaction::allowCredit()==1 && $user->credit($request->amount)){
                    //$user->update(['credit' => 512]);
                \Session::flash('error','You have insufficient credit to perform this transaction,Please contact admin');
            }


            else{
                return true;
            }
        
    }
//if customer hs got photo in photo table
    private function agentCustomerHasPhoto($sender_id){
            $photo = Photo::whereId($sender_id)->first();
            if($photo == null){
                return true;
            }else{
                return false;
            }
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $user = Auth::user();
         $transactions = Transaction::where('user_id',$id)
                        ->orderBy('created_at', 'DESC')
                        ->paginate(5);
        return view('transactions.agent.index',compact('user','transactions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $bank = Bank::where('status', 'b')->pluck('name','name')->toArray();
        $transaction = Transaction::findorfail($id);
        $customer = $transaction->agentCustomer;
        return view('transactions.agent.edit',compact('transaction','customer','bank'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $input  = $request->all();
        Transaction::whereId($id)->first()->update($input);
        \Session::flash('message','Transaction Successfully Updated');
        return redirect('/agent/customer-transactions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $transaction = Transaction::findorfail($id);
        $transaction->delete();
        \Session::flash('message','Transaction Successfully Deleted');
        return redirect('/agent/customer-transactions');
    }
}
