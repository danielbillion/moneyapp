<?php

namespace App\Http\Controllers;
use App\User;
use App\Currency;
use Illuminate\Http\Request;
use App\Http\Requests\CommissionRequest;
use App\Commission;

class CommissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     
        $commissions = Commission::orderBy('user_id', 'DESC')->orderBy('start_from', 'ASC')->paginate('30');
        
        $currencies = Currency::whereStatus(1)->pluck('code','id')->toArray();
        
        $users = User::where('type','customer')
                        ->orWhere('type','agent')
                        ->pluck('name','id')->toArray();

   //add for array [] & reverse order
        $users['0'] = 'All'; 
        $users = collect($users)->reverse()->toArray();
        return view('admin.commission.index',compact('commissions','users','currencies'));
      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommissionRequest $request)
    {
        
        $input = $request->all();
        Commission::create($input);
        \Session::flash('message','New Commission range Successfully Added');
        return redirect('commission');
       // return $request->all();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $commission_value=Commission::findorfail($id);

        $currencies = $currencies = Currency::whereStatus(1)->pluck('code','id')->toArray();

        $commissions = Commission::orderBy('user_id', 'DESC')->orderBy('start_from', 'ASC')->paginate('30');
       
        $users = User::where('type','customer')
                    ->orWhere('type','agent')
                    ->pluck('name','id')->toArray();

        //add for array [] & reverse order
        $users['0'] = 'All'; 
        $users = collect($users)->reverse()->toArray();
                
        return view('admin.commission.edit',compact('commissions','users','commission_value','currencies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CommissionRequest $request, $id)
    {
        //
        Commission::find($id)->update($request->all());
        \Session::flash('message','Commission successfully  updated');
        return  redirect()->route('commission.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $commission = Commission::findorfail($id);
        $commission->delete();
        \Session::flash('message','Commission range Successfully Deleted');
        return redirect('commission');
    }
}
