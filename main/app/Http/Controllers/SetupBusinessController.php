<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting_business;
use App\Http\Requests\SetupBusinessRequest;

class SetupBusinessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $business = setting_business::orderBy('id', 'desc')->first();
        
        if($business){
            return view('setting.business.edit',compact('business')); 
        }

        return view('setup.business.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SetupBusinessRequest $request)
    {
        //
        setting_business::create($request->all());
         \Session::flash('message','New Business Setting Submitted');
        return redirect('setup-business');

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view('setup.business.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SetupBusinessRequest $request, $id)
    {
        //
        setting_business::create($request->all());
         \Session::flash('message','New Business Setting Submitted');
         return redirect('setup-business');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
