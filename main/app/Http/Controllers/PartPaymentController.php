<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use URL;
use App\Agent_Payment;
use App\PartPayment;
use App\Outstanding;
use App\User;
use App\Subscription;
use App\Agent;

class PartPaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $_api_context;

    public function __construct()
    {
       
    
}
    public function index($id = null)
    {
        //
        
    }

    public function transactions($id = null)
    {
        
        if($id ===null){
            $user = "All";
            $payments = PartPayment::orderBy('created_at','desc')->paginate('20');
        }else{
            $user = User::find($id)->name;
            $payments = PartPayment::whereUser_id($id)->orderBy('created_at','desc')->paginate('20');
        }
        return view('partpayment.transactions_part',compact('payments','user'));
    }

    public function commissions($id = null)
    {
        
        if($id ===null){
            $user = "All";
            $payments = PartPayment::wherePayment_type('commission')
                                    ->orderBy('created_at','desc')->paginate('20');
        }else{
            $user = User::find($id)->name;
            $payments = PartPayment::whereUser_id($id)->wherePayment_type('commission')->            
                                     orderBy('created_at','desc')->paginate('20');
        }
        return view('partpayment.commissions_part',compact('payments','user'));
    }

    public function post_search(Request $request){
        
    $value = ['from'=>$request->from,'to'=>$request->to,'query_string'=>$request->query_string];
       $user = "searching";
        if($request->type === 'transaction'){
         $payments =PartPayment::PaymentSearch('transaction', $value);
         return view('partpayment.transactions_part',compact('payments','user'));
       }else{
        
        $payments =PartPayment::PaymentSearch('commission', $value);
        return view('partpayment.commissions_part',compact('payments','user'));
       }
       
    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //      $input = $request->all();
        //      $amount = $request->amount;
        //      $total = $request->total;
        
        //     $input['fully_paid'] = $amount >= $total?1:0;
        //     $input['balance'] = $total - $amount;
        //      $input['fully_paid'] = 0;
         
        //      $payment = Agent_PartPayment::create($input);
        //       Transaction::whereUser_id($request->user_id)
        //       ->whereAgent_payment_id('none')
        //       ->update(['agent_payment_id'=>$payment->id]);
        
        // \Session::flash('message','Successfully Updated'); 
        // return redirect('payments');
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        // $user = User::find($id)->first();
        // $payments = User::find($id)->payments()->get();
        // return view('payments.paid',compact('payments','user'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        // $member = Agent::find($id);
        // return view('payments.create',compact('member','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
