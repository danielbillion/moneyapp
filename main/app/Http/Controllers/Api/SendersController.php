<?php

namespace App\Http\Controllers\Api;
use App\AgentCustomer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SendersController extends Controller
{
    public function index($id=null)
    {
        //
       
        return response()->json(
                             $id?AgentCustomer::whereUser_id($id)
                                 ->select('name','mobile','id') ->get()
                            :AgentCustomer::get());                         
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input = $request->all();
        $customer = AgentCustomer::create($input);
        return response()->json($customer);

    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
         return response()->json(
                            AgentCustomer::whereId($id)
                            ->select('name','mobile','id') ->get());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        return response()->json(AgentCustomer::findorfail($id)->delete());
    }
}
