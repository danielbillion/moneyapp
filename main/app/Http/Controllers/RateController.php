<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RateRequest;
use App\Rate;
use App\User;
use App\Currency;
class RateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rates = Rate::orderBy('user_id', 'DESC')->orderBy('created_at', 'DESC')->paginate('20');
        $users = User::where('type','customer')
                        ->orWhere('type','agent')
                        ->pluck('name','id')->toArray();

    //currencies   
        $currencies = Currency::whereStatus(1)->pluck('code','id')->toArray();

     //add for array [] & reverse order
         $users['0'] = 'All'; 
         $users = collect($users)->reverse()->toArray();
        return view('admin.rate.index',compact('users','rates','currencies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RateRequest $request)
    {
       $input = $request->all();
        Rate::create($input);
        \Session::flash('message','New Rate Added');
        return redirect()->route('rate.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rates = Rate::orderBy('user_id', 'DESC')->orderBy('created_at', 'DESC')->paginate('20');
        $rate_selected = Rate::findorfail($id);
        $users = User::where('type','customer')
                        ->orWhere('type','agent')
                        ->pluck('name','id')->toArray();

    //currencies   
        $currencies = Currency::whereStatus(1)->pluck('code','id')->toArray();

     //add for array [] & reverse order
         $users['0'] = 'All'; 
         $users = collect($users)->reverse()->toArray();
        return view('admin.rate.edit',compact('users','rates','currencies','rate_selected'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RateRequest $request, $id)
    {
        //
        Rate::findorfail($id)->update($request->all());
        \Session::flash('message','Rate Successfully modified');
        return redirect()->route('rate.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rate = Rate::findorfail($id);
        $rate->delete();
        \Session::flash('message',' Rate Successfully Deleted');
        return redirect('rate');
    }
}
