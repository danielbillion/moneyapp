<?php

namespace App\Http\Controllers;
use App\PartPayment;
use App\Outstanding;
use App\User;
use Illuminate\Http\Request;
use Auth;

class OutstandingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('outstanding.index');
    }
    public function status ($id,$type){
        $controller = $type ==='transaction'?'OutstandingController@update'
                                        :'OutstandingController@update_commissions';
            $outstanding = Outstanding::whereId($id)->first();
            return view('outstanding.status',compact('outstanding','type','controller'));
    }

    public function transactions($id, $status = null){

                $owing = PartPayment::where('fully_paid',0)->sum('amount')
                -Outstanding::where('transaction_paid',0)->sum('amount');
            if($id ===null){
                $agent = null ;             //All member
                $outstandings = Outstanding::orderBy('created_at','desc')->paginate('20');
            }else{
                    $user = User::find($id);
                    $agent = $user;             //single agent member details
                    $owing = $user->pending_outstanding->sum('amount')
                                                -$user->transaction_part_payments->sum('amount');
                if($status === 'all') {
                    $outstandings = Outstanding::where('user_id',$id)
                                ->orderBy('created_at','desc')->paginate('20');    
                }else{
                    $outstandings = Outstanding::where('user_id',$id)->
                    whereTransaction_paid($status)->orderBy('created_at','desc')->paginate('20');  
                }                            
                
            }
                return view('outstanding.transactions',compact('outstandings','owing','agent'));
    }

    public function commissions($id, $status = null){

        $owing = PartPayment::where('fully_paid',0)->sum('amount')
        -Outstanding::where('commission_paid',0)->sum('agent_commission');
        if($id ===null){
            $agent = null ; //Al members
            $commissions = Outstanding::orderBy('created_at','desc')->paginate('20');
        }else{
            $user = User::find($id);
            $agent = $user;     //Single Agent User
            
            $owing = $user->pending_commission->sum('agent_commission')
                                        -$user->commission_part_payments->sum('agent_commission');

            if($status === 'all') {
                  $commissions = Outstanding::where('user_id',$id)->orderBy('created_at','desc')->paginate('20');
                }else{
                $commissions = Outstanding::where('user_id',$id)->
                    whereCommission_paid($status)->orderBy('created_at','desc')->paginate('20');
                    
                    }  
        }
            return view('outstanding.commissions',compact('commissions','owing','agent'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly Paid/create Transactions Outstanding.

     */
    public function store(Request $request)
    {
       
       if($request->amount>=$request->total){
             Outstanding::whereUser_id($request->agent_id)
                        ->update(['transaction_paid'=>1,
                        'admin_id' => Auth::user()->type==='admin'?Auth::id():0,
                        'manager_id' => Auth::user()->type==='manager'?Auth::id():0,]);
            PartPayment:: whereUser_id($request->agent_id)
                     ->where('payment_type','transaction')->update(['fully_paid'=>1]);    
         }
       
        PartPayment::create([
                'user_id' =>$request->agent_id,
                'amount'=>  $request->amount,
                'payment_type'=>'transaction',
                'admin_payment_id' => Auth::user()->type==='admin'?Auth::id():0,
                'manager_id' => Auth::id(),
                'fully_paid'=>$request->amount>=$request->total?1:0
        ]);

        return redirect()->route('outstandings.index');
    }

     /**
     * Update Transaction Outstanding.
     
     */
    public function update(Request $request, $id)
    {
        //
       
        
            $outstanding = Outstanding::findorfail($id);
            if  (Auth::attempt(['email' => Auth::user()->email, 'password' => $request->password])) {
                    
                $outstanding->update([
                            'transaction_paid'=>1,
                            'admin_id' => Auth::user()->type==='admin'?Auth::id():0,
                            'manager_id' => Auth::user()->type==='manager'?Auth::id():0,
                            ]);

                PartPayment::create([
                        'user_id' =>$outstanding->user_id,
                        'amount'=>  $outstanding ->amount,
                        'payment_type'=>'transaction',
                        'admin_payment_id' => Auth::user()->type==='admin'?Auth::id():0,
                        'manager_id' => Auth::user()->type==='manager'?Auth::id():0,
                        'fully_paid'=>0
                ]);
                     return redirect()->route('outstandings.index');
             }else{
                 \Session::flash('error','Incorrect password');
                 return redirect()->back();
             }
        
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function store_commissions(Request $request)
    {
        if($request->amount>=$request->total){
            Outstanding::whereUser_id($request->agent_id)
                       ->update(['commission_paid'=>1,
                       'admin_id' => Auth::user()->type==='admin'?Auth::id():0,
                       'manager_id' => Auth::user()->type==='manager'?Auth::id():0,]);
           PartPayment:: whereUser_id($request->agent_id)
                     ->where('payment_type','commission')->update(['fully_paid'=>1]);    
        }
      
       PartPayment::create([
               'user_id' =>$request->agent_id,
               'amount'=>  $request->amount,
               'payment_type'=>'commission',
               'admin_payment_id' => Auth::user()->type==='admin'?Auth::id():0,
               'manager_id' => Auth::id(),
               'fully_paid'=>$request->amount>=$request->total?1:0
       ]);

       return redirect()->route('outstandings.index');
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_commissions(Request $request,$id)
    {
        if  (Auth::attempt(['email' => Auth::user()->email, 'password' => $request->password])) {
               
                $outstanding = Outstanding::findorfail($id);
            
                $outstanding->update([
                            'commission_paid'=>1,
                            'admin_id' => Auth::user()->type==='admin'?Auth::id():0,
                            'manager_id' => Auth::user()->type==='manager'?Auth::id():0,
                            ]);

                PartPayment::create([
                        'user_id' =>$outstanding->user_id,
                        'amount'=>  $outstanding ->agent_commission,
                        'payment_type'=>'commission',
                        'admin_payment_id' => Auth::user()->type==='admin'?Auth::id():0,
                        'manager_id' => Auth::user()->type==='manager'?Auth::id():0,
                        'fully_paid'=>0
                ]);
                return redirect()->route('outstandings.index');
                }else{
                    \Session::flash('error','Incorrect Password');
                    return redirect()->back();
                }
        
        
    }

   

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
