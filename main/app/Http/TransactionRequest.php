<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TransactionRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
   public function rules()
    {
        return [
            'sender_id'=>'required',
            'receiver_id' => 'required',
            'receiver_fname' => 'required',
            'receiver_lname' => 'required',
            'amount' => 'required',
            'exchange_rate' => 'required',
            //
        ];
    }

     public function messages()
    {
        return [
            'sender_id.required'=>'Sender Name is required',
            'receiver_id.required' =>'Receiver Name is required',
            'receiver_fname.required' =>'First name is required',
            'receiver_lname.required' =>'Last Name is required',
            'amount.required' =>'Amount To Send is Required',
            'exchange_rate.required' =>'Excahnge Rate is Required',
            //
        ];
    }
}
