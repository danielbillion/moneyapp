<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SetupBusinessRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'=>'required',
            'abbreviation' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'mobile' => 'required',
            'address' => 'required',
            'postcode' => 'required',
            'website' => 'required',
            //
        ];
    }

     public function messages()
    {
        return [
            'name.required'=>'Business Name is required',
            'abbreviation.required' =>'abbreviation is required',
            'phone.required' =>'Phone Number is required',
            'mobile.required' =>'Mobile Number is required',
            'address.required' =>'Address is Required',
            'postcode.required' =>'Postcode is Required',
            //
        ];
    }
}
