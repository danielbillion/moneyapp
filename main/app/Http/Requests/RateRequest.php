<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rate'=>'required',
            'currency_id' => 'required',
            
               
        ];
    }

     public function messages()
    {
        return [
            'rate.required'=>'Rate is required',
            'currency_id.required' =>'Currency Code rate is required'
            
             
        ];
    }
}
