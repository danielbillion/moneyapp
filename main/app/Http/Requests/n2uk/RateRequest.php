<?php

namespace App\Http\Requests\n2uk;

use Illuminate\Foundation\Http\FormRequest;

class RateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bou'=>'required|numeric',
             'sold'=>'required|numeric',
            

        ];
    }

     public function messages()
    {
        return [
            'bou.required'=>'Bank Name is required',
            'sold.required'=>'Bank Name is required',
            
             
        ];
    }
}
