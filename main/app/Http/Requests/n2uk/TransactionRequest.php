<?php

namespace App\Http\Requests\n2uk;

use Illuminate\Foundation\Http\FormRequest;

class TransactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
   public function rules()
    {
        return [
            'senders'=>'required',
            'receivers' => 'required',
            'receiver_fname' => 'required',
            'receiver_lname' => 'required',
            'amount_pounds' => 'required|numeric',
            'amount_naira' => 'required|numeric'
        ];
    }

     public function messages()
    {
        return [
        'senders.required'=>'Sender Name is required',
        'receivers.required' =>'Receiver Name is required',
        'receiver_fname.required' =>'First name is required',
        'receiver_lname.required' =>'Last Name is required',
        'amount_pounds.required' =>'Amount pounds is Required',
        'amount_naira.required' =>'Amount naira is Required'
        ];
    }
}
