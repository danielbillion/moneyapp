<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class agentTransactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sender_id'=>'required',
            'receiver_id' => 'required',
            'amount' => 'required',
            'receiver_fname' => 'required',
            'receiver_lname' => 'required',
            'receiver_phone' => 'required',
            'bank' => 'required',
            'transfer_type' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'sender_id.required'=>'Please select a sender',
            'receiver_id.required' =>'Please select a receiver',
            'amount.required' =>'Amount can not be empty',
            'receiver_fname.required' => 'Receiver First name is required',
            'receiver_lname.required' => 'Receiver Last name is required',
            'receiver_phone.required' => 'Receiver Phone name is required',
            'bank.required' => 'Receiver Bank is required',
            'transfer_type.required' => 'Transfer Type is required',
          
        ];
    }
}
