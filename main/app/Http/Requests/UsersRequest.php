<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fname' => 'required',
            'lname' => 'required',
            'dob'=>'required',
            'search' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
           
            'title' => 'required',
            
               
        ];
    }

     public function messages()
    {
        return [
            'fname.required'=>'First Name is required',
            'lname.required'=>'Last Name is required',
            'email.required'=>'email Name is required',
            'search.required' =>'Address to Uk rate is required',
             'phone.required' =>'Phone is required',
             'dob.required'=>'Date Of Birth  is required',
            
             'title.required'=>'Please choose title',
        ];
    }
}
