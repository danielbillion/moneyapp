<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
class CurrencyCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'destination' => 'required'
             

        ];
    }

     public function messages()
    {
        return [
            'origin.required'=>'Origin Name is required',
            'origin_symbol.required' =>'Origin Symbol range is required',
             'destination.required' =>'Destination is required',
             'destination_symbol.required' =>'Destination Symbol is required',
             'code.required'=>'Code or currency:which is combination origin + destination symmbol is required',
        ];
    }
}
