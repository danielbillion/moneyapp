<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommissionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_from'=>'required',
            'end_at' => 'required',
            'value' => 'required',
             'agent_quota' => 'required',

        ];
    }

     public function messages()
    {
        return [
            'start_from.required'=>'starting range is required',
            'end_at.required' =>'ending range is required',
             'value.required' =>'value is required',
             'agent_quota.required' =>'Agent Quota is required',
             
        ];
    }
}
