<?php
use App\Transaction;
namespace App;

use Illuminate\Database\Eloquent\Model;

class Outstanding extends Model
{
    //
    protected $table = "laravel_outstandings";
    protected $fillable = [
        'transaction_id',
        'user_id',
        'amount',
       'agent_commission',
        'manager_id',
        'admin_id',
        'transaction_paid',
        'commission_paid'

    ];

    public function transaction(){
        return $this->belongsTo('App\Transaction','transaction_id')->withDefault(function($outstanding){
            $outstanding->receipt_number ="deleted";
        });
        //return Transaction::where('id',$this->transaction_id);
         
    }

    public function payment(){
        return $this->morphMany('App\Payments','paymentable');
    }

    public function user_agent(){
        return $this->BelongsTo('App\User','agent_id');
    }

    public function getSenderNameAttribute(){
       $senderId = Transaction::where('id',$this->transaction_id)->value('sender_id');
        $sender = $senderId?AgentCustomer::whereId($senderId)->value('name'):null;
        return $sender?ucfirst($sender):'unavailable';
        
    }

    public function getStatusAttribute($value){
        return $this->attributes['transaction_paid'] === 0? 'Unpaid': 'Paid';
         
     }

    public function getReceiverNameAttribute(){
        $receiverId = Transaction::where('id',$this->transaction_id)->value('receiver_id');
        $receiver = $receiverId?Receiver::whereId($receiverId)->select('fname','lname')->first():null;
        return $receiver?ucfirst($receiver->fname." ".$receiver->lname):'unavailable';
    } 
    
}
