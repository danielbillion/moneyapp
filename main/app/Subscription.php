<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
class Subscription extends Model
{
    //
    protected $table = "laravel_subscriptions";
    
    
 protected $fillable = [
   'user_id',
   'start_at',
    'ref',
    'amount',
    'payment_mode',
    'others',
    'paid',
    'months_duration',
    'notice'];
    

    //Relationships

    public function payment(){
        return $this->morphMany('App\Payments','paymentable');
    }
    
    public function getExpiresAttribute(){
        $time = strtotime($this->attributes['start_at']);
        $expire_time =  strtotime(" + ".$this->attributes['months_duration']." month", $time);
        return Date('d/m/Y',$expire_time);
    }

    public  function getDaysLeftAttribute(){
        $start_time = strtotime($this->attributes['start_at']);
        $expires =  strtotime(" + ".$this->attributes['months_duration']." month", $start_time);
        return  round(($expires - time()) / (60 * 60 * 24));
        return $this->days_left;
    }

    public  function check(){
        $notice = $this->getDaysLeftAttribute(). " days left".", Your MoneyApp Manager will soon expire,
         re-activate  
            your subscription with £".$this->attributes['amount'] ."  outstanding to 
            continue using this service without restriction or interruption. 
            Once you have reactivated your subscription, 
            it will be extended and the service will 
            be extended for another 24 months,please login to your site. "." ".env('CLIENT_WEBSITE');
        if($this->getDaysLeftAttribute() < 31 && $this->attributes['notice'] === 0){
                $message = $notice;
                Subscription::orderBy('id','DESC')->limit(1)->first()->update(['notice'=>1]);
                $this->notifyWithEmail($notice);
        }
        if($this->getDaysLeftAttribute()<= 14 && $this->attributes['notice'] === 1){
            $message = $notice;
            Subscription::orderBy('id','DESC')->limit(1)->first()->update(['notice'=>2]);
            $this->notifyWithEmail($notice);
            $this->notifyWithSMS($notice, $this->attributes['amount']);
        }
        if($this->getDaysLeftAttribute() <= 7 && $this->attributes['notice'] === 2){
            $message = $notice;
            Subscription::orderBy('id','DESC')->limit(1)->first()->update(['notice'=>3]);
            $this->notifyWithEmail($notice);
            $this->notifyWithSMS($notice,$this->attributes['amount']);

        }else{
            if($this->getDaysLeftAttribute() <= 7 && $this->attributes['notice'] > 1){
                $message = $notice;
            }else{
                $message = null;
            }
            
        }

       ;
    }

    public function notifyWithEmail($notice){
        $title = "Renewal Notice";
        $content = $notice;

        Mail::send('subscription.email', ['title' => $title, 'content' => $content], function ($message)
        {

            $message->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));

            $message->to(env('CLIENT_EMAIL'));

             $message->subject("Renewal Notice");

        });


        return response()->json(['message' => 'Request completed']);
    }

    public function notifyWithSMS($notice,$amount){

        //Authorisation details.
        $username = env('SMS_USERNAME');
        $hash =  env('SMS_HASH');

        // Config variables. Consult http://api.txtlocal.com/docs for more info.
        $test = "0";

                // Data for text message. This is the text message data.
        $sender = "Renewal-Notice"; // This is who the message appears to be from.
        $numbers = env('CLIENT_MOBILE'); // A single number or a comma-seperated list of numbers
        $message = "Your MoneyApp Manager will soon expire,please login to your website "
                        .env('CLIENT_WEBSITE')." "." outstanding Payment of £".$amount.
                    " is due to avoid any interruption";

        // 612 chars or less
        // A single number or a comma-seperated list of numbers

        $message = urlencode($message);
        $data = "username=".$username."&hash=".$hash."&message=".$message."&sender=".$sender."&numbers=".$numbers."&test=".$test;
        $ch = curl_init('http://api.txtlocal.com/send/?');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch); // This is the result from the API
        curl_close($ch);
    }
    
}
