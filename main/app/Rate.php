<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    //
    protected $table = "laravel_rates";
    protected $fillable = ['rate','user_id','currency_id','bou_rate','sold_rate'];
    public function user()
    {
        return $this->belongsTo('App\User')->withDefault(function ($user) {
			$user->name = 'All';
		});;
    }

    public static function todays_rate(){
    	return Rate::orderBy('id','desc')->select('rate')->take(1)->first();
  }

  public function currency(){
    return $this->belongsTo('App\Currency','currency_id');
  }
    
    }

