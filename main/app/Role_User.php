<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role_User extends Model
{
    //
    protected $table = "laravel_role_user";
    protected $fillable = ['name'];
}
