<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting_business extends Model
{
    //
    protected $table = "laravel_setting_businesses";
    public $fillable = [
        'name',
        'slogan',
        'abbreviation',
        'phone',
        'mobile',
        'email',
        'email2',
        'address',
        'postcode',
        'website',
        'type',
        'activity_notice'

    ];

    public function setActivityNoticeAttribute($value){
        $this->attributes['activity_notice'] = $value==='on'?1:0;
    }

    public static function setting(){
        return Setting_business::orderBy('id','desc')->limit(1)->first();
    }
}
