<?php 
return [ 
    'client_id' => env('CLIENT_PAYPAL_CLIENT_ID',''),
    'secret' => env('CLIENT_PAYPAL_SECRET',''),
    'settings' => array(
        'mode' => env('CLIENT_PAYPAL_MODE','sandbox'),
        'http.ConnectionTimeOut' => 30,
        'log.LogEnabled' => true,
        'log.FileName' => storage_path() . '/logs/paypal.log',
        'log.LogLevel' => 'ERROR'
    ),
];