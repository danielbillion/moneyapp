@extends('layouts.layout')
@section('content')
<div class="row">										
				<div class="col-md-8">		 
						<input type="text" id ="getreceivers" class="form-control" id="getMember" placeholder="Search...">			
					</div>
					
					<div class="col-md-4">
						<span style="float:right;"><?php //echo $pages; ?>
								 <?php //echo $items; ?></span>
								
								<nav style="float:right;">
								 <ul class="pagination">
								<?php //echo $pagin;?>
								</ul>
								</nav>
					</div>
					
			</div>
					<h6>Receivers</h6>	
				<table id="sort-table" class="table table-striped table-bordered tablesorter text-center">
				<thead>
					<tr>
					<th>No </th>
					<th>Name <i class="caret"></i></th>
						
					<th>Mobile <i class="caret"></i></th>
					<th>Modify</th>	
					@if(Auth::user()->type === 'admin')
					<th>Delete</th>	
					@endif				
					<th>Send</th>
					</tr>
				<thead>
				<tbody>
						
				<?php $x=0; foreach($receivers as $receiver): ?>
					<tr>
						<td><?php  echo $x=$x+1;?></td>
						<td><a href="{{route('receivers.edit',$receiver->id)}}">
								<?php  echo ucfirst($receiver->title)." ".ucfirst($receiver->fname)." ". ucfirst($receiver->lname);?></a></a></td>
							
						<td><?php  echo $receiver->phone;?></td>
						 <td><a href="{{route('receivers.edit',$receiver->id)}}" class="btn btn-primary"><i class="fa fa-edit fa-lg"> Modify</a></td>	
						 
						@if(Auth::user()->type === 'admin')
						 {!! Form::open(['method'=>'DELETE', 'action'=>['ReceiversController@destroy', $receiver->id ], 'files'=>true, 'class'=>'delete']) !!}	
						 	<td>
						 		{!! Form::submit('Delete', ['class'=>'btn btn-danger btn-block'])!!}</td>
						 {!! Form::close() !!}
						@endif	
						<td>
							@if(Auth::user()->type === 'agent' || Auth::user()->type === 'customer')
							<a href="{{route('transaction.create',
									['sender_id'=>@Auth::user()->type==='agent'?
									$receiver->sender_id:0,'receiver_id'=>$receiver->id]) }}"
							 class="btn btn-success btn-lg"><i class="fa fa-money fa-lg"></i>  Send Money</a>
							 @endif
						</td>
						 </tr>
						  <?php endforeach; ?>
						  
						  </tbody>
						</table>
						{{$receivers->render()}}
@endsection

@section('script')

$(".delete").on("submit", function(){
        return confirm("Are you sure?");
    });

$(function(){
$( "#getreceivers" ).autocomplete({	
	max:10,
   source: function (request, response) {
			$.ajax({
				url:  "{{route('receivers.autocomplete',[$type,$id,""])}}" + "/" + request.term ,
				data: { term: request.term },
				dataType: "json",
				success: response,
				error: function () {
					response([]);
					
				}
			});
		},
   select: function( event, ui ) {
	   var add="{{route('receivers.show',"")}}";		
   window.location.href = add+'/'+ui.item.id;
   }
});

});

@endsection