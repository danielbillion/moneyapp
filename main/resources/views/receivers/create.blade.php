@extends('layouts.layout')
@section('content')

	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 style="display:inline-block;text-transform:uppercase;"></h4>
		</div>
		<div class="panel-body">
			<div class="panel-resize">
			  <div class="row">
				<div class="col-md-7 col-md-offset-3">
						<ol class="breadcrumb">
						<!--<li><a href="index.php">Home</a></li>
						<li><a href="sendersReceivers.php">Sender</a></li>-->
						<li class="active">Create New Receiver</li>
						
						</ol>
					</div>
					
				</div>
				
						
	{!! Form::open(['method'=>'POST', 'action'=> 'ReceiversController@store', 'files'=>true]) !!}
	{!! form::hidden('user_id',@Auth::id() ,['id'=>'user_id']) !!}
	{!! Form::hidden('myurl',URL::to('/'),['id'=>'myurl']) !!}
	{!! Form::hidden('sender_id',$id) !!}
	
			
			@if(Auth::user()->type === 'agent')
			<div class="row">
				<div class="col-md-8">
					<div class="form-group form-group-sm">
						<span class="text-danger">*</span>
						{!! Form::label('Senders', 'Senders')!!}
						{!! Form::select('sender_id',$sender,null,['id'=>'senders','class'=>'form-control'])!!}
					</div>
				</div>
			</div>
			@endif
			<div class="row">
				<div class="col-md-5 ">
					<div class="form-group form-group-sm">
						<span class="text-danger">*</span>
						{!! Form::label('fname', 'First Name')!!}
						{!! Form::text('fname', null,['class'=>'form-control'])!!}
					</div>
				</div>
				<div class="col-md-5 ">
					<div class="form-group form-group-sm">
						<span class="text-danger">*</span>
						{!! Form::label('lname', 'Last Name')!!}
						{!! Form::text('lname', null, ['class'=>'form-control'])!!}
					</div>
				</div>
				</div>		
				<div class="row">	
					<div class="col-md-5">
						<div class="form-group form-group-sm">
						{!! Form::label('transfer_type', 'Transfer Type:') !!}
						{!! Form::select('transfer_type', ['Pick Up'=>'Pick Up','Bank Account'=>'Bank Account'] , null, ['class'=>'form-control','id'=>'transfer_type'])!!}
						</div>
							</div>

						<div class="col-md-5">
							<div class="form-group form-group-sm">
								{!! Form::label('phone', 'Phone')!!}
								{!! Form::text('phone', null, ['class'=>'form-control'])!!}
							</div>
						</div>
				</div>
						
							
			<div class="row">
				<div class="col-md-5 col-md-offset-3">
					<div class="form-group form-group-sm">
						{!! Form::label('bank', 'Bank') !!}
						{!! Form::select('bank', [$bank] , null, ['class'=>'form-control'])!!}	
					</div>
				</div>
			</div>
						
			<div class="row"  id="actno" >
				<div class="col-md-5 col-md-offset-3" >
					<div class="form-group form-group-sm">
						{!! Form::label('account_number', 'Account Number')!!}
						{!! Form::text('account_number', null, ['class'=>'form-control'])!!}
					</div>
				</div>
			</div>
			<div class="row" id="modeId" >	
				<div class="col-md-5 col-md-offset-3">
						<div class="form-group form-group-sm">
							{!! Form::label('Identity', 'Identity') !!}
							 {!! Form::select('identity_type', ['National ID'=>'National ID','Intl Passport'=>'International Passport','Drivers Lincence'=>'Drivers License','Any type of ID' => 'Any type of ID'] , null, ['class'=>'form-control'])!!}	
								</div>
							</div>
			</div>
				<br/><br/><br/>		
			<div class="row">
			
				<div class="col-md-3 text-center col-md-offset-3">
							<div class="form-group">
								{!! Form::submit('Submit', ['class'=>'btn btn-primary btn-block'])!!}
							</div>
				</div>
				<div class="col-md-3 text-center">
							<div class="form-group">
								<a href=="#" class="btn btn-danger btn-block">Cancel</a>
							</div>
				</div>
			</div>
			
				{!! Form::close() !!}

				</div>
			</div>
		</div>

@endsection
@section('script')
	
	
		$(function(){
		
		
				var myurl= $('#myurl').val() + '/' 
				+ $('#user_id').val() + '/customer'
				console.log(myurl)
	$.ajax({
		method:'get',
		url:myurl,
		success:function(response){
		console.log('customer=',response)
		$('#senders').append($('<option>Select Sender</option>'))
						$.each(response,function(sender){
							$('#senders').append($("<option></option>")
											.attr("value",response[sender].id)

											.text(response[sender]
											.name.toUpperCase()))
						})
		},
		error:function(error){
			console.log(error)
		}
	})
})
@endsection

