@extends('layouts.layout')
@section('content')

					<div class="row">
						<div class="col-md-4">
							<small class="essential">Areas Marked with * are important</small>
						</div>
					
					</div><h3>Transaction Setup</h3>
<!-- Form -->
		{!! Form::model(['method'=>'POST', 'action'=> ['SetupTransactionController@store'], 'files'=>true]) !!}
		{!! Form::hidden('photo_id', null, ['class'=>'form-control'])!!}
		{!! Form::hidden('address_id', null, ['class'=>'form-control'])!!}
					 <div class="panel panel-default">
						<div class="panel-heading">
							
						</div>
					<div class="panel-body">
					<div class="row">																					
								<div class="col-md-5 col-md-offset-1 ">
									<div class="form-group form-group-sm">
										<span class="essential">*</span>
										{!! Form::label('Allow Credit', 'Allow Credit')!!}
									<input name = 'allow_credit' type = 'checkbox' >
														 
													
									</div>
								</div>
										
								<div class="col-md-5">
										<div class="form-group">
										<span class="essential">*</span>
										{!! Form::label('Send Email', 'Send Email')!!}
										<input name = 'send_email' type = 'checkbox' >
													
									</div>
								</div>
							</div>
							<p/>
							<div class="row">																					
								<div class="col-md-5 col-md-offset-1 ">
									<div class="form-group form-group-sm">
										<span class="essential">*</span>
										{!! Form::label('Cap Transaction', 'Cap Transaction')!!}
										<input name = 'cap_transaction' type = 'checkbox' id = "cap_transaction" > 
													 
									{!! Form::text('max_transaction',null,['class'=>'form-control','id'=>'max_transaction']) !!}				
									</div>
								</div>
										
								<div class="col-md-5">
										<div class="form-group">
										<span class="essential">*</span>
										{!! Form::label('Agent Name on Receipt', 'Agent Name on Receipt')!!}
									<input name = 'agentname_onreceipt' type = 'checkbox'>
													
									</div>
								</div>
							</div>
							<p/>
							<div class="row">																					
								<div class="col-md-5 col-md-offset-1 ">
									<div class="form-group form-group-sm">
										<span class="essential">*</span>
										{!! Form::label('Allow SMS', 'Allow SMS')!!}
										<input name = 'allow_sms' type = 'checkbox'  id = "allow_sms"  >
									{!! Form::text('sms_email',null,['class'=>'form-control','placeholder'=>'Enter email','id'=>'sms_email']) !!}
													
									</div>
								</div>
										
								<div class="col-md-5">
										<div class="form-group" id = "sms_hash">
										<span class="essential">*</span>
										{!! Form::label('SMS Hash', 'SMS Hash')!!}
										{!! Form::text('sms_hash',null,['class'=>'form-control','placeholder'=>'Enter hash']) !!}
													
									</div>
								</div>
							</div>
							<p/>
							<div class="row">	
							<div class="col-md-5 col-md-offset-1 ">
									<div class="form-group form-group-sm">
										<span class="essential">*</span>
										{!! Form::label('Two Copies', 'Two Receipt Copies')!!}
										<input name ='two_copies' type = 'checkbox'  >
								
									</div>
								</div>																				
								<div class="col-md-5 col-md-offset-1 ">
									<div class="form-group form-group-sm">
										<span class="essential">*</span>
										{!! Form::label('Max Payment', 'Max Payment')!!}
										<input name = 'allow_payment' type = 'checkbox'   id = "allow_payment">
										{!! Form::text('max_payment',null,['class'=>'form-control','id'=>'max_payment']) !!}
									
													
									</div>
								</div>
										
								
							</div>
							<p/>
							
								<p/>
								<div class="col-md-12">
									<div class="form-group form-group-sm">
										{!! Form::submit('submit', ['class="btn btn-primary btn-block"']) !!}
							                
										
									</div>
								</div>
							</div>
							
								{!! Form::close() !!}				
								
							<br/><br/>
							
							
										
										
										
					</div>
					
			

@endsection

@section('script')
	$('#max_transaction').hide()
	$(function(){
			$('#cap_transaction').change(function() {
					$('#max_transaction').toggle()	
				})
			
			
			$('#sms_email').hide()
			$('#sms_hash').hide()
				$('#allow_sms').change(function() {
					$('#sms_email').toggle()	
					$('#sms_hash').toggle()
				
				})


			$('#max_payment').hide()
				$('#allow_payment').change(function() {
				
					$('#max_payment').toggle()	
				
				})
	
	})					
@endsection