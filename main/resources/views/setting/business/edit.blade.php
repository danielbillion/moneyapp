@extends('layouts.layout')
@section('content')

					<div class="row">
						<div class="col-md-4">
							<small class="essential">Areas Marked with * are important</small>
						</div>
					
					</div><h3>Business Setup</h3>
<!-- Form -->
		{!! Form::model($business,['method'=>'PATCH', 'action'=> ['SetupBusinessController@update',$business->id], 'files'=>true]) !!}
		{!! Form::hidden('photo_id', null, ['class'=>'form-control'])!!}
		{!! Form::hidden('address_id', null, ['class'=>'form-control'])!!}
					 <div class="panel panel-default">
						<div class="panel-heading">
							
						</div>
					<div class="panel-body">
					<div class="row">																					
								<div class="col-md-5 col-md-offset-1 ">
									<div class="form-group form-group-sm">
										<span class="essential">*</span>
										{!! Form::label('Name', 'Name')!!}
										{!! Form::text('name', null, ['class'=>'form-control','placeholder'=>'Enter Business Name'])!!}
													
									</div>
								</div>
										
								<div class="col-md-5">
										<div class="form-group">
										<span class="essential">*</span>
										{!! Form::label('Abbreviation 2', 'Abbreviation')!!}
										{!! Form::text('abbreviation', null, ['class'=>'form-control','placeholder'=>'Enter Abbreviation'])!!}
													
									</div>
								</div>
							</div>
						<div class="row">																					
								<div class="col-md-5 col-md-offset-1 ">
									<div class="form-group form-group-sm">
										<span class="essential">*</span>
										{!! Form::label('Slogan', 'Slogan')!!}
										{!! Form::text('slogan', null, ['class'=>'form-control','placeholder'=>'Enter Email'])!!}
													
									</div>
								</div>
								<div class="col-md-5 col-md-offset-1 ">
									<div class="form-group form-group-sm">
										<span class="essential">*</span>
										{!! Form::label('Activity Notice', 'Activity Notice')!!}
										<input name = 'activity_notice' type = 'checkbox'   {{$business->activity_notice ===1?'checked':''}} >
									
													
									</div>
								</div>
						</div>
						
							<div class="row">																					
								<div class="col-md-5 col-md-offset-1 ">
									<div class="form-group form-group-sm">
										<span class="essential">*</span>
										{!! Form::label('Email', 'Email')!!}
										{!! Form::text('email', null, ['class'=>'form-control','placeholder'=>'Enter Email'])!!}
													
									</div>
								</div>
										
								<div class="col-md-5">
										<div class="form-group">
										<span class="essential">*</span>
										{!! Form::label('Email 2', 'Email 2')!!}
										{!! Form::text('email2', null, ['class'=>'form-control','placeholder'=>'Enter Email 2'])!!}
													
									</div>
								</div>
							</div>
							<p/>
							<div class="row">																				
								<div class="col-md-5 col-md-offset-1">
										<div class="form-group form-group-sm">
											<span class="essential">*</span>
											{!! Form::label('Phone', 'Phone')!!}
											{!! Form::text('phone', null, ['class'=>'form-control','placeholder'=>'Enter Phone Number'])!!}
														
										</div>
								</div>
										
										<div class="col-md-5">
											<span class="essential">*</span>
											{!! Form::label('Mobile', 'Mobile')!!}
											{!! Form::text('mobile', null, ['class'=>'form-control','placeholder'=>'Enter Mobile Number'])!!}
												
										</div>
							</div>
							<p/>
							<div class="row">	
								<div class="col-md-5 col-md-offset-1">
									<div class="form-group form-group-sm">
										
										<span class="essential">*</span>
										{!! Form::label('Address', 'Address')!!}
											{!! Form::text('address', null, ['class'=>'form-control','placeholder'=>'Enter Address'])!!}
														
									</div>
								</div>
										
								<div class="col-md-5">
									<div class="form-group form-group-sm">
										
										<span class="essential">*</span>
										{!! Form::label('Postcode', 'Postcode')!!}
											{!! Form::text('postcode', null, ['class'=>'form-control','placeholder'=>'Enter Postcode'])!!}
														
									</div>
								</div>
							</div>
							<p/>
							<div class="row">													
								<div class="col-md-5 col-md-offset-1">
									<div class="form-group form-group-sm">
										<span class="essential">*</span>
										{!! Form::label('website', 'Website')!!}
											{!! Form::text('website', null, ['class'=>'form-control','placeholder'=>'Enter Website'])!!}
														
									</div>
								</div>
								<div class="col-md-5">
									<div class="form-group form-group-sm">
										<span class="essential">*</span>
										{!! Form::label('Type', 'Type:') !!}
							                {!! Form::select('type', ['full'=>'full','agent'=>'agent','customers'=>'customers'] , null, ['class'=>'form-control'])!!}
										
									</div>
								</div>
								<p/>
								<div class="col-md-12">
									<div class="form-group form-group-sm">
										{!! Form::submit('submit', ['class="btn btn-primary btn-block"']) !!}
							                
										
									</div>
								</div>
							</div>
							
								{!! Form::close() !!}				
								
							<br/><br/>
							
							
										
										
										
					</div>
			

@endsection