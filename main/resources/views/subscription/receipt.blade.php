

@extends('layouts.subscription_receipt')
@section('content')


<div id="invoice">

    <div class="toolbar hidden-print">
        <div class="text-right">
            <button id="printInvoice" class="btn btn-info"><i class="fa fa-print"></i> Print</button>
            <button class="btn btn-info"><i class="fa fa-file-pdf-o"></i> -</button>
        </div>
        <hr>
    </div>
    <div class="invoice overflow-auto">
        <div style="min-width: 600px">
            <header>
                <div class="row">
                    <div class="col">
                        <a target="_blank" href="#">
                            <img src="#" data-holder-rendered="true" />
                            </a>
                    </div>
                    <div class="col company-details">
                        <h2 class="name">
                            <a target="_blank" href="#">
                           Computing24X7
                            </a>
                        </h2>
                        <div>Canning Town London</div>
                        <div>+44 7951170080</div>
                        <div>www.danielogunkoya.com</div>
                    </div>
                </div>
            </header>
            <main>
                <div class="row contacts">
                    <div class="col invoice-to">
                        <div class="text-gray-light">INVOICE TO:</div>
                        <h2 class="to">{{env('APP_NAME')}}</h2>
                        <div class="address">{{$business->address}}</div>
                        <div class="email"><a href="mailto:john@example.com">{{$business->email}}</a></div>
                    </div>
                    <div class="col invoice-details">
                        <h1 class="invoice-id">INVOICE </h1>
                        <div class="date">Date of Invoice: {{$subscription->created_at->format('y-m-d')}}</div>
                        <div class="date">Due Date: {{$subscription->expires}}</div>
                    </div>
                </div>
                <table border="0" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th class="text-left">DESCRIPTION</th>
                            <th class="text-right">HOUR PRICE</th>
                            <th class="text-right">HOURS</th>
                            <th class="text-right">TOTAL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="no">01</td>
                            <td class="text-left">
                                <h3>
                                        Renewal:Domain, Hosting , Emails     
                                </h3>
                            
                            </td>
                            <td class="unit">-</td>
                            <td class="qty">-</td>
                            <td class="total">£{{$subscription->amount}}</td>
                        </tr>

                        <tr>
                            <td class="no">02</td>
                            <td class="text-left">
                                <h3>
                                       Text-Message    
                                </h3>
                            
                            </td>
                            <td class="unit">-</td>
                            <td class="qty">-</td>
                            <td class="total">£{{$subscription->other}}</td>
                        </tr>

                        <tr>
                            <td class="no">03</td>
                            <td class="text-left">
                                <h3>
                                        Payment Mechants   
                                </h3>
                            
                            </td>
                            <td class="unit">-</td>
                            <td class="qty">-</td>
                            <td class="total">£-</td>
                        </tr>
                       
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="2"></td>
                            <td colspan="2">SUBTOTAL</td>
                            <td>£{{number_format($subscription->amount,2)}}</td>
                        </tr>
                        <tr>
                            <td colspan="2"></td>
                            <td colspan="2">TAX 25%</td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <td colspan="2"></td>
                            <td colspan="2">GRAND TOTAL</td>
                            <td>£{{number_format($subscription->amount,2)}}</td>
                        </tr>
                    </tfoot>
                </table>
                <div class="thanks">Thank you!</div>
                <div class="notices">
                    <div>NOTICE:</div>
                    <div class="notice">A finance charge of 1.5% will be made on unpaid balances after 30 days.</div>
                </div>
            </main>
            <footer>
                Invoice was created on a computer and is valid without the signature and seal.
            </footer>
        </div>
        <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->
        <div></div>
    </div>
</div>

@endsection

@section('script')
$('#printInvoice').click(function(){
            Popup($('.invoice')[0].outerHTML);
            function Popup(data) 
            {
                window.print();
                return true;
            }
        });
@stop