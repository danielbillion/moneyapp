@extends('layouts.layout')
@section('content')

@if(Auth::user()->type === 'admin')
<!--------------------------------------end part payment-------------------------------------------- -->

{!! Form::open(['method'=>'POST', 'action'=>'SubscriptionController@store','file'=>true]) !!}
				{!! Form::hidden('agent_id',$agent->id) !!}
				<div class="col-md-7 col-md-offset-2 text-center well ">
					<div class="col-md-12">
									<div class="form-group input-group">
										<span class="input-group-addon">Amount</span>
										{!! Form::text('amount','250',['class'=>'form-control','placeholder'=>'Enter Amount']) !!}	
									</div>
									<div class="form-group input-group">
										<span class="input-group-addon">Others</span>
										{!! Form::text('others','250',['class'=>'form-control',
											'id'=>'getTransactions','placeholder'=>'Enter Amount']) !!}	
									</div>

									<div class="form-group input-group">
										<span class="input-group-addon">Duration (Days)</span>
										{!! Form::text('duration','770',['class'=>'form-control','placeholder'=>'Duration in Days']) !!}	
									</div>

									<div class="form-group input-group">
										<span class="input-group-addon">Start Date</span>
										{!! Form::Date('start_at',['class'=>'form-control']) !!}	
									</div>
									{!! Form::submit('Make Payment',['class'=>'confirm btn btn-success  col-md-12 btn-block']) !!}
					</div>
				
					
	</div>
	
				{!! Form::close() !!}


<!----------------------------------------------------------------end part payment-------------------------- !-->
@endif
	
	 <div class="col-md-12 text-center">
		<h4> {{env(APP_NAME)}} Subscription History</h4>	
				
			
					<table class="table table-striped table-bordered tablesorter">
			<thead>
					<tr>
				
						<th>No </th>
						<th>Created On</th>
						<th class = "text-center">Ref</th>
						<th>Duration(days)</th>
						<th>Amount</th>
						<th class = "text-center">Other Payment </th>	
						<th class = "text-center">Paid Status </th>
						<th class = "text-center">Expires On </th>
                        <th class = "text-center">Duration </th>
                        <th class = "text-center">Payment-Mode </th>			
					</tr>
				</thead>
				
				
				<tbody>
							@foreach($subscriptions as $key=> $subscription)
					<tr>
						
						<td>
							{{$key +1}}					
						</td>
						<td>

						{{$subscription->start_at->format('Y-m-d')}}	
						</td>
						
						<td>
						{{$subscription->ref}}	
						</td>

						<td>
						{{$subscription->days_duration}}	
						</td>
										
						<td> {{$subscription->amount}} </td>
						
						
						<td>
								{{$subscription->others)}}
						</td>

						<td>
								{{$subscription->paid}}
						</td>
						
						<td>
								
                        </td>
                        <td>
								{{$subscription->duration}}
                        </td>
                        <td>
								{{$subscription->payment-mode}}
                        </td>
                        <td>
								<a href = "#" class="btn btn-default">View Receipt</a>
						</td>
					</tr>
									
					@endforeach
					
					
										
			</table>

					
				{{$subscriptions->render()}}		
			
		</div>
	
@endsection

@section('script')
	
	
@stop





