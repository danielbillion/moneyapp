@extends('layouts.layout')
@section('content')
 <div class="col-md-12 text-center">
		<h4> {{env('APP_NAME')}} Subscription History</h4>	
				
			
					<table class="table table-striped table-bordered tablesorter">
			<thead>
					<tr>
				
						<th>No </th>
						<th>Starts</th>
						<th>Expire</th>
						<th class = "text-center">Ref</th>
						<th>Amount</th>
						<th class = "text-center">Others </th>	
						<th class = "text-center">Paid </th>
						<th class = "text-center">Duration </th>
                        <th class = "text-center">Remaining </th>
						<th class = "text-center" colspan = "2"></th>			
					</tr>
				</thead>
				
				
				<tbody>
							@foreach($subscriptions as $key=> $subscription)
					<tr>
						
						<td>
							{{$key +1}}					
						</td>
						<td>
						{{$subscription->created_at->format('d/m/Y')}}
							
						</td>

						<td>
						{{$subscription->expires}}
							
						</td>
						
						<td>
						{{$subscription->ref}}	
						</td>
						<td> {{$subscription->amount}} </td>
						<td>
								{{$subscription->others}}
						</td>

						<td>
									@if($subscription->paid)
										<i class ='fa fa-check text-success'></i>
									@else
										<i class ='fa fa-times text-danger'></i>
									@endif
						</td>
						
                        <td>
								{{$subscription->months_duration}} months
                        </td>
                        <td>
								{{$subscription->days_left}} days
                        </td>
                        <td>
								<a href = "{{route('subscription.show',$subscription->id)}}" class="btn btn-default">View Receipt</a>
						</td>

						<td>
							<a href = "{{route('payments.pay',['subscription',$subscription->id])}}" class="btn btn-danger">Make Payment</a>
						</td>
					</tr>
									
					@endforeach
					
					
										
			</table>

					
				{{$subscriptions->render()}}		
			
		</div>
	
@endsection






