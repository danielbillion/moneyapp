@extends('layouts.layout')
@section('content')


<div class="row">										
				<div class="col-md-8" >		
					<input  type="text" class="form-control" id="getsender" placeholder="Search...">
				</div>
					
					<div class="col-md-4">
						<span style="float:right;"><?php //echo $pages; ?>
								 <?php //echo $items; ?></span>
								
								<nav style="float:right;">
								 <ul class="pagination">
								<?php //echo $pagin;?>
								</ul>
								</nav>
					</div>
					
			</div>
					<h6>Senders</h6>
					@if(count($senders) > 0)
					   <table id="sort-table" class="table table-striped tablesorter">
						  <thead >
							<tr class="text-center">
									<th>No </th>
									<th>Sender <i class="caret"></i></th>
									
									<th>Mobile <i class="caret"></i></th>
									<th colspan = "4" class="text-center">Actions</th>
							</tr>
						<thead>
						<tbody>
					
					
					
					 <?php $x=0; foreach($senders as $sender): ?>
						<tr>
							<td><?php  echo $x=$x+1;?></td>
							<td><a href="{{route('senders.edit',$sender->id)}}">
								<?php  echo ucfirst($sender->title)." ".ucfirst($sender->fname)." ". ucfirst($sender->lname);?></a></a></td>
							
							<td><?php  echo $sender->mobile;?></td>
							<td><a href="{{route('receivers.new',$sender->id)}}" class="btn btn-default "><i class="fa fa-users "></i> New Receiver</a></td>	
						 	
						 	<td><a href="{{route('senders.edit',$sender->id)}}" class="btn btn-default"><i class="fa fa-users "></i> Modify</a></td>	
							
							<td>
							<a href="
								{{route('receivers.sender',$sender->id)}}" class="btn btn-default">
								<i class="fa fa-user fa-md"></i> Receivers [{{App\Receiver::whereSender_id($sender->id)->count()}}]
							</a></td>

							<td> 
											@if(Auth::user()->isAdmin())
												{!! Form::open(['method'=>'DELETE', 'action'=>[ 'AgentCustomerController@destroy',
													$sender->id], 'files'=>true,'class'=>'delete']) !!}
													{!! Form::submit('Delete',['class'=>'delete btn btn-danger']) !!}
											{!! Form::close() !!}
										
											@endif
							</td>
							
							<td>
							@if(Auth::user()->type === 'agent' || Auth::user()->type === 'customer')
								<a href="{{route('transaction.create',['sender_id'=>$sender->id,'receiver_id'=>'null']) }}" 
								class="btn btn-primary">
								<i class="fa fa-money fa-lg"></i>  Send Money</a>
								@endif
							</td>
					 </tr>
						  <?php endforeach; ?>
				
				 </tbody>
			</table>
			@else
					<p><strong class = "col-md-12 text-center text-danger"> No Sender Available</strong></p>
				@endif
						{{$senders->render()}}
@endsection


@section('script')

<!--confirm Delete --> 
				$(".delete").on("submit", function(){
					return confirm("Are you sure, you want to delete?");
						});

		$(function(){

				 $( "#getsender" ).autocomplete({	
				 	max:10,
					source: function (request, response) {
							 $.ajax({
								 url:  "{{route('senders.autocomplete',[Auth::id(),''])}}" + "/"+ request.term,
								 data:"",
								 dataType: "json",
								 success: response,
								 error: function () {
									 response([]);
									 
								 }
							 });
						 },
					select: function( event, ui ) {
						var add="{{route('senders.show',"")}}";		
					window.location.href = add+'/'+ui.item.id;
					}
    			});
				
			});
@endsection