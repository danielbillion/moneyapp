
@extends('layouts.layout')
@section('content')
    
   	 <div class="col-md-12">
		  <section id="no	tice"><div class="well well-lg">
					<i class="fa fa-time"></i> 
					<span class="label label-info">{{App\User::find(@Auth::id())->previousLoginAt()}}	</span>
					<span class="text-danger pull-right">
						@if(Auth::user()->body)Note: {{Auth::user()->body}}
						@endif
						
					</span>
					<div>
					@if($subscription->days_left < 30)
						{{$subscription->check()}}
						<h1 style = "display:inline;">{{$subscription->days_left}} days left!</h1>
						<strong class="text-danger">
								Your MoneyApp Manager will soon expire, re-activate  
								your subscription with a new payment to 
								continue using this service without restriction or interruption. 
								Once you have make payment, the service will 
								be extended for another 24 months
						</strong>
						<a href="{{route('payments.pay',['subscription',$subscription->id])}}" class="btn btn-primary">Pay Nows</a>
					@endif
					</div>
			</section>
		

@if(Auth::user()->isCustomer() || Auth::user()->isAgent())
          <!-----How It Works---->
		  <div id="howitwork">
			  <div class="well well-lg">
					<h2> How It Works
						 <i style = "font-size:18px;"
						 data-toggle="tooltip" data-placement="right" title="Tooltip on top"
						  class="fa fa-info-circle x"></i></h2>
				 <div class="flow">
				@if(!Auth::user()->isCustomer())
					<a href="{{route('senders.create')}}" >
					  <span> Create New Sender</span>
					 </a>
				@endif
					  <span><a href="{{route('receivers.create')}}" > Create Receiver</a></span>
					  <span> <a href="{{route('transactions.create')}}" >Send Money</a></span>
				</div>
				  <div class="diagram">
					@if(!Auth::user()->isCustomer())
						<a href="{{route('senders.create')}}" >
							<span class="fa-stack fa-4x">		  
							  <i class="fa fa-square-o fa-stack-2x"></i>
							  <i class="fa fa-user fa-stack-1x"></i>
							</span>
						</a>
						<span><i class="fa fa-long-arrow-right fa-4x" aria-hidden="true"></i></span>
					@endif
					
						<a href="{{route('receivers.create')}}">
							<span class="fa-stack fa-4x">		  
							  <i class="fa fa-square-o fa-stack-2x"></i>
							  <i class="fa fa-users fa-stack-1x"></i>
							</span>
						</a>
					
						
						<span><i class="fa fa-long-arrow-right fa-4x" aria-hidden="true"></i></span>
						
						<a href="{{route('transactions.create')}}" >
							<span class="fa-stack fa-4x">		  
							  <i class="fa fa-square-o fa-stack-2x"></i>
							  <i class="fa fa-money fa-stack-1x"></i>
							</span>
						</a>
					</div>
			</div>
			</div>
@endif

@if(Auth::user()->isAdmin())
         <!-----How It Works---->
		  <div id="howitwork">
			  <div class="well well-lg">
					
			  <div class="row">
				  <div class="col-md-12">
			 			 <h2> Getting Started
						 <i style = "font-size:18px;"
						 data-toggle="tooltip" data-placement="right" title="Tooltip on top"
						  class="fa fa-info-circle x"></i></h2><br/>
				 <div class="flow">
				@if(!Auth::user()->isCustomer())
					<a href="{{route('currencies.index')}}" >
					  <span>Setup Currency</span>
					 </a>
				@endif
					  <span><a href="{{route('rate.index')}}" > Setup Rate</a></span>
					  <span> <a href="{{route('commission.index')}}" >Setup Commission</a></span>
				</div>
			</div>
		</div>	  
			
		
		<div class="diagram">
					@if(!Auth::user()->isCustomer())
						<a href="{{route('currencies.index')}}" >
							<span class="fa-stack fa-4x">		  
							  <i class="fa fa-square-o fa-stack-2x"></i>
							  <i class="fa fa-globe fa-stack-1x"></i>
							</span>
						</a>
						<span><i class="fa fa-long-arrow-right fa-4x" aria-hidden="true"></i></span>
					@endif
					
						<a href="{{route('rate.index')}}">
							<span class="fa-stack fa-4x">		  
							  <i class="fa fa-square-o fa-stack-2x"></i>
							  <i class="fa fa-filter"></i>
							</span>
						</a>
					
						
						<span><i class="fa fa-long-arrow-right fa-4x" aria-hidden="true"></i></span>
						
						<a href="{{route('commission.index')}}" >
							<span class="fa-stack fa-4x">		  
							  <i class="fa fa-square-o fa-stack-2x"></i>
							  <i class="fa fa-money"></i>
							</span>
						</a>
					</div>
			</div>
		</div>
@endif	

	<canvas id="myChart" ></canvas>


<!-----chat---->


 
            <h5>Latest Transactions</h5>
			<table class="table table-striped table-bordered">
				<thead>
				<tr row="row">
				<th>No</th>
				<th>Created</th>
				<th>Type</th>
				@if(Auth::user()->isAdmin())
				<th>Agent</th>
				@endif
				<th>Tcode</th>
				<th>Sender</th>
				<th>Receiver</th>
				<th>Local Pay</th>
				<th>C_B</th>
				<th>C-ag</th>
				<th>Total</th>
				<th>Status</th>
				</tr>
				<thead>
				<?php $x=1; ?>
		<tbody>	  
		 @foreach($transactions as $transaction)
		   	<tr>
					<td>{{$x++}}</td>
					<td>{{$transaction->created_at->format('d/m/y')}}</td>
					<td>{{ ucfirst($transaction->type)}}</td>
					@if(Auth::user()->isAdmin())
					<td>{{ ucfirst($transaction->user->name)}}</td>
					@endif
					<td>{{ ucfirst($transaction->receipt_number)}}</td>
					<td>{{ ucfirst($transaction->sender_name)}}</td>
					<td>{{ ucfirst($transaction->receiver_name)}}</td>
					<td>{{ ($transaction->amount * $transaction->commission)}}</td>
					<td>{{ ($transaction->commission - $transaction->agent_commission)}}</td>
					<td>{{ $transaction->agent_commission}}</td>
					<td>{{ ($transaction->amount + $transaction->commission)}}</td>
					<td>{{ $transaction->status}}</td>
				</tr>
			  @endforeach
		</tbody>
     </table>
            <a class="btn btn-default" href="{{route('transactions.index')}}">View All Pages</a>
            <hr>
            	@if(Auth::user()->isCustomer())
            		<h5>Latest Receiver</h5>  
				@else 
				<h5>Latest Customers</h5	>
				@endif
            <table class="table table-striped table-bordered">
				<tr>
	               	<th>No</th>
					<th>Created At</th>
	               	<th>Name</th>
					<th>Mobile</th>	
				 </tr>
			<tbody>
	        	<?php $x=1; ?>
				@foreach($customers as $customer)
				<tr>
					<td>{{$x++}}</td>
					<td>{{$customer->created_at->format('d/m/y')}}</td>
					<td>{{ ucfirst($customer->fname).
					"-" . ucfirst($customer->lname) }}</td>
					<td>{{ $customer->mobile}}</td>
						
					</tr>
				@endforeach			  
			</tbody>	  
		 @if(Auth::user()->isCustomer())
		 		<?php $x=1; ?>
				@foreach($receivers as $receiver)
	              <tr>
	                <td>{{$x++}}</td>
					<td>{{$receiver->created_at->format('d/m/y')}}</td>
	                <td>{{ ucfirst($receiver->fname). "-" . ucfirst($receiver->lname) }}</td>
	               <td>{{ $receiver->phone }}</td>
	              </tr>
	             @endforeach
	        @endif
            </table>
            <a class="btn btn-default"
			 href="{{Auth::user()->isCustomer()?route('receivers.index'):route('senders.index')}}">View All</a>
          </div>
        </div>
      </div>

    </section>
    

@endsection

@section('script')

var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [{{App\Transaction::chart()->label}}],
        datasets: [{
            label: 'Your Activites',
            data: [{{App\Transaction::chart()->data}}],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});


   console.log('script running')

@endsection