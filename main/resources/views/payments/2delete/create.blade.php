@extends('layouts.layout')
@section('content')
	
	 <div class="col-md-12">
		<h3>New Payment</h3>
		<div class="well well-lg text-center">
			<strong class="text-center">
				Payment of £{{number_format($member->agentTotalPayment(),2)}} for {{$member->countPendingPayment()}} Transactions
			</stronng>
			<br><br>
			<div class="row">
				<div class = "col-md-4 ">
					{!! Form::open(['method'=>'POST', 'action'=> 'PaymentsController@store', 'files'=>true]) !!}
						{!! Form::hidden('user_id',$member->id) !!}
						{!! Form::hidden('agent_name',$member->name) !!}
						{!! Form::hidden('user_role',$member->type) !!}	
						{!! Form::hidden('payment_type','transaction') !!}
						{!! Form::hidden('manager_id',Auth::user()->id) !!}
						{!! Form::hidden('total',$member->agentTotalPayment()) !!}				
				</div>
			</div>	
				<br/>
					<div class="form-group form-group-sm" >
						{!! Form::label('Amount', 'Amount')!!}
						{!! Form::text
							('amount',$member->agentTotalPayment(), ['class'=>'form-control','id'=>'amount',])!!}

							{!! Form::submit('Make Payment', ['class'=>'btn btn-warning btn-block'])!!}
					</div>
				
				{!! Form::close() !!}
		
		</div>

	</div>
@endsection

@section('script')
		$(function(){
			$('#partpay').on('click', function(){
				$('#displayDivPartPay').toggle()
	})
		
	})
@endsection





