@extends('layouts.layout')
@section('content')
	
	 <div class="col-md-12">
		
			
					<h4>Agents Payments</h4>	
				
			
					<table class="table table-striped table-bordered tablesorter">
			<thead>
					<tr>
						<th>No </th>
						<th>Name</th>	
						<th colspan="2" class = "text-center">Transactions <small class="help-block">payments by Agent</small> </th>
						<th colspan="2" class = "text-center">Commission <small class="help-block">payments by Admin</small></th>			
					</tr>
				</thead>
				
				
				<tbody>
						<?php $avater="noavatar.jpg";$x=0; 
							foreach(App\User::whereType('agent')->get() as $agent): ?>
					<tr>
						<td>
							<input type="checkbox" id="check_list" name="check_list" value="">
							<?php echo $x=$x+1; ?>					
						</td>
										
						<td> {{str_limit(ucfirst($agent->fname." ".$agent->lname),10)}} </td>
<!----Transactions---->										
						<td>
							<span>
								<a href="">
									<small class="text-danger">Pending
									 -£{{$agent->pending_outstanding->sum('amount')}}
									/ [ {{$agent->pending_outstanding->count()}} ] ,
									</small>
								 </a>
								 <br/> <br/>
								 <a href="">
								 	<small class="text-primary">Part-Paid
								 	- £{{$agent->transaction_part_payments->sum('amount')}}</a>
									</small>
								<a href="">
							</span>
						</td>
						<td class="text-center">
							<a  class="btn btn-primary btn-block" href="{{route('payments.agent',$agent->id)}}">Paid </a>
							<a  class="btn btn-default btn-block" href="">View Paid </a>
						</td>
<!----Commission---->											
						<td>
						<span>
								<a href="">
									<small class="text-danger">Pending
									 -£{{$agent->pending_outstanding->sum('amount')}}
									/ [ {{$agent->pending_outstanding->count()}} ] ,
									</small>
								 </a>
								 <br/> <br/>
								 <a href="">
								 	<small class="text-primary">Part-Paid
								 	- £{{$agent->transaction_part_payments->sum('amount')}}</a>
									</small>
								<a href="">
							</span>
						<td class="text-center">
							<a  class="btn btn-success  " href="">Clear</a>
							<a  class="btn btn-default btn-block" href="">View Cleared </a>
						</td>
				</tr>
									
										<?php endforeach; ?>
					
										
			</table>
					
						
			
		</div>
	
@endsection





