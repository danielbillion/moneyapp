@extends('layouts.layout')
@section('content')
	
	 <div class="col-md-12">
		
			
					<h4>Agents Payments</h4>	
				
			
					<table class="table table-striped table-bordered tablesorter">
			<thead>
					<tr>
						<th>No </th>
						<th>Name</th>	
						<th colspan="2" class = "text-center">Transactions <small class="help-block">payments by Agent</small> </th>
						<th colspan="2" class = "text-center">Commission <small class="help-block">payments by Admin</small></th>			
					</tr>
				</thead>
				
				
				<tbody>
						<?php $avater="noavatar.jpg";$x=0; 
							foreach(App\User::whereType('agent')->get() as $agent): ?>
					<tr>
						<td>
							<input type="checkbox" id="check_list" name="check_list" value="">
							<?php echo $x=$x+1; ?>					
						</td>
										
						<td> {{str_limit(ucfirst($agent->fname." ".$agent->lname),10)}} </td>
<!----Transactions---->										
						<td>
							<span>
								<a href="">
									<small class="text-danger">Pending
									 -£{{$agent->pending_outstanding->sum('amount')}}
									/ [ {{$agent->pending_outstanding->count()}} ] ,
									</small>
								 </a>
								 
								 <a href="">
								 	<small class="text-primary">part-Paid
								 	- £{{$agent->transaction_part_payments->sum('amount')}}</a>
									</small>
								<a href="">
							</span>
						</td>
						<td>
							<a  class="btn btn-default" href="">New</a>
							<a  class="btn btn-primary" href="">View</a>
						</td>
<!----Commission---->											
						<td>
						<span>
								<a href="">
									<small class="text-danger">Pending
									 -£{{$agent->pending_outstanding->sum('amount')}}
									/ [ {{$agent->pending_outstanding->count()}} ] ,
									</small>
								 </a>
								 
								 <a href="">
								 	<small class="text-primary">part-Paid
								 	- £{{$agent->transaction_part_payments->sum('amount')}}</a>
									</small>
								<a href="">
							</span>
						<td>
							<a  class="btn btn-default" href="">New</a>
							<a  class="btn btn-primary" href="">View</a>
						</td>
				</tr>
									
										<?php endforeach; ?>
					
										
			</table>
					
						
			
		</div>
	
@endsection





