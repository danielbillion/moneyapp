@extends('layouts.layout')
@section('content')
	<div class="row well" >
		{!! Form::open(['method'=>'POST', 'action'=> 'PaymentsController@post_search', 'files'=>true]) !!}	
							<div class="col-md-7" >
								<div class="form-group input-group form-group-sm">
									<span class="input-group-addon"><i class="fa fa-filter"></i></span>
									{!! Form::text('ref',null,['class'=>'form-control',
										'id'=>'getTransactions','placeholder'=>'Search by payment reference']) !!}
								</div>
							</div>

							<div class="col-md-6">
								{!! Form::label('Start Date', 'Start Date')!!}
								<div class="form-group input-group form-group-sm">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									{!! Form::date('from', \Carbon\Carbon::now(), ['class'=>'form-control','id'=>'datefrom'])!!}
								</div>
							</div>

							<div class="col-md-6">
								{!! Form::label('End Date', 'End Date')!!}
								<div class="form-group input-group form-group-sm">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									{!! Form::date('to',\Carbon\Carbon::now(), ['class'=>'form-control','id'=>'dateto'])!!}
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="form-group input-group">
										<span class = "input-group-addon"><strong>Type</strong></span>
									{!! Form::select('type',['cash'=>'cash','card'=>'card','paypal'=>'paypal'],
									null,['class'=>'form-control']) !!}
								</div>
							</div>
							{!! Form::submit('Apply',['class'=>'btn btn-danger col-md-6 btn-block']) !!}
			
			{!! Form::close() !!}
			
	</div>
	
	<br/>
	 <div class="row">
					<div class="col-md-3">
						<ol class="breadcrumb">
						<li class="active">{{$header?$header:""}} Payments</li>
						</ol>
					</div>
	@if(Auth::user()->type == 'admin')			
					<div class="col-md-4 col-md-offset-2">
						<ol class="breadcrumb">
							<li class="active"><a href="{{route('payments.user_type','customer')}}">Customer</a></li>
							<li class="active"><a href="{{route('payments.user_type','agent')}}">Agent</a></li>
						</ol>
					</div>			
	</div>
	@endif
	@if(count($payments) > 0 )
	<div class="row">
		<div class="col-md-12">
		
			 <table id="sort-table" class="table table-striped table-bordered tablesorter">
				<thead>
							<tr>
								<th>No </th>
								<th>Date <i class=""></i></th>
								<th>Member Type</th>
								<th>Name</th>
								<th>Ref  </th>
								<th>Amount</th>
								<th>Payment For</th>
								<th>Payment Type </th>
							</tr>
					</thead>
					<tbody>
						

				@foreach($payments as $key=>$payment)
							<tr>
								<td>{{$key+1}}</td>
								<td>{{$payment->created_at->format('d/m/Y')}}</td>
								<td>{{$payment->user->type}}</td>
								<td>{{$payment->user->name}}</td>
								<td>{{$payment->ref}}</td>
								<td>{{$payment->amount}}</td>
								<td>{{$payment->for}}</td>
								<td>{{$payment->mode}}</td>
							 </tr>
					 @endforeach
							<tr>
								<td class = "text-center" colspan = "5">Total</td>
								<td colspan = "3">{{$payments->sum('amount')}}</td>
							</tr>
			 
						</tbody>
			</table>
			
			
				<div class="row">
					<div class="col-md-12 text-center">
						{{$payments->render()}}
					</div>
				</div>
			
		</div>
		
	</div>
	@else
		<p class = "text-center text-danger"> <strong>Search Result Unavailable</strong></p>
		
		@endif

@endsection

@section('script')
	$(function(){	
<!--confirm Delete --> 
					$(".delete").on("submit", function(){
					return confirm("Are you sure?");
						});

<!--Trigger Autocomplete --> 
				 $('#clicksearch').on('click', function(e) {
				      $('#datesearch').toggle();
				      e.preventDefault();
				    });
				 url  = $('#myurl').val() + '/transaction/search'  		
			});
@endsection

