@extends('layouts.layout')
@section('content')

<body>
	<div class="container">
		<div class='row'>
			<div class='col-md-5 col-md-offset-1 well well-lg'>
			<h4>Pay With</h4>
			<div class= "row">
					<div class="col-md-5 col-md-offset-2">
						<strong>Member</strong><br/>
					</div>
					<div class="col-md-3">
						<strong> {{$paid_by}}</strong><br/>
					</div>
			</div>
<p>
			<div class= "row">
					<div class="col-md-5 col-md-offset-2">
						<strong>Total</strong>
					</div>
					<div class="col-md-3">
						<strong>£{{number_format($amount,2)}}</strong>
					</div>
			</div>

			<p>
			<div class= "row">
					<div class="col-md-5 col-md-offset-2">
						<strong>Payment For</strong>
					</div>
					<div class="col-md-3">
						<strong>{{$type}}</strong>
					</div>
			</div>
<!-- Stripe error  -->	
<br/><br/>
<h1><i class="fa fa-credit-card-alt"></i> 
<a  role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
						Using Credit / Debit card
        </a></h1>
<hr>
				<div class='row'>
						<div class='col-md-12 error form-group hide'>
							<div class='alert-danger alert'>Please correct the errors and try
								again.</div>
						</div>
					</div>
					<div id="collapseExample" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
				<script src='https://js.stripe.com/v2/' type='text/javascript'></script>
					{!! Form::open(['method'=>'POST', 
								'action'=> 'PaymentsController@stripe_store',
								'data-stripe-publishable-key'=>$type =='subscription'?env('STRIPE_PUBLIC_KEY'):env('CLIENT_STRIPE_PUBLIC_KEY'),
								'class'=>'require-validation',
								'data-cc-on-file'=>'false',
								'id'=>'payment-form',
								 'files'=>true]) !!}
					{{ csrf_field() }}
					{!!Form::hidden('mode','card') !!}
					{!!Form::hidden('ref',$ref) !!}
					{!!Form::hidden('id',$id) !!}
					{!!Form::hidden('for',$type) !!}
					{!!Form::hidden('amount',$amount) !!}
					{!!Form::hidden('user_id',Auth::id()) !!}
					{!!Form::hidden('user_type',Auth::user()->type) !!}
					<div class='form-row'>
						<div class='col-xs-12 form-group required'>
							<label class='control-label'>Name on Card</label> <input
								class='form-control' size='4' type='text'>
						</div>
					</div>
				
					<div class='form-row'>
						<div class='col-xs-12 form-group card required'>
							<label class='control-label'>Card Number</label> <input
								autocomplete='off' class='form-control card-number' size='20'
								type='text'>
						</div>
					</div>
					
					<div class='form-row'>
						<div class='col-xs-4 form-group cvc required'>
							<label class='control-label'>CVC</label> <input
								autocomplete='off' class='form-control card-cvc'
								placeholder='ex. 311' size='4' type='text'>
						</div>
						<div class='col-xs-4 form-group expiration required'>
							<label class='control-label'>Expiration</label> <input
								class='form-control card-expiry-month' placeholder='MM' size='2'
								type='text'>
						</div>
						<div class='col-xs-4 form-group expiration required'>
							<label class='control-label'> </label> <input
								class='form-control card-expiry-year' placeholder='YYYY'
								size='4' type='text'>
						</div>
					</div>
					
					
					
					<div class='form-row'>
						<div class='col-md-12 form-group'>
							<input class='form-control btn btn-primary submit-button'
								type='submit' style="margin-top: 10px;" value = "pay">
						</div>
					</div>
					
              {{Form::close()}}
	</div>
                
<!-- Paypal-->
        {!! Form::open(['method'=>'POST', 'action'=> 'PaymentsController@paypal_store', 'files'=>true]) !!}
    	        {{ csrf_field() }}
							
                <input id="mode" type="hidden" value = 'paypal' name="mode">
								<input id="ref" type="hidden" value = {{$ref}} name="ref">
								<input id="id" type="hidden" value = {{$id}} name="id">
								<input id="type" type="hidden" value = {{$type}} name="for">
								<input id="amount" type="hidden" value = {{$amount}} name="amount">
                <input id="user_id" type="hidden" value = {{Auth::id()}} name="user_id">
								<input id="user_type" type="hidden" value = "{{Auth::user()->type}}" name="user_type"></p>
								<button class="btn btn-block">	<strong>Using</strong> 
                <img src="https://www.paypalobjects.com/webstatic/mktg/Logo/pp-logo-100px.png" border="0" alt="PayPal Logo"/>
								</button>
						
						<hr>
    	{{Form::close()}}

						<p/>@if($type === 'subscription')
								<h1> Bank Transfer</h1><hr><p>
									<strong>Account No: 03028445</strong><p>
									<strong>Sort Code: 207291</strong><p>
									<strong>Ref: {{env('APP_NAME')}}</strong><hr>
								@endif
								</div>
		    </div>
		</div>
	
						

@endsection
@section('script')
$('#collapseTwo').collapse('toggle')
$(function() {
			  $('form.require-validation').bind('submit', function(e) {
			    var $form         = $(e.target).closest('form'),
			        inputSelector = ['input[type=email]', 'input[type=password]',
			                         'input[type=text]', 'input[type=file]',
			                         'textarea'].join(', '),
			        $inputs       = $form.find('.required').find(inputSelector),
			        $errorMessage = $form.find('div.error'),
			        valid         = true;
			    $errorMessage.addClass('hide');
			    $('.has-error').removeClass('has-error');
			    $inputs.each(function(i, el) {
			      var $input = $(el);
			      if ($input.val() === '') {
			        $input.parent().addClass('has-error');
			        $errorMessage.removeClass('hide');
			        e.preventDefault(); // cancel on first error
			      }
			    });
			  });
			});
			$(function() {
			  var $form = $("#payment-form");
			  $form.on('submit', function(e) {
			    if (!$form.data('cc-on-file')) {
			      e.preventDefault();
			      Stripe.setPublishableKey($form.data('stripe-publishable-key'));
			      Stripe.createToken({
			        number: $('.card-number').val(),
			        cvc: $('.card-cvc').val(),
			        exp_month: $('.card-expiry-month').val(),
			        exp_year: $('.card-expiry-year').val()
			      }, stripeResponseHandler);
			    }
			  });
			  function stripeResponseHandler(status, response) {
			    if (response.error) {
			      $('.error')
			        .removeClass('hide')
			        .find('.alert')
			        .text(response.error.message);
			    } else {
			      // token contains id, last4, and card type
			      var token = response['id'];
			      // insert the token into the form so it gets submitted to the server
			      $form.find('input[type=text]').empty();
			      $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
			      $form.get(0).submit();
			    }
			  }
			})
@endsection




