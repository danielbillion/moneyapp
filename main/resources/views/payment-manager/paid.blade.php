@extends('layouts.layout')
@section('content')
	
	 <div class="col-md-12">
		<div class="panel panel-default">
				<div class=" panel-headingt">
						
				</div>
			<div class=" panel-body">
					<table class="table table-striped table-bordered tablesorter">
			<thead>
					<tr>
						<th>No </th>
						<th>Name <i class=""></i></th>
						<th>Amount	</th>
						<th>Total	</th>
						<th>Balance	</th>
						<th>Status	</th>
									
					</tr>
				</thead>
				
				<h3>Payments By {{$user->name}}</h3>
				<tbody>
						<?php $avater="noavatar.jpg";$x=0; 
							foreach($payments as $payment): ?>
					<tr>
						<td>
							<input type="checkbox" id="check_list" name="check_list" value="">
							<?php echo $x=$x+1; ?>					
						</td>
										
						<td>  {{str_limit(ucfirst($payment->agent_name),10)}} </td>
										
						<td>
							{{$payment->amount}}
						</td>
											
						<td>
							{{$payment->total}}
						</td>
																				
						<td>
							{{$payment->balance}} 
						</td>

						<td>
							{{$payment->fully_paid== 0?'uncompleted': 'completed'}} 
						</td>

						<td>
							{!! Form::open(['method'=>'DELETE', 'action'=>[ 'PaymentsController@destroy', $payment->id ], 'files'=>true]) !!}	
						 	<td>{!! Form::submit('Delete', ['class'=>'btn btn-danger btn-block'])!!}</td>
						 	{!! Form::close() !!}
						</td>
										
										
					</tr>
									
										<?php endforeach; ?>
					
										
			</table>
					<div class="row">
					<div class="col-md-5 col-md-offset-3 text-center">
						
					</div>
				</div>
						
			</div>
		</div>
	</div>
@endsection





