@extends('layouts.layout')
@section('content')
	
	 <div class="col-md-12">
		<h3>New Payment</h3>
		<div class="well well-lg text-center">
			<strong class="text-center">
				Payment of £{{number_format($manager->managerTotalPayment(),2)}} for {{$manager->countManagerPendingPayment()}} Transactions
			</stronng>
			<br><br>
			<div class="row">
				<div class = "col-md-4 ">
					{!! Form::open(['method'=>'POST', 'action'=> 'ManagerPaymentsController@store', 'files'=>true]) !!}
						{!! Form::hidden('manager_name',$manager->name) !!}	
						{!! Form::hidden('payment_type','remittance') !!}
						{!! Form::hidden('manager_id',$manager->id) !!}
						{!! Form::hidden('total',$manager->managerTotalPayment()) !!}

				</div>
			</div>	
				<br/>
					<div class="form-group form-group-sm" >
						{!! Form::label('Amount', 'Amount')!!}
						{!! Form::text
							('amount',$manager->managerTotalPayment(), ['class'=>'form-control','id'=>'amount',])!!}

							{!! Form::submit('Make Payment', ['class'=>'btn btn-warning btn-block'])!!}
					</div>
				
				{!! Form::close() !!}
		
		</div>

	</div>
@endsection

@section('script')
		$(function(){
			$('#partpay').on('click', function(){
				$('#displayDivPartPay').toggle()
	})
		
	})
@endsection





