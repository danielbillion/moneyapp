@extends('layouts.layout')
@section('content')
	
	 <div class="col-md-12">
		<div class="panel panel-default">
				<div class=" panel-headingt">
						
				</div>
			<div class=" panel-body">
					<table class="table table-striped table-bordered tablesorter">
						<h3>Managers Payment</h3>
			<thead>
					<tr>
						<th>No </th>
						<th>Name <i class=""></i></th>
						<th colspan="4">Make Part or Full Payments	</th>
									
					</tr>
				</thead>
				
				
				<tbody>
						<?php $avater="noavatar.jpg";$x=0; 
							foreach(App\Manager::whereType('manager')->get() as $manager): ?>
					<tr>
						<td>
							<input type="checkbox" id="check_list" name="check_list" value="">
							<?php echo $x=$x+1; ?>					
						</td>
										
						<td> {{str_limit(ucfirst($manager->fname." ".$manager->lname),10)}} </td>
<!----View Profile---->										
						<td>
							<a href="{{route('transactions.search',
									['type' =>'payments','value'=>$manager->id])}}" class="text-danger">
							 Outstanding £{{$manager->managerTotalPayment()}}</a> / {{$manager->countManagerPendingPayment()}} 
						</td>
<!----Customers---->											
						<td>
							<td>
							<a  class = "btn btn-primary" href="{{route('managerpayments.edit',$manager->id)}}"> Make Payemnt </a>
						</td>
																				
						</td>
<!----Peniding Transaction---->											
						<td>
						<a href="{{route('managerpayments.show',$manager->id)}}"
						class="btn btn-default"><i class="fa fa-money" aria-hidden="true"> 
						</i> View Payments 
											</a>
																																			
						</td>
										
										
					</tr>
									
										<?php endforeach; ?>
					
										
			</table>
					<div class="row">
					<div class="col-md-5 col-md-offset-3 text-center">
						
					</div>
				</div>
						
			</div>
		</div>
	</div>
@endsection





