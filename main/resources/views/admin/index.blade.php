
@extends('layouts.layout')
@section('content')
    	<canvas id="myChart" ></canvas>
   	 <div class="col-md-12">
		  <section id="notice"><div class="well well-lg">
		  <i class="glyphicon glyphicon-dashboard"></i> 
		  <span class="label label-info pull-left">Last Login:12-03-2016 08:12</span>
		  <span class="label label-info pull-right">Bank Details</span>
		  </section>
          <!-----How It Works---->
		  <div id="howitwork">
			  <div class="well well-lg">
					<h2> Business Summary</h2>
				<div class="row">
					<div class = "col-sm-6">
						<h4 class="text-center"> Agents</h4>
						<ul class="list-group">
							<li class="list-group-item  ">Total Number: <span class="badge">{{@auth::user()->totalAgents()}}</span></li>
							<li class="list-group-item text-info">Active: <span class="badge">{{@auth::user()->activeAgents()}}</span></li>
							<li class="list-group-item ">Inactive: <span class="badge">{{@auth::user()->inactiveAgents()}}</span></li>
							<li class="list-group-item text-danger">Pending Transaction: <span class="badge">{{App\Transaction::pendingAgentTransactions()}}</span></li>
						</ul>
						
					</div>
					<div class = "col-sm-6">
						<h4 class="text-center"> Customers</h4>
						<ul class="list-group">
							<li class="list-group-item  ">Total Number: <span class="badge">{{@auth::user()->totalCustomers()}}</span></li>
							<li class="list-group-item text-info">Active: <span class="badge">{{@auth::user()->activeCustomers()}}</span></li>
							<li class="list-group-item ">Inactive: <span class="badge">{{@auth::user()->inactiveCustomers()}}</span></li>
							<li class="list-group-item text-danger">Pending Transaction: <span class="badge">{{App\Transaction::pendingCustomerTransactions()}}</span></li>
						</ul>
					</div>
				</div>
				 
			</div>
		  </div>
		   
		   
		   
            <h3>Latest Transactions</h3>
            <table class="table table-striped">
			<thead>
			<tr row="row">
			<th>No</th>
			<th>Date</th>
			<th>Tcode</th>
			<th>Sender</th>
			<th>Receiver</th>
			<th>C_B</th>
			<th>C-ag</th>
			<th>Total</th>
			<th>Status</th>
			</tr>
			<thead>
             <?php $x=0; foreach($transactions as $transaction): ?>
				 
				 
			
			   <tr>
                <td><?php  echo $x=$x+1;?></td>
                <td><?php  echo ($transaction->created_at)->diffForHumans();?></td>
				<td><?php  echo ucfirst($transaction->receipt_number);?></td>
				<td><?php  echo ucfirst($transaction->sender_name);?></td>
				<td><?php  echo ($transaction->amount * $transaction->commission);?></td>
				<td><?php  echo ($transaction->commission - $transaction->agent_commission);?></td>
				<td><?php  echo $transaction->agent_commission;?></td>
				<td><?php  echo ($transaction->amount + $transaction->commission);?></td>
				<td><?php  echo $transaction->status;?></td>
                
              </tr>
			  <?php endforeach; ?>
             
             
            </table>
            <a class="btn btn-default" href="prevTrans.php">View All Pages</a>
            <hr>

          
				 <h3>Just Registered</h3>
				
				
            <table class="table table-striped">
				<tr>
	                <th>No</th>
	                <th>Date</th>
	                <th>Name</th>
					 <th>Mobile</th>
					
	             </tr>
					<?php $x=0; foreach($users as $user): ?>
	              <tr>
	                <td><?php echo $x=$x+1; ?></td>
	                <td><?php  echo ($user->created_at)->diffForHumans();?></td>
	                <td><?php echo ucfirst($user->fname). "-" . ucfirst($user->lname); ?></td>
	               	<td><?php echo $user->mobile; ?></td>
	               	
	              </tr>
	              <?php endforeach; ?>
				  
            </table>
            <a class="btn btn-default" href="sendersReceivers.php">View All</a>
          </div>
        </div>
      </div>

    </section>
    

@endsection

@section('script')

	 var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Customers", "Receivers", "Transactions"],
        datasets: [{
            label: 'Your Activites',
            data: [12, 19, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});

@endsection