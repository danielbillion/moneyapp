@extends('layouts.layout')
@section('content')
    	

    <div class="row">
    	<div class="text-center col-sm-11 col-sm-offset-1">
    		<div class="panel panel-default">
				<div class="panel-heading">
						<h6>Banks For Transaction</h6>
				</div>
				<div class="panel-body">
						{!! Form::open(['method'=>'POST', 'action'=> 'BankController@store', 'files'=>true]) !!}
							<div class="row">
								<div class="col-md-3 col-md-offset-3">
									<div class="form-group">
										{!! Form::label('Bank','Bank') !!}
										{!! Form::radio('type','b',true,['id'=>'bank']) !!}
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										{!! Form::label('Pick Up','Pick Up') !!}
										{!! Form::radio('type','p',false,['id'=>'pickup']) !!}
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-7 col-md-offset-2">
									<div class="form-group form-group-sm">
										{!! Form::label('New Bank','New Bank') !!}
										{!! Form::text('name',null,['class'=>'form-control col-md-12 btn-block','placeholder'=>'New Bank','required'=>'required']) !!}
									</div>
								</div>
							</div>
							<p></p>
							<div class="row">
							{!! Form::submit('Submit',['class'=>'btn btn-primary col-md-12 btn-block']) !!}
				</div>

						{!! Form::close() !!}

				</div>
			</div>
		
    		<table  id="sort-table" class="table table-striped table-bordered tablesorter">
						<thead>
									<tr>
										<th>NO</th>
										<th>BankS</th>
										<th>Type</th>	
										<th colspn = "2">Actions</th>
									</tr>
							</thead>
							<tbody>
									 <?php $x=0; ?>
									 @foreach($banks as $bank)
										<tr>
											<td><?php  echo $x=$x+1;?></td>
											<td>{{ucfirst($bank->name)}}</td>
											<td>{{$status=$bank->status =='p' ? 'Pick Up': 'Bank Account'}} </td>
											<td>
											<a href= {{route('bank.edit', $bank->id)}} class="btn btn-default btn-block">Edit</a>
												{!! Form::open(['method'=>'DELETE', 'action'=> ['BankController@destroy',$bank->id], 'files'=>true]) !!}
													
														{!!Form::submit('Delete',['class'=>'btn btn-danger btn-block']) !!}
												{!! Form::close() !!}
											</td>
										 </tr> 
										 
									@endforeach						
							</tbody>
								
							</table>
							{{$banks->render()}}
				</div>
    </div>
    

@endsection

