@extends('layouts.layout')
@section('content')
    	
    <div class="row">
    	<div class="col-sm-11 col-sm-offset-1">
    		<div class="panel panel-default">
				<div class="panel-heading text-center">
						<h5 class= "text-center text-primary">Edit Currency Setup</h5>
				</div>
				<div class="panel-body">
						{!! Form::model($currency_value,['method'=>'PATCH', 'action'=> ['CurrenciesController@update',$currency_value->id], 'files'=>true]) !!}
						{!! Form::hidden('user_id',Auth::id()) !!}
						{!! Form::hidden('id',$currency_value->id) !!}
							<br/><br/>
							
							<p class="text-center well well-sm">Destination: <strong>{{$currency_value->destination}} </strong> <p>	
							<div class="row">
							<div class="col-md-6">
									<div class="form-group form-group-sm">
										{!! Form::label('Currency Code','Currency Code') !!}
										{!! Form::select('status',[0=>'Disable','1'=>'Enable'],null,['class'=>'form-control col-md-12 btn-block','required'=>'required']) !!}
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group form-group-sm">
										{!! Form::label('Income Category','Income Category') !!}
										{!! Form::select('income_category',$income_types,null,['class'=>'form-control col-md-12 btn-block','required'=>'required']) !!}
									</div>
								</div>
								
							</div>
							<p>	</p>
							<div class="row">
							 	{!! Form::submit('Submit',['class'=>'btn btn-primary btn-lg col-md-12 btn-block']) !!}
							</div>

						{!! Form::close() !!}

				</div>
			</div>
		
    		<table  id="sort-table" class="table table-striped table-bordered tablesorter">
						<thead>
									<tr>
										<th>No</th>
										<th>Setup</th>
										<th>Code</th>
										<th>Origin</th>
										<th>Origin Symbol</th>	
										<th>Destination</th>
										<th>Destination Symbol</th>
										<th>Income Type</th>
										<th colspan="2">Actions</th>
									</tr>
							</thead>
							<tbody>
									 <?php $x=0; ?>
					@foreach($currencies as $key => $currency)
										<tr>
											<td>{{$key + 1 }}</td>
											<td>{{ $currency->created_at->format('d/m/Y')}}, {{ $currency->created_at->diffForHumans() }}</td>
											<td>{{$currency->code}}</td>
											<td>{{$currency->origin}}</td>
											<td>{{$currency->origin_symbol}}</td>
											<td>{{$currency->destination}}</td>
											<td>{{$currency->destination_symbol}}</td>
											<td>{{$currency->income_category}}</td>
											
											<td>
												<a href= {{route('currencies.edit', $currency->id)}} class="btn btn-warning btn-block">Edit</a>
												
											</td>
										 </tr> 
										 
									@endforeach						
							</tbody>
									
							</table>
							{{$currencies->render()}}
				</div>
    </div>
    

@endsection

