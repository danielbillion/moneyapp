@extends('layouts.layout')
@section('content')
    	
    <div class="row">
    	<div class="col-sm-11 col-sm-offset-1">

				<a class="btn btn-primary" 
						role="button" data-toggle="collapse"
						href="#collapse_currency" 
						aria-expanded="false" 
						aria-controls="collapseExample">
								New Currency
				 </a>

    		<div class="panel panel-default collapse" id = "collapse_currency">
				<div class="panel-heading text-center">
						<h6 class= "text-center text-primary">New Currency Setup</h6>
				</div>
				<div class="panel-body">
						{!! Form::open(['method'=>'POST', 'action'=> 'CurrenciesController@store', 'files'=>true]) !!}
						{!! Form::hidden('user_id',Auth::id()) !!}
							<br/><br/>
						
							<p><p>	
							<div class="row">
								<div class="col-md-6">
									<div class="form-group form-group-sm">
										{!! Form::label('Destination','Destination') !!}
										{!! Form::select('destination',$destinations,null,['class'=>'form-control col-md-12 btn-block','placeholder'=>'Destination','required'=>'required','id'=>'destination']) !!}
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group form-group-sm">
										{!! Form::label('Income Category','Income Category') !!}
										{!! Form::select('income_category',$income_types,null,['class'=>'form-control col-md-12 btn-block','required'=>'required']) !!}
									</div>
								</div>
								
							</div>
							<p>	</p>
							<div class="row">
							 	{!! Form::submit('Submit',['class'=>'btn btn-info btn-lg col-md-12 btn-block']) !!}
							</div>

						{!! Form::close() !!}

				</div>
			</div>
		
    		<table  id="sort-table" class="table table-striped table-bordered tablesorter">
						<thead>
									<tr>
										<th>No</th>
										<th>Setup</th>
										<th>Code</th>
										<th>Origin</th>
										<th>Origin Symbol</th>	
										<th>Destination</th>
										<th>Destination Symbol</th>
										<th>Income Type</th>
										<th colspan="2">Actions</th>
									</tr>
							</thead>
							<tbody>
									 <?php $x=0; ?>
					@foreach($currencies as $key => $currency)
										<tr>
											<td>{{$key + 1 }}</td>
											<td>{{ $currency->created_at->format('d/m/Y')}}, {{ $currency->created_at->diffForHumans() }}</td>
											<td>{{$currency->code}}</td>
											<td>{{$currency->origin}}</td>
											<td>{{$currency->origin_symbol}}</td>
											<td>{{$currency->destination}}</td>
											<td>{{$currency->destination_symbol}}</td>
											<td>{{$currency->income_category}}</td>
											
											<td>
												<a href= {{route('currencies.edit', $currency->id)}} class="btn btn-warning btn-block">Edit</a>
											</td>
										 </tr> 
										 
									@endforeach						
							</tbody>
									
							</table>
							{{$currencies->render()}}
				</div>
    </div>
    

@endsection



