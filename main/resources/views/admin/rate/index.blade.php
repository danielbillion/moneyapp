@extends('layouts.layout')
@section('content')
    	
			
    <div class="row">
    	<div class="col-sm-9 col-sm-offset-1">
    		<div class="panel panel-default">
				<div class="panel-heading">
						<h6>Set Rate</h6>
				</div>
				<div class="panel-body">
						{!! Form::open(['method'=>'POST', 'action'=> 'RateController@store', 'files'=>true]) !!}
						<div class="row">
								<div class="col-md-12">
									<div class="form-group form-group-sm">
										{!! Form::label('User','Users') !!}
										{!! Form::select('user_id',$users,null,['class'=>'form-control col-md-12 btn-block']) !!}
									</div>
								</div>
								
							</div>
							<br/><br/>	
							<div class="row">
								<div class="col-md-7">
									<div class="form-group form-group-sm">
									{!! Form::label('Currency Code','Currency Code') !!}
										{!! Form::select('currency_id',$currencies,null,['class'=>'form-control','required'=>'required','id'=>'currency_id']) !!}
									</div>
								</div>
								<div class="col-md-5" id = "income_commission"  style = "display:none">
									<div class="form-group form-group-sm">
										{!! Form::label('Rate','Rate,  £1 = ') !!}
										{!! Form::text('rate',0,['class'=>'form-control','placeholder'=>'Main Rate','required'=>'required','id'=>'rate']) !!}
									</div>
								</div>
								
							</div>
							<p/><p/>
							<div id = "income_profit" class="row" style = "display:none">
								<div class="col-md-6">
									<div class="form-group form-group-sm">
										{!! Form::label('Bou Rate','Bou Rate') !!}
										{!! Form::text('bou_rate',0,['class'=>'form-control','placeholder'=>'Bou Rate','required'=>'required','id'=>'bou_rate']) !!}
									</div>
								</div>
								<div class="col-md-6">	
									<div class="form-group form-group-sm">
										{!! Form::label('Sold Rate','Sold Rate') !!}
										{!! Form::text('sold_rate',0,['class'=>'form-control','placeholder'=>'Sold Rate','required'=>'required','id'=>'sold_rate']) !!}
									</div>
								</div>
								
							</div>
							<p></p>
								
							<p>	</p>
							<div class="row">
							 	{!! Form::submit('Submit',['class'=>'btn btn-primary btn-lg col-md-12 btn-block']) !!}
							</div>

						{!! Form::close() !!}

				</div>
			</div>
			<h6>Rate History</h6>
    		<table  id="sort-table" class="table table-striped table-bordered tablesorter">
						<thead>
									<tr>
										<th>No</th>
										<th>Date</th>
										<th>Member</th>
										<th>Rate</th>	
										<th>Bou</th>	
										<th>Sold</th>	
										<th>Currrency</th>	
										<th>Actions</th>
									
										
									</tr>
							</thead>
							<tbody>
									 <?php $x=0; ?>
					@foreach($rates as $rate)
										<tr>
											<td><?php  echo $x=$x+1;?></td>
											<td>{{ $rate->created_at->format('d/m/Y')}}, {{ $rate->created_at->diffForHumans() }}</td>
											<td>{{$rate->user->name}}</td>
											<td>{{$rate->rate}}</td>
											<td>{{$rate->bou_rate}}</td>
											<td>{{$rate->sold_rate}}</td>
											<td>{{$rate->currency->code}}</td>
											<td>
											<a href= {{route('rate.edit', $rate->id)}} class="btn btn-default btn-block">Edit</a>
												{!! Form::open(['method'=>'DELETE', 'action'=> ['RateController@destroy',$rate->id], 'files'=>true]) !!}
													
														{!!Form::submit('Delete',['class'=>'btn btn-danger']) !!}
												{!! Form::close() !!}
											</td>
										 </tr> 
										 
									@endforeach						
							</tbody>
								
							</table>
							{{$rates->render()}}
				</div>
    </div>
    

@endsection

@section('script')
			code_id = $('#currency_id').find(":selected").val()
					show(code_id)
			
			$('#currency_id').on('change', function(e) {
					code_id = this.value;
					show(code_id)   
			})

			function show(){
				$.ajax({
						type:'GET',
						url:"{{route('currencies.show','')}}" + '/' + code_id	,
						error:function(error){ console.log(error)},
						success:function(response){
							if(response.income_category == 'profit'){
								$('#income_profit').show()
								$('#income_commission').hide()
							}else{
								$('#income_commission').show()
								$('#income_profit').hide()
							}
						}
					})
			}
			
@stop

