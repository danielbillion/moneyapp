@extends('layouts.layout')
@section('content')
    	
    <div class="row">
    	<div class="col-sm-11 col-sm-offset-1">
    		<div class="panel panel-default">
				<div class="panel-heading">
						<h6>Set Commission</h6>
				</div>
				<div class="panel-body">
						{!! Form::open(['method'=>'POST', 'action'=> 'CommissionController@store', 'files'=>true]) !!}
						<div class="row">
								<div class="col-md-12">
									<div class="form-group form-group-sm">
										{!! Form::label('User','Users') !!}
										{!! Form::select('user_id',$users,null,['class'=>'form-control col-md-12 btn-block']) !!}
									</div>
								</div>
								
							</div>
							<br/>
							<div class="row">
							<div class="col-md-12">
									<div class="form-group form-group-sm">
									{!! Form::label('Currency Code','Currency Code') !!}
										{!! Form::select('currency_id',$currencies,null,['class'=>'form-control','required'=>'required','id'=>'currency_id']) !!}
									</div>
								</div>
							</div>
							<br/>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group form-group-sm">
										{!! Form::label('Amount From','Amount From') !!}
										{!! Form::text('start_from',null,['class'=>'form-control col-md-12 btn-block','placeholder'=>'Amount From','required'=>'required']) !!}
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group form-group-sm">
										{!! Form::label('Amount To','Amount To') !!}
										{!! Form::text('end_at',null,['class'=>'form-control col-md-12 btn-block','placeholder'=>'Amount To','required'=>'required']) !!}
									</div>
								</div>
							</div>
							<p></p>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group form-group-sm">
										{!! Form::label('Commission','Commission') !!}
										{!! Form::text('value',null,['class'=>'form-control col-md-12 btn-block','placeholder'=>'Commission','required'=>'required']) !!}
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group form-group-sm">
										{!! Form::label('% For Agent','% For Agent') !!}
										{!! Form::text('agent_quota',50,['class'=>'form-control col-md-12 btn-block','placeholder'=>'Agent Percent ','required'=>'required']) !!}
									</div>
								</div>
							</div>	
							<p>	</p>
							<div class="row">
							 	{!! Form::submit('Submit',['class'=>'btn btn-primary btn-lg col-md-12 btn-block']) !!}
							</div>

						{!! Form::close() !!}

				</div>
			</div>
		
    		<table  id="sort-table" class="table table-striped table-bordered tablesorter">
						<thead>
									<tr>
										<th>No</th>
										<th>Member</th>
										<th>From</th>
										<th>To</th>	
										<th>Currency</th>	
										<th>Commission</th>
										<th>Commission Ratio, B% Agent%</th>
										<th colspan = "2">Actions</th>
									</tr>
							</thead>
							<tbody>
									 <?php $x=0; ?>
					@foreach($commissions as $commission)
										<tr>
											<td><?php  echo $x=$x+1;?></td>
											<td>{{$commission->user->name}}</td>
											<td>{{$commission->start_from}}</td>
											<td>{{$commission->end_at}}</td>
											<td>{{$commission->currency->code}}</td>
											<td>{{$commission->value}}</td>
											<td>{{$commission->agent_quota}}%, {{100- $commission->agent_quota}}%</td>
											<td>
											<a href= {{route('commission.edit', $commission->id)}} class="btn btn-default btn-block">Edit</a>
												{!! Form::open(['method'=>'DELETE', 'action'=> ['CommissionController@destroy',$commission->id], 'files'=>true]) !!}
													
														{!!Form::submit('Delete',['class'=>'btn btn-danger']) !!}
												{!! Form::close() !!}
											</td>
										 </tr> 
										 
									@endforeach						
							</tbody>
									
							</table>
							{{$commissions->render()}}
				</div>
    </div>
    

@endsection

