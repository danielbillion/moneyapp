@extends('layouts.layout')
@section('content')
	<div class="row">
				<div class="col-md-6">
					<ol class="breadcrumb text-center">
						<li class="active">{{$header==$header?$header:""}} Members</li>
						<li class="active"><a href="{{route('members.customer')}}">Customer</a></li>
						<li class="active"><a href="{{route('members.agent')}}">Agent</a></li>
					</ol>
				</div>
					
				<div class="col-md-5 col-md-offset-1">
						<div class="ui-widget">
								<input type="text" id="getMember" class="form-control" placeholder=" Ben...." >
								</div>
					</div>
	</div>
	<div class="row">									
						<div  class="col-md-3">
							<!--<button name="send" id="send" class="btn btn-success " ><i class="fa fa-money" aria-hidden="true"></i> SMS / EMAIL</button>	-->	
							
						</div >
							<div  class="col-md-8">
							
							</div>
	</div>
	<br/>
	 <div class="col-md-12">
		<table class="text-center table table-striped table-bordered tablesorter">
			<thead>
					<tr>
								<th>No </th>
								<th>Type <i class=""></i></th>
								<th>Name <i class=""></i></th>
								<th>Senders <i class=""></i></th>
								<th>Transactions <i class=""></i></th>
								<th colspan="2" class="text-center">Actions</th>
									
					</tr>
				</thead>
			<tbody>
				<?php $avater="noavatar.jpg";$x=0; 
					foreach($members as $key=>$member): ?>
				<tr>
					<td>
							{{$key + 1}}					
					</td>

					<td>
						<strong>{{$member->type}}</strong>		
									
					</td>
										
					<td> {{str_limit(ucfirst($member->fname." ".$member->lname),10)}} </td>

<!----Senders---->											
					<td>
							<i class="fa fa-users" aria-hidden="true"></i>
							
							@if($member->type==='agent')
								<a href="{{route('customer.list',$member->id)}}">
								Senders [{{App\AgentCustomer::whereUser_id($member->id)->count()}}]
								</a>
							@else
								
							 </a>
							 @endif
							 
							 @if($member->type==='customer')
							 	<a href="{{route('receivers.customer',$member->id)}}">
								 Receivers [{{App\Receiver::whereUser_id($member->id)->count()}}]
								</a>
							@else
							<a>-</a>
							@endif
						 	</a>
						</td>
<!----Peniding Transaction---->											
							<td>
								<a href="{{route('transactions.user',$member->id)}}"
								class="btn btn-primary">Transactions [{{App\Transaction::whereUser_id($member->id)->count()}}]  
								</a>																									
										</td>
										
<!----Email----						<td>
												<i class="fa fa-envelope" aria-hidden="true"></i> Email										
										</td> -->
										
<!----Actions---->										
							<td>

								<a href="{{route('member.status',$member->id)}}" class="btn btn-default" 
									class ={{$member->is_active===1?'text-success':'text-danger'}}>
									Notes | {{$member->is_active===1?'Active':'Suspended'}} </a>
								
								<a href="{{route('members.edit',$member->id)}}" class="btn btn-default">
								<i class="fa fa-pencil" aria-hidden="true"> </i> Edit </a>
								
								<a href="{{route('message.user',$member->id)}}" class="btn btn-default">
								<i class="fa fa-pencil" aria-hidden="true"> </i> Message </a>
									@if(Auth::user()->isAdmin())
												{!! Form::open(['method'=>'DELETE', 'action'=>[ 'UsersController@destroy',
													$member->id], 'files'=>true,'class'=>'delete']) !!}
													{!! Form::submit('Delete',['class'=>'delete btn btn-danger']) !!}
											{!! Form::close() !!}
										
											@endif
									</td>
										<input name="table" id="table" value="<?php if(isset($type)){echo $type; } ?>"  type="hidden">
					</tr>
									
										<?php endforeach; ?>
					
										
			</table>
					<div class="row">
					<div class="col-md-5 col-md-offset-3 text-center">
						{{$members->render()}}
					</div>
				</div>
	</div>
@endsection





@section('script')
	$(function(){
				<!--confirm Delete --> 
				$(".delete").on("submit", function(){
					return confirm("Are you sure, you want to delete?");
						});

				 $( "#getMember" ).autocomplete({	
				 	max:10,
					//source:'quickSearch.php',
					
						 source: function (request, response) {
							 $.ajax({
								 url:  "{{route('members.search',"")}}"+"/" + request.term ,
								 data: { term: request.term },
								 dataType: "json",
								 success: response,
								 error: function () {
									 response([]);
								 }
							 });
						 },
					select: function( event, ui ) {
						var add="{{route('members.show',"")}}";		
					window.location.href = add+'/'+ui.item.id;
					}
    });
				
			});
@endsection