@extends('layouts.layout')
@section('content')
	 <div class="col-md-7 col-md-offset-2">
		<div class="panel panel-default">
			<div class=" panel-headingt">
				<h3>Transaction Status</h3>
			</div>
		<div class=" panel-body">
			{!! Form::model($transaction,['method'=>'PATCH','action'=>['TransactionsController@update',$transaction->id],'file'=>true]) !!}


				<div class="row">
					<div class="col-md-12">
<!--sender details--><div class="form-group">	
						{!! Form::label('Sender','Sender') !!}<span class="essential">*</span>
						{!! Form::text('sender_name',null,['id'=>'no', 'class'=>'form-control','placeholder'=>'Receipt Number','readonly'=>'readonly']) !!}
						</div>
					</div>
				</div>
				<div class="row">
						<div class="col-md-12">
	<!--sender details--><div class="form-group">	
							{!! Form::label('Receipt','Receipt') !!}<span class="essential">*</span>
							{!! Form::text('receipt_number',null,['id'=>'no', 'class'=>'form-control','placeholder'=>'No of Transactions','readonly'=>'readonly']) !!}
							</div>
						</div>
				</div>
				<div class="form-group">
							{!! Form::label('Status','Status') !!}<span class="essential">*</span>
							{!! Form::select('status',['paid' =>'paid','pending'=>'pending','suspended'=>'suspended'],null,['class'=>'form-control','placeholder'=>'Status','id'=>'Status']) !!}
							
				</div>

				<div class="row">
							{!! Form::submit('Submit',['class'=>'btn btn-primary col-md-12 btn-block']) !!}
				</div>
			{!! Form::close() !!}

		</div>
		</div>
	</div>
@endsection