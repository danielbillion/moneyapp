@extends('layouts.layout')
@section('content')
	 <div class="col-md-9 col-md-offset-1">
		<div class="panel panel-default">
			<div class=" panel-headingt">
				<h4>Member Message</h4>
			</div>
		<div class=" panel-body">
			{!! Form::open(['method'=>'POST','action'=>['MessageController@store'],'file'=>true]) !!}
			{!! Form::hidden('mobile',$member->mobile) !!}
			{!! Form::hidden('email',$member->email) !!}
			{!! Form::hidden('receiver',$member->name) !!}

				<div class="row">
					<div class="col-md-12">
<!--Member Name--><div class="form-group">	
						{!! Form::label('Member','Member') !!}
						{!! Form::text('name',
									$member?$member->name.
									" [".$member->email."]"
									." [".$member->mobile."]"
									:null,['id'=>'no', 'class'=>'form-control','placeholder'=>'Member Name','readonly'=>'readonly']) !!}
						</div>
					</div>
				</div>
				<div class="row">
						<div class="col-md-12">
	<!--is Active--><div class="form-group">	
							{!! Form::label('Message Type','Message Type') !!}
							{!! Form::select('type',['EMAIL' =>'EMAIL','SMS'=>'SMS'],
							null,['class'=>'form-control','id'=>'Status']) !!}
							</div>
						</div>
				</div>
				<div class="form-group">
							{!! Form::label('Subject','Subject') !!}<span class="essential">*</span>
							{!! Form::text('subject',
							null,['class'=>'form-control','placeholder'=>'Your subject']) !!}
							
				</div>
				<div class="form-group">
							{!! Form::label('Message','Message') !!}
							{!! Form::textarea('body',
							null,['class'=>'form-control','placeholder'=>'Your message body']) !!}
							
				</div>
				<hr>

				
				<div class="row">
							{!! Form::submit('Submit',['class'=>'btn btn-primary col-md-12 btn-block']) !!}
				</div>
			{!! Form::close() !!}

		</div>
		</div>
	</div>
@endsection