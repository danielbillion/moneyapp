@extends('layouts.app')

@section('content')

<div class="row container">
 <!-- Currency List -->
	<div class="col-md-7 well well-md">
		<div class="row">
			<span class="col-md-6 text-center">
					 @foreach(App\Currency::rates() as $key=>$rate)
						@if($key < 2)
                            <img src="{{asset('images/flags/')}}/{{strtolower($rate->destination)}}.png" width="50px" height="50px">
                            {{$rate->destination}} 
                             {{$rate->rate}} &nbsp;&nbsp; 
						@endif
					@endforeach
				</span>
				<span class="col-md-6">
				
					<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <!-- Wrapper for slides -->
					<div class="carousel-inner" role="listbox">
					@foreach(App\Currency::rates() as $key=>$rate)
							@if($key >= 2)
                              <div class="item {{$key==2?'active':''}}" >
                                    <img src="{{asset('images/flags/')}}/{{strtolower($rate->destination)}}.png" 
										width="50px" height="50px" style="display:inline">
                                    {{$rate->destination}}
									{{$rate->rate}} &nbsp;&nbsp; 
							</div>
							@endif
						@endforeach
						 <!-- Controls -->
  
						</div>
					</div>
            </span>
	</div>


        








		<p>
	<!-- Converter -->
		 <div class="row">
		 <h6 class="pull-left">{{ config('app.name', 'Laravel') }}</h6>
			<div class="col-md-9 col-md-offset-2 ">
			
				<input type = "hidden" value=
				 {{App\Rate::todays_rate()->rate}} id = "rate" >
				<div class="">
					
					<div class="panel-body">
						<div class="row">
							<div class="col-md-8 form-group">
								{!! Form::label('Amount',
									'Amount £') !!}
								{!!Form::text('amount-pounds',
									null,['class'=>'form-control','id'=>'amount-pounds']) !!}
							   </div> 
							   <div class="col-md-8 form-group">
							{!! Form::label('Amount','Amount &#8358;') !!}
						   {!!Form::text('amount-naira',null,['class'=>'form-control','id'=>'amount-naira']) !!}
							   </div> 
								<div class="col-md-8 form-group">
									{!! Form::label('Destination','Destination') !!}
									{!! Form::select('currency_id',App\Currency::list(),null,
												['class'=>'form-control','required'=>'required','id'=>'currency_id']) !!}
										</div>
						
							
							   <div class="form-group">
								{!! Form::label('Commission',
									'Commission £') !!}
								{!!Form::text('commission',
									null,['class'=>'form-control','id'=>'commission','readonly'=>'readonly']) !!}
							   </div> 
							   <div class="form-group">
							{!! Form::label('Total','Total') !!}
						   {!!Form::text('Total',null,['class'=>'form-control','id'=>'total','readonly'=>'readonly']) !!}
							    
							</div>

						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	
	
<!-- Login------------------------- -->
	
        <div class="col-md-4 col-md-offset-1 text-center"><br/>
            <div class="panel panel-default col-md-12">
                <div class="panel-heading" >
					<strong >Login</strong></div>

                <div class="panel-body ">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">

                        {{ csrf_field() }}
                         <input type="hidden" id="myurl">
                        
						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="">E-Mail Address</label>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif 
                        </div>
							<br><br>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="">Password</label>
								<input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary btn-block" onclick="spinner(this);">
                                   <strong> Login</strong>
                                </button>
                                    <br/>
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
</div>
@endsection
@section('script')  
   
 
  $(function(){
            rate = parseFloat($('#rate').val())

         $('#amount-pounds').on('keyup',function(){
                amount = parseFloat($('#amount-pounds').val())
                 $('#amount-naira').val(amount*rate)
                makeRequest(amount) 
                      
         })

         $('#amount-naira').on('keyup',function(){ 
                amount =parseFloat($('#amount-naira').val())
                pounds = (amount/rate).toFixed(2)
                $('#amount-pounds').val(pounds)  

                makeRequest(pounds)
        })

        
        
        function makeRequest(amount){
      currencyId = $('#currency_id').val()
        uri= 'api/user/' + 0 + '/currency/' + '1' + '/amount/' + amount + '/commission'
 
            $.ajax({
                    method:'get',
                    url:uri,
                    success:function(response){
                        console.log(response)
                        commission=response.value
                        
                        $('#commission').val(commission)
                        total = (+commission + +amount).toFixed(2)
                        $('#total').val(total)     
                    }

                })
         }
     
    })
@stop