@extends('layouts.layout')
@section('content')

@if(Auth::user()->type === 'admin')
<!--------------------------------------end part payment-------------------------------------------- -->

{!! Form::open(['method'=>'POST', 'action'=>'OutstandingController@store','file'=>true]) !!}
				{!! Form::hidden('agent_id',$agent->id) !!}
				{!! Form::hidden('total',$owing) !!}
<div class="col-md-7 col-md-offset-2 text-center well ">
					<div class="col-md-12">
									<div class="form-group input-group">
										<span class="input-group-addon">Total Outstanding</span>
										{!! Form::text('amount',$owing,['class'=>'form-control',
											'id'=>'getTransactions','placeholder'=>'Enter Amount']) !!}
										
									</div>
									{!! Form::submit('Make Payment',['class'=>'confirm btn btn-success  col-md-12 btn-block']) !!}
					</div>
				
					
	</div>
	
				{!! Form::close() !!}


<!----------------------------------------------------------------end part payment-------------------------- !-->
@endif
	
	 <div class="col-md-12 text-center">
		<h4> {{$agent->name?$agent->name:'All'}} Outstanding History</h4>	
				
			
					<table class="table table-striped table-bordered tablesorter">
			<thead>
					<tr>
				
						<th>No </th>
						<th>Date</th>
						<th class = "text-center">Receipt</th>
						<th>Sender</th>
						<th>Receiver</th>
						<th class = "text-center">Com_A/Com_B </th>	
						<th class = "text-center">Amount </th>
						<th class = "text-center">Total </th>
						<th class = "text-center">Actions </th>			
					</tr>
				</thead>
				
				
				<tbody>
						<?php $avater="noavatar.jpg";$x=0; ?>
							@foreach($outstandings as $key=> $outstanding)
					<tr>
						
						<td>
							{{$key +1}}					
						</td>
						<td>

						{{$outstanding->created_at->format('Y-m-d')}}	
						</td>
						
						<td>
						{{$outstanding->transaction->receipt_number}}	
						</td>

						<td>
						{{$outstanding->sender_name}}	
						</td>
										
						<td> {{$outstanding->receiver_name}} </td>
						
						<td>
								{{$outstanding->transaction->agent_commission}} /
								{{$outstanding->transaction->commission -
												 $outstanding->transaction->agent_commission}}
						</td>
						<td>
								{{number_format($outstanding->amount,2)}}
						</td>

						<td>
								{{$outstanding->transaction->amount + 
											$outstanding->transaction->commission}}
						</td>
						
						<td>
						
						@if($outstanding->transaction_paid === 1)
							<strong  class="text-success" href="#">Paid</strong>
						@else
							@if(Auth::user()->type === 'admin')
								<a  class="btn btn-danger" href={{route('outstandings.modify',[$outstanding->id,'transaction'])}}>UnPaid</a>
							@else
							<a  class="btn btn-danger" href="{{App\Setting_transaction::setting()->allow_payment=== NULL?'#':
								route('payments.pay',['outstanding',$outstanding->id])}}">UnPaid</a>
							@endif
						@endif
						</td>
					</tr>
									
					@endforeach
					<tr>
						<td colspan = '5'>Total </td>
						<td> {{number_format($outstandings->sum('agent_commission'),2)}}  / Agent</td>
						<td> {{number_format($outstandings->sum('amount'),2)}} </td>
					</tr>
					
										
			</table>

					
				{{$outstandings->render()}}		
			
		</div>
	
@endsection

@section('script')
	
		$(".confirm").on("submit", function(){
					 confirm("Are you sure?");
						});

@stop





