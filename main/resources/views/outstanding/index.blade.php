@extends('layouts.layout')
@section('content')
	
	 <div class="col-md-12">
		
			
					<h4>Agents Payments</h4>	
				
			
					<table class="table table-striped table-bordered tablesorter">
			<thead>
					<tr>
						<th>No </th>
						<th>Name</th>	
						<th colspan="2" class = "text-center">Transactions <small class="help-block">payments by agent</small> </th>
						<th colspan="2" class = "text-center">Commission <small class="help-block">payments by admin to agent</small></th>			
					</tr>
				</thead>
				
				
				<tbody>
						<?php $avater="noavatar.jpg";$x=0; 
							foreach(App\User::whereType('agent')->get() as $agent): ?>
					<tr>
						<td>
							
							<?php echo $x=$x+1; ?>					
						</td>
										
						<td> {{str_limit(ucfirst($agent->fname." ".$agent->lname),10)}} </td>
<!----Transactions---->										
						<td>
							<span>
								<a href="">
									<small class="text-danger">Pending
									  £{{$agent->pending_outstanding->sum('amount')}}
									/ [ {{$agent->pending_outstanding->count()}} ] ,
									</small>
								 </a>
								 <br/> <br/>
								 <a href="{{route('partpayment.transactions',$agent->id)}}">
								 	<small class="text-primary">Part-Paid
								 	- £{{$agent->transaction_part_payments->sum('amount')}}</a>
									</small>
								<a href="">
							</span>
						</td>
						<td class="text-center">
							<a  class="btn btn-primary" href="{{route('outstandings.transactions',[$agent->id,0])}}">Pending Payments </a>
							<a  class="btn btn-default"  href="{{route('outstandings.transactions',[$agent->id,1])}}"> Paid Payment </a>
							<a  class="btn btn-default" href="{{route('partpayment.transactions',$agent->id)}}">Part Paid </a>
						</td>
<!----Commission---->											
						<td>
						<span>
								<a href="">
									<small class="text-danger">Pending
									  £{{$agent->pending_commission->sum('agent_commission')}}
									/ [ {{$agent->pending_commission->count()}} ] ,
									</small>
								 </a>
								 <br/> <br/>
								 <a href="{{route('partpayment.commissions',$agent->id)}}">
								 	<small class="text-primary">Part-Paid
								 	- £{{$agent->commission_part_payments->sum('amount')}}</a>
									</small>
								<a href="">
							</span>
						<td class="text-center">
							<a  class="btn btn-success"  href="{{route('outstandings.commissions',[$agent->id,0])}}">Pending Commissions</a>
							<a  class="btn btn-default" href="{{route('outstandings.commissions',[$agent->id,1])}}"> Paid Commissions </a>
							<a  class="btn btn-default" href="{{route('partpayment.commissions',$agent->id)}}">Part Paid </a>
						</td>
				</tr>
									
										<?php endforeach; ?>
					
										
			</table>
					
						
			
		</div>
	
@endsection





