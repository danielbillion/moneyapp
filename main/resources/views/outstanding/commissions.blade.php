@extends('layouts.layout')
@section('content')

@if(Auth::user()->type === 'admin')
<!------------------------------------- part payment-------------------------------------------- -->

{!! Form::open(['method'=>'POST', 'action'=>'OutstandingController@store_commissions','file'=>true]) !!}
				{!! Form::hidden('agent_id',$agent->id) !!}
				{!! Form::hidden('total',$owing) !!}
<div class="col-md-7 col-md-offset-2 text-center well ">
					<div class="col-md-12">
									<div class="form-group input-group">
										<span class="input-group-addon">Total Outstanding Commission</span>
										{!! Form::text('amount',$owing,['class'=>'form-control',
											'id'=>'getTransactions','placeholder'=>'Enter Amount']) !!}
										
									</div>
									{!! Form::submit('Make Payment',['class'=>'confirm btn btn-warning  col-md-12 btn-block']) !!}
					</div>
				
					
	</div>
	
				{!! Form::close() !!}


<!----------------------------------------------------------------end part payment-------------------------- !-->
@endif
	
	 <div class="col-md-12 text-center">
		<h4> {{$agent->name?$agent->name:'All'}} Outstanding Commission History</h4>	
				
			
					<table class="table table-striped table-bordered tablesorter">
			<thead>
					<tr>
						<td></td>					
						<th>Date</th>
						<th class = "text-center">Receipt</th>
						<th>Sender</th>
						<th>Receiver</th>
						<th class = "text-center">Agent_Comm </th>	
						<th class = "text-center">Amount </th>
						<th class = "text-center">Total </th>
						<th class = "text-center">Action Require </th>		
					</tr>
				</thead>
				
				
				<tbody>
						<?php $avater="noavatar.jpg";$x=0; ?>
							@foreach($commissions as $key=> $commission)
					<tr>
						<td>
							{{$key +1}}					
						</td>
										
						<td> {{$commission->created_at->format('Y-m-d')}} </td>
						
						<td>
								{{$commission->transaction->receipt_number}}
						</td>

						<td>
								{{$commission->sender_name}}
						</td>
						<td>
								{{$commission->receiver_name}}
						</td>

						<td>
								{{$commission->transaction->agent_commission}}
								
						</td>

						<td>
								{{number_format($commission->amount,2)}}
						</td>
						<td>
								{{$commission->transaction->amount + 
											$commission->transaction->commission}}
						</td>
						
						<td>
						@if($commission->commission_paid === 0)
							<a  class="btn btn-danger" href={{route('outstandings.modify',[$commission->id,'commission'])}}>UnPaid</a>
						@else
							<strong  class="text-success" href="#">Paid</strong>
						@endif
						</td>
					</tr>
									
					@endforeach
					
					<tr>
						<td colspan = '5'>Total </td>
						<td> {{number_format($commissions->sum('agent_commission'),2)}}  / Agent</td>
						<td> {{number_format($commissions->sum('amount'),2)}} </td>
					</tr>					
			</table>
					
				{{$commissions->render()}}		
			
		</div>
	
@endsection

@section('script')
	
		$(".confirm").on("submit", function(){
					 confirm("Are you sure?");
						});

@stop





