@extends('layouts.layout')
@section('content')
	 <div class="col-md-7 col-md-offset-2">
		<div class="panel panel-default">
			<div class=" panel-headingt">
				<h4>Outstanding {{$type}} Update</h4>
			</div>
		<div class=" panel-body">
			{!! Form::model($outstanding,['method'=>'PATCH','action'=>[$controller,$outstanding->id],'file'=>true]) !!}


				<div class="row">
					<div class="col-md-12">
<!--sender details--><div class="form-group">	
						{!! Form::label('Sender','Sender') !!}
						{!! Form::text('sender_name',$outstanding->transaction->sender_name,['id'=>'no', 'class'=>'form-control','placeholder'=>'Receipt Number','readonly'=>'readonly']) !!}
						</div>
					</div>
				</div>
				<div class="row">
						<div class="col-md-12">
	<!--sender details--><div class="form-group">	
							{!! Form::label('Receipt','Receipt') !!}
							{!! Form::text('receipt_number',$outstanding->transaction->receipt_number,['id'=>'no', 'class'=>'form-control','placeholder'=>'No of Transactions','readonly'=>'readonly']) !!}
							</div>
						</div>
				</div>
				<div class="row">
						<div class="col-md-12">
	<!--sender details--><div class="form-group">	
							{!! Form::label('Amount','Amount') !!}
							{!! Form::text('amount',null,['id'=>'no', 'class'=>'form-control','readonly'=>'readonly']) !!}
							</div>
						</div>
				</div>
				<div class="form-group">
							{!! Form::label('Status','Status') !!}<span class="essential">*</span>
							{!! Form::select('status',['1' =>'Paid','0'=>'UnPaid'],null,['class'=>'form-control','placeholder'=>$outstanding->status,'id'=>'Status']) !!}
							
				</div>

				<hr>

				<div class="row">
						<div class="col-md-12">
	<p></p>
	<!--Authenticate--><div class="form-group text-center">	
							{!! Form::label('Password','Password') !!}<span class="essential">*</span>
							{!! Form::password('password',['id'=>'no', 'class'=>'form-control']) !!}
							</div>
						</div>
				</div>
				<div class="row">
							{!! Form::submit('Submit',['class'=>'btn btn-primary col-md-12 btn-block']) !!}
				</div>
			{!! Form::close() !!}

		</div>
		</div>
	</div>
@endsection