@extends('layouts.layout')
@section('content')
    	
			
    <div class="row">
    	<div class="col-sm-9 col-sm-offset-1">
    		<div class="panel panel-primary">
				<div class="panel-heading">
						<h3>Set Rate</h3>
				</div>
				<div class="panel-body">
						{!! Form::open(['method'=>'POST', 'action'=> 'N2UK\RatesController@store', 'files'=>true]) !!}
						{!! Form::hidden('user_id',Auth::id()) !!}
							<div class="row">
								<div class="col-md-8">
									<div class="form-group form-group-sm">
										{!! Form::label('Bou Rate','Bou Rate,  ') !!}
										{!! Form::text('bou',null,['class'=>'form-control col-md-12 btn-block','placeholder'=>'Bou  Rate','required'=>'required']) !!}
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group form-group-sm">
										{!! Form::label('Sold Rate','Sold Rate') !!}
										{!! Form::text('sold',null,['class'=>'form-control col-md-12 btn-block','placeholder'=>'Sold Rate','required'=>'required']) !!}
									</div>
								</div>
							</div>
							<p></p>
								
							<p>	</p>
							<div class="row">
							 	{!! Form::submit('Submit',['class'=>'btn btn-primary btn-lg col-md-12 btn-block']) !!}
							</div>

						{!! Form::close() !!}

				</div>
			</div>
			<h3>Rate History</h3>
    		<table  id="sort-table" class="table table-striped table-bordered tablesorter">
						<thead>
									<tr>
										<th>No</th>
										<th>Date</th>
										<th>Bou Rate</th>	
										<th>Sold Rate</th>
										
									</tr>
							</thead>
							<tbody>
										 <?php $x=0; ?>
									@foreach($rates as $rate)
										<tr>
											<td><?php  echo $x=$x+1;?></td>
											<td>{{ $rate->created_at->diffForHumans() }}</td>
											<td>{{$rate->bou}}</td>
											<td>{{$rate->sold}}</td>
										 </tr> 
										 
									@endforeach						
							</tbody>
								
							</table>
							<div class="row">
								<div class="col-md-5 col-md-offset-3 text-center">
									{{$rates->render()}}
								</div>
							</div>
				</div>
			</div>
    </div>
    

@endsection

