@extends('layouts.layout')
@section('content')

			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 style="display:inline-block;text-transform:uppercase;"></h4>
				</div>
			<div class="panel-body">
				<div class="panel-resize">
			  <div class="row">
				<div class="col-md-7 col-md-offset-2">
						<ol class="breadcrumb">
						<li><a href="index.php">Home</a></li>
						<li><a href="sendersReceivers.php"> Sender</a></li>
						<li class="active">Create New Receiver</li>
						
						</ol>
					</div>
				<div class="col-md-3">
					<h3>N2UK</h3>
				</div>
					
				</div>
				
						
				{!! Form::open(['method'=>'POST', 'action'=> 'N2UK\ReceiversController@store', 'files'=>true]) !!}
				{!! Form::hidden('user_id', Auth::id(), ['class'=>'form-control'])!!}
				{!! Form::hidden('sender_id', $customer->id, ['class'=>'form-control'])!!}
				<div class="row">
							<div class="col-md-5 ">
								<div class="form-group form-group-sm">
									<span class="text-danger">*</span>
									{!! Form::label('fname', 'First Name')!!}
									{!! Form::text('fname', null, ['class'=>'form-control'])!!}
								</div>
							</div>
							
							<div class="col-md-5 ">
								<div class="form-group form-group-sm">
									<span class="text-danger">*</span>
									{!! Form::label('lname', 'Last Name')!!}
									{!! Form::text('lname', null, ['class'=>'form-control'])!!}
								</div>
							</div>
				</div>		
				<div class="row">	
							<div class="col-md-5">
								<div class="form-group form-group-sm">
									{!! Form::label('transfer_type', 'Transfer Type:') !!}
							         {!! Form::select('transfer_type', ['Pick Up'=>'Pick Up','Pick'=>'Bank Account'] , null, ['class'=>'form-control'])!!}
								</div>
							</div>

							<div class="col-md-5">
								<div class="form-group form-group-sm">
									{!! Form::label('phone', 'Phone')!!}
									{!! Form::text('phone', null, ['class'=>'form-control'])!!}
								</div>
							</div>
				</div>

				<div class="row" id="modeId" >	
				<div class="col-md-5 col-md-offset-3">
						<div class="form-group form-group-sm">
							{!! Form::label('Identity', 'Identity') !!}
							 {!! Form::select('identity_type', ['National ID'=>'National ID','Intl Passport'=>'Intl Passport','Drivers Lincence'=>'Drivers Lincence','Any type of ID' => 'Any type of ID'] , null, ['class'=>'form-control'])!!}	
								</div>
							</div>
			</div>
						
<!-- Uk BANK -->							
			<div class="row">
					<div class="col-md-4">
						<div class="form-group form-group-sm">
								{!! Form::label('UK Bank', 'UK Bank') !!}
							    {!! Form::select('uk_bank', [$bank_uk] , null, ['class'=>'form-control'])!!}
									
								</div>
							</div>

					
						<div class="col-md-4" >
							<div class="form-group form-group-sm">
									{!! Form::label('UK Sort Code', 'UK Sort Code')!!}
									{!! Form::text('uk_sort_code', null, ['class'=>'form-control'])!!}
								</div>
							</div>
							<div class="col-md-4" >
							<div class="form-group form-group-sm">
									{!! Form::label('uk_account_no', 'UK Account No')!!}
									{!! Form::text('uk_account_no', null, ['class'=>'form-control'])!!}
								</div>
							</div>
			</div>
<!-- NG BANK -->
			<div class="row">
					<div class="col-md-4">
						<div class="form-group form-group-sm">
								{!! Form::label('NG Bank', 'NG Bank') !!}
							    {!! Form::select('ng_bank', [$bank_ng] , null, ['class'=>'form-control'])!!}
									
								</div>
							</div>

					
						<div class="col-md-4" >
							<div class="form-group form-group-sm">
									{!! Form::label('NG Account No', 'NG Account No')!!}
									{!! Form::text('ng_account_no', null, ['class'=>'form-control'])!!}
								</div>
							</div>
							<div class="col-md-4" >
							<div class="form-group form-group-sm">
									{!! Form::label('NG Account Name', 'NG Account Name')!!}
									{!! Form::text('ng_account_name', null, ['class'=>'form-control'])!!}
								</div>
							</div>
			</div>
			
						
			<div class="row">
				<div class="col-md-12">
					<div class="form-group form-group-sm">
						<br/><br/>
						{!! Form::submit('Submit', ['class'=>'btn btn-primary btn-block'])!!}
								</div>
							</div>
			</div>
				{!! Form::close() !!}

				</div>
			</div>
		</div>

@endsection