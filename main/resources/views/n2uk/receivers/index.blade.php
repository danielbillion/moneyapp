@extends('layouts.layout')
@section('content')
<div class="row">										
				<div class="col-md-6">		
							<div class="row">	
								<div class="col-md-4">
									 <ol class="breadcrumb">
									  <li><a href="#">Home</a></li>
									  <li class="active"><a href="sendersReceivers.php">Receivers</a></li>
									</ol>  
								</div>
							</div>
						</div>
<!---search customer-->	
						
					
					
					<div class="col-md-6">
						<form name="form1" action="" method="post">
							<div class="form-group">
								<div class="ui-widget">
									<input type="text" class="form-control" id="getreceivers" placeholder="Search...">
											</div>
										</div>
									</form>		
					</div>
					
			</div>
						
					   <table id="sort-table" class="table table-striped tablesorter">
						  <thead>
						  <tr>
						<th>No </th>
						<th>Name <i class="caret"></i></th>
						
						<th>Mobile <i class="caret"></i></th>
						<th>Modify</th>	
						<th>Delete</th>					
						<th>Send</th>
						</tr>
						<thead>
						<tbody>
						
						 <?php $x=0; foreach($receivers as $receiver): ?>
						<tr>
							<td><?php  echo $x=$x+1;?></td>
							<td><a href="{{route('receivers_n2uk.edit',$receiver->id)}}">
								<?php  echo ucfirst($receiver->title)." ".ucfirst($receiver->fname)." ". ucfirst($receiver->lname);?></a></a></td>
							
							<td><?php  echo $receiver->phone;?></td>
								
						 	
						 	<td><a href="{{route('receivers_n2uk.edit',$receiver->id)}}" class="btn btn-primary"><i class="fa fa-users fa-lg"> Modify</a></td>	
						<td>
						 	{!! Form::open(['method'=>'DELETE', 'action'=>[ 'N2UK\ReceiversController@destroy', $receiver->id ], 'files'=>true]) !!}	
						 	{!! Form::submit('Delete', ['class'=>'btn btn-danger btn-block'])!!}
						 	{!! Form::close() !!}
						 </td>
							
						<td>
							<a href="
									{{route('transactions_n2uk.transfer',
									[$receiver->customer->id,$receiver->id]) }}"
							 class="btn btn-success btn-lg"><i class="fa fa-money fa-lg">  Send Money</a></td>
						 </tr>
						  <?php endforeach; ?>
						  
						  </tbody>
						</table>
@endsection

@section('script')
	$(function(){
				 $( "#getreceivers").autocomplete({	
				 	max:10,
					//source:'quickSearch.php',
					
						 source: function (request, response) {
							 $.ajax({
								 url:  "{{route('receivers_n2uk.autocomplete',$receiver->sender_id)}}" ,
								 data: "",
								 dataType: "json",
								 success: response,
								 error: function () {
									 response([]);
								 }
							 });
						 },
					select: function( event, ui ) {
						var add="{{route('receivers_n2uk.show',"")}}";		
					window.location.href = add+'/'+ui.item.id;
					}
    });
				
			});

@endsection