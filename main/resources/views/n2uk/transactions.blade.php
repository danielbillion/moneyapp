@extends('layouts.layout')
@section('content')
	<div class="row">
						<div class="col-md-6 col-md-offset-4">
							<div class="ui-widget">
									<input type="text" id="getTransactions" class="form-control" placeholder=" Search WE£446GF,0796643,John " >
									</div>
						</div>
						<div class="col-md-2">
									<a href="#" id ="clicksearch">Search by Date</a>
									<a href="{{route('transactions.search',
									['type' =>'today','value'=>'today'])}}">Today</a>&nbsp;
									<a href="{{route('transactions.search',
									['type' =>'yesterday','value'=>'yesterday'])}}">Yesterday</a>
									<a href="{{route('transactions.search',
									['type' =>'month','value'=>'month'])}}">Month ago</a>
						</div>

	</div>
	<div class= "row"  id = "datesearch">
				<div class="col-md-5 col-md-offset-2">
								{!! Form::label('From', 'From Date')!!}
					<div class="form-group input-group form-group-sm">
					 <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						{!! Form::date('from', '09/04/2015', ['class'=>'form-control','id'=>'datefrom'])!!}
					</div>
				</div>

				<div class="col-md-5">
								{!! Form::label('To', 'To Date')!!}
					<div class="form-group input-group form-group-sm">
					 <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						{!! Form::date('to', '09/04/2015', ['class'=>'form-control','id'=>'dateto'])!!}
					</div>
				</div>
	</div>
	<br/>
	 <div class="row">
					<div class="col-md-3">
						<ol class="breadcrumb">
						<li class="active">{{$header==$header?$header:""}} Transactions</li>
						</ol>
					</div>
	@if(Auth::user()->type == 'admin')			
					<div class="col-md-4 col-md-offset-2">
						<ol class="breadcrumb">
							<li class="active"><a href="{{route('transactions.customer')}}">Customer</a></li>
							<li class="active"><a href="{{route('transactions.agent')}}">Agent</a></li>
						</ol>
					</div>			
	</div>
	@endif
	<div class="row">
		<div class="col-md-12">
			 <table id="sort-table" class="table table-striped table-bordered tablesorter">
				<thead>
							<tr>
								<th>No </th>
								<th>Date <i class=""></i></th>
								<th>Tcode  </th>
								<th>Sender <i class=""></i></th>
								<th>Receiver </th>
								<th>Amt</th>
								<th>Local Pay</th>
								<th>C_B</th>
								<th>C-ag</th>
								<th>Total</th>
								<th>Status</th>
								<th>--</th>
								<th>--</th>
								<th>--</th>
							</tr>
					</thead>
					<tbody>
						<?php $x=0;$tamt_send=0;$totalAmount=0;$totalLocalAmount=0;$totalAgentCommission=0;$totalBusinessCommission=0; $tTotal=0; ?>
			@foreach($transactions as $transaction)
							<tr>
							<td>{{$x=$x+1}}</td>
							<td>{{ $transaction->created_at->diffForhumans()}}</td>
							<td>{{ucfirst($transaction->receipt_number)}}</td>
							<td>{{ $transaction->sender_name}}</td>
							<td>{{ $transaction->receiver_name}}</td>
							<td>{{ number_format($transaction->amount, 2)}}</td>
							<td>{{$transaction->local_payment}}</td>
							<td>{{$transaction->business_commission}}</td>
							<td>{{$transaction->agent_commission}}</td>
							<td>{{$transaction->total}}</td>
							<td> <a href="{{route('transactions.status',
								$transaction->id)}}"class="btn btn-success">{{$transaction->status}}</a></td>
<!---Sum Here-->
							<?php
							$totalAmount += $transaction->amount;
							$totalLocalAmount += ($transaction->amount * $transaction->exchange_rate);
							$totalAgentCommission += ($transaction->commission * ($transaction->agent_commission/100));
							$totalBusinessCommission += ($transaction->commission * (100-$transaction->agent_commssion)/100);
							$tTotal +=($transaction->amount + $transaction->commission);		
							?>
									
									
							<td> <a href="{{route('transactions.edit',$transaction->id)}}" class="btn btn-warning">Edit</a></td>

							{!! Form::open(['method'=>'DELETE', 'action'=>[ 'AgentTransactionsController@destroy',$transaction->id], 'files'=>true]) !!}
								<td>{!! Form::submit('Delete',['class'=>'btn btn-danger']) !!}</td>
							{!! Form::close() !!}
								
							<td><a href="{{route('transactions.receipt',$transaction->id)}}" class="btn btn-info"><i class="fa fa-print" aria-hidden="true"></i>Receipt</a></td>		
									
								 </tr>
							 @endforeach

							 <tr>
								<td colspan="5"><strong>Total</td>
								<td><strong>&pound;{{number_format(($transactions->sum('amount')),2)}}</strong></td>
								<td><strong>&#8358;{{number_format($totalLocalAmount,2)}}</strong></td>
								<td><strong>&pound;{{ number_format($totalAgentCommission,2)}}</strong></td>
								<td><strong>&pound;{{ number_format($totalBusinessCommission,2)}}</strong></td>
								<td><strong>&pound;{{ number_format($tTotal,2) }}</strong></td>
								</strong>
							</tr>
						</tbody>
			</table>
				<div class="row">
					<div class="col-md-5 col-md-offset-3 text-center">
						{{$transactions->render()}}
					</div>
				</div>
		</div>
	</div>
@endsection

@section('script')
	$(function(){
<!--Trigger Autocomplete --> 
				 $('#clicksearch').on('click', function(e) {
				      $('#datesearch').toggle();
				      e.preventDefault();
				    });
				 url  = $('#myurl').val() + '/transaction/search'  
<!--Autocomplete --> 
				 $( "#getTransactions" ).autocomplete({	
				 	max:10,
					//source:'quickSearch.php',
					
						 source: function (request, response) {
							 $.ajax({
								 url:  "{{route('transactions.autocompletetips',"")}}"+"/" + request.term ,
								 data: { term: request.term },
								 dataType: "json",
								 success: response,
								 error: function () {
									 response([]);
								 }
							 });
						 },
					select: function( event, ui ) {
						var add=url;		
					window.location.href = add+'/id/'+ui.item.id;
					}
    });
   <!--Date Search --> 
   					$('#dateto').on('change', function (e) {
   					value = $('#datefrom').val() + ',' + $('#dateto').val()
   					window.location.href = url +'/date/'+ value
   				});
				
			});
@endsection