@extends('layouts.layout')
@section('content')

	<div class="container row">
		<div class="well col-md-8 col-md-offset-1">
<!--Edit Transactions-->	
			<div class="row">
				<div class="col-md-8 col-md-offset-1">
						<strong><h4 class="text-danger">Transaction Edit N2UK</h4></strong><p/>
				</div>
			</div>
			<hr>
				

			{!! Form::model($transaction,['method'=>'PATCH', 'action'=> ['N2UK\TransactionsController@update',$transaction->id], 'files'=>true]) !!}
		  
		
			
			<div class="row">
<!--Sender-->	
				<div class="col-md-6">
					<div class="form-group form-group-sm">
						{!! Form::label('Sender Name', 'Sender Name') !!}
						{!! Form::text('sender_name', $transaction->customer->name, ['class'=>'form-control','readonly'=>'readonly'])!!}
					</div>

					<div class="form-group form-group-sm">
						{!! Form::label('Address', 'Address') !!}
						{!! Form::text('address', $transaction->customer->address, ['class'=>'form-control','readonly'=>'readonly'])!!}
					</div>

					<div class="form-group col-xs-6">
						{!! Form::label('Postcode', 'Postcode') !!}
						{!! Form::text('postcode', $transaction->customer->postcode, ['class'=>'form-control','readonly'=>'readonly'])!!}
					</div>
					<div class="form-group col-xs-6">
						{!! Form::label('Country', 'Country') !!}
						{!! Form::text('country', $transaction->customer->country, ['class'=>'form-control','readonly'=>'readonly'])!!}
					</div>
					<div class="form-group form-group-sm">
						{!! Form::label('Phone', '') !!}
						{!! Form::text('phone', $transaction->customer->phone, ['class'=>'form-control','readonly'=>'readonly'])!!}
					</div>
				</div>
<!--Receiver-->	
				<div class="col-md-6">
					<div class="form-group form-group-sm">
						{!! Form::label('Receiver First Name', 'Receiver Name') !!}
						{!! Form::select('receiver_id', $transaction->senderReceivers(),null, ['class'=>'form-control'])!!}
					</div>
					
					
					<div class="form-group form-group-sm">
						{!! Form::label('Destination', 'Destination') !!}
						{!! Form::text('destination', 'Nigeria', ['class'=>'form-control','readonly'=>'readonly'])!!}
					</div>
					</div>
				</div>
<!--Transactions-->	
				<div class="row">
				<div class="col-md-6 col-md-offset-1"><p/>
					<h2 class="text-danger">Transaction N2UK</h2><p/><hr>
				</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group form-group-sm">
							{!! Form::label('Amount Pounds', 'Amount Pounds') !!}
							{!! Form::text('amt_pounds', $transaction->amount_pounds, ['class'=>'form-control','readonly'=>'readonly'])!!}
						</div>
							
						<div class="form-group form-group-sm">
							{!! Form::label('Amount Naira', 'Amount Naira') !!}
							{!! Form::text('amt_naira', $transaction->amount_pounds, ['class'=>'form-control','readonly'=>'readonly'])!!}
						</div>
						<div class="form-group form-group-sm">
							{!! Form::label('Bou Rate', 'Bou Rate') !!}
							{!! Form::text('bou_rate', $transaction->bou_rate, ['class'=>'form-control','readonly'=>'readonly'])!!}
						</div>
						<div class="form-group form-group-sm">
							{!! Form::label('Sold Rate', 'Sold Rate') !!}
							{!! Form::text('sold_rate', $transaction->sold_rate, ['class'=>'form-control','readonly'=>'readonly'])!!}
						</div>
						<div class="form-group form-group-sm">
							{!! Form::label('Margin', 'Margin') !!}
							{!! Form::text('margin', $transaction->margin, ['class'=>'form-control','readonly'=>'readonly'])!!}
						</div>
							
						<div class="form-group form-group-sm">
							{!! Form::label('Transfer Code', 'Transfer Code') !!}
							{!! Form::text('transfer_code', $transaction->transfer_code, ['class'=>'form-control','readonly'=>'readonly'])!!}
						</div>

						<div class="form-group form-group-sm">
							{!! Form::label('Transfer Mode', 'Transfer Mode') !!}
							{!! Form::select('transfer_type',['Pick up'=>'Pick up','Bank Account'=>'Bank Account'] ,$transaction->transfer_type, ['class'=>'form-control'])!!}
						</div>
							
							
					</div>
					
					<div class="col-md-6">
						<div class="form-group form-group-sm">
							{!! Form::label('Mode of Identity', 'Mode Of Identity') !!}
							{!! Form::text('identity', $transaction->receiver->identity, ['class'=>'form-control','readonly'=>'readonly'])!!}
						</div>
							
						<div class="form-group form-group-sm">
							{!! Form::label('Bank UK', 'Bank UK') !!}
							{!! Form::select('uk_bank',$bank_uk,$transaction->receiver->uk_bank, ['class'=>'form-control'])!!}
						</div>
						
						<div class="form-group form-group-sm">
							{!! Form::label('Account No UK', 'Account No UK') !!}
							{!! Form::text('uk_account_no',
										$transaction->receiver->uk_account_no, ['class'=>'form-control'])!!}
						</div>
						<div class="form-group form-group-sm">
							{!! Form::label('Sort Code UK', 'Sort Code UK') !!}
							{!! Form::text('uk_sort_code',
										$transaction->receiver->uk_sort_code, ['class'=>'form-control'])!!}
						</div>

						<div class="form-group form-group-sm">
							{!! Form::label('Bank Nigeria', 'Bank Nigeria') !!}
							{!! Form::select('ng_bank',
											$bank_ng,$transaction->receiver->ng_bank,
											 ['class'=>'form-control'])!!}
						</div>
						
						<div class="form-group form-group-sm">
							{!! Form::label('Account No Nigeria', 'Account No Nigeria') !!}
							{!! Form::text('ng_account_no',
											 $transaction->receiver->ng_account_no, 
											 ['class'=>'form-control'])!!}
						</div>

						<div class="form-group form-group-sm">
							{!! Form::label('Account Name', 'Account Name') !!}
							{!! Form::text('ng_account_name',
										 $transaction->receiver->ng_account_name,
										  ['class'=>'form-control'])!!}
						</div>

							
				</div>

			</div>
			<div class="form-group form-group-sm">
							<input type="submit" class="btn btn-primary btn-block" name="submit" value="Submit">
			</div>

					{!! Form::close() !!}
		</div>
	</div>

@stop