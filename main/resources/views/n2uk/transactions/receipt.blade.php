<link href="https://fonts.googleapis.com/css?family=Droid+Serif|Montserrat|PT+Serif|Roboto" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
	<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
	<link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">
	<link href="{{asset('css/style.css')}}" rel="stylesheet">
	<link href="{{asset('css/receipt.css')}}" rel="stylesheet">
	<link href="{{asset('css/googleaddresslookup.css')}}" rel="stylesheet">
	<link href="{{asset('css/font-awesome.css')}}" rel="stylesheet">
	<link href="{{asset('css/datepicker.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	 <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
<style>

#container{
	text-align:center;
	margin:3px 8%;
}

*{
	
font:normal 11px/1.2em sans-serif;	
}
.newrow{
	width:100%;
	overflow:hidden;
}
.companylogo{
	text-align:center;
	display:block;
	margin:8px 25%;
}
.headerlogo{
	display:inline;
	
	float:left;	
}

.companyname{
	display:inline;
	float:left;
}

@media print {
  /* style sheet for print goes here */
  .noprint {
    visibility: hidden;
  }
</style>
<div class="col-md-9">
	<div id="centerit">
			<div id="container" class="container">
						<div id="header" class="header">
							<div class="row">
								<div class="text-center companylogo col-md-12">
									<div class="headerlogo">
										<img src="img_logo.php" height="40px" width="40px">
									</div>
									<div class="companyname text">
										<h1>{{$business->name}}</h1>
									</div>
								</div>
							</div>
							<div class="newrow">
								<div class="address">
									<h4>{{$business->address}} {{$business->postcode}}</h4>
								</div>
							</div>
							<div class="newrow">
								<div class="contact">
									<i class="fa fa-phone-square" aria-hidden="true"></i>
									<span class="tel"><strong>{{$business->phone}} {{$business->mobile}}<strong></span>,
									<i class="fa fa-envelope" aria-hidden="true"></i>
									<span class="email"><strong>{{$business->email}}</strong></span>,
										<i class="fa fa-globe" aria-hidden="true"></i>
									<span class="website">{{$business->website}}</span>
								</div>
							</div>
							
						</div>
<!--date/time-->
						<div class="newrow">
								<div class="datetime">
									<span class="date">{{$transaction->created_at}}</span>&nbsp;&nbsp;
									<span class="time"> {{$transaction->created_at}}</span>
								</div>
							</div>
							<div class="container-main">
				
				<!--SenderReceiver-->
								<div id="senderReceiver" class="senderReceiver">
<!---Sender-->
									<div class="sender">
										<div class="sheading"><h2>Sender<h2></div>
										<div class="item">
											<div class="col1">
												<h4>Sender Name</h4>
											</div>
											<div class="col2">
												<strong>{{$transaction->sender_name}}</strong>
											</div>
										</div>
										<div class="clearfix"></div>
										<div class="item">
											<div class="col1">
												<h4>Residence</h4>
											</div>
											<div class="col2">
												<strong>{{$transaction->Address()->address}}</strong>
											</div>
										</div>
										<div class="clearfix"></div>
										<div class="item">
											<div class="col1">
												<h4>Post Code</h4>
											</div>
											<div class="col2">
												<strong>{{$transaction->Address()->postcode}}</strong>
											</div>
										</div>
										<div class="clearfix"></div>
										<div class="item">
											<div class="col1">
												<h4>Phone Number</h4>
											</div>
											<div class="col2">
												<strong>{{$transaction->sender_phone}}</strong>
											</div>
										</div>
									</div>
									
<!--Receiver-->
									<div class="receiver">
										<div class="sheading"><h2>Receiver</h2></div>
									<div class="clearfix"></div>
										<div class="item">
											<div class="col1">
												<h4>Receiver Name</h4>
											</div>
											<div class="col2">

												<strong>{{$transaction->receiver_name}}</strong>
											</div>
										</div>
										<div class="clearfix"></div>
									
										<div class="item">
											<div class="col1">
												<h4>Receiver Phone</h4>
											</div>
											<div class="col2">
												<strong>{{$transaction->receiver_phone}}</strong>
											</div>
										</div>
										<div class="clearfix"></div>
										<div class="item">
											<div class="col1">
												<h4>Destination</h4>
											</div>
											<div class="col2">
												<strong>Nigeria</strong>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
									
									
								</div>
						<div class="clearfix"></div>
<!--TRansaction-->
								<div id="transPayment" class="transPayment">
								
								<!--Transaction---->
									<div class="trans">
									<div class="sheading">
										<h2>Transaction</h2>
										</div>
										<div class="item">
											<div class="col1">
												<h4>Sending</h4>
											</div>
											<div class="col2">
												<strong> &pound;{{$transaction->amount}} </strong>
											</div>
										</div>
										<div class="clearfix"></div>
										
										<div class="item">
											<div class="col1">
												<h4>Naira Equiv</h4>
											</div>
											<div class="col2">
												<strong>{{$transaction->local_payment}}</strong>
											</div>
										</div>
										<div class="clearfix"></div>
										
										<div class="item">
											<div class="col1">
												<h4>Commission</h4>
											</div>
											<div class="col2">
												<strong>&pound;{{$transaction->commission}}</strong>
											</div>
										</div>
										<div class="clearfix"></div>
										
										<div class="item">
											<div class="col1">
												<h4>Total Amount</h4>
											</div>
											<div class="col2">
												<strong>&pound;{{$transaction->total }}</strong>
											</div>
										</div>
										<div class="clearfix"></div>
										
										<div class="item">
											<div class="col1">
												<h4>Transfer Code</h4>
											</div>
											<div class="col2">
												<strong>{{$transaction->receipt_number}}</strong>
											</div>
										</div>
										<div class="clearfix"></div>
										
										<div class="item">
											<div class="col1">
												<h4>Agent Name</h4>
											</div>
											<div class="col2">{{$business->name}}</strong>
											</div>
										</div>
										<div class="clearfix"></div>
										
									</div>
									
	<!--Payment-->
									<div class="payment">
										<div class="sheading">
										<h2>Payment</h2>
										</div>
										<div class="item">
											<div class="col1">
												<h4>Exchange Rate</h4>
											</div>
											<div class="col2">
												<strong>&pound;1 = &#8358;{{$transaction->exchange_rate}}</strong>
											</div>
										</div>
										<div class="clearfix"></div>
										<div class="item">
											<div class="col1">
												<h4>Bank</h4>
											</div>
											<div class="col2">
												<strong>{{$transaction->bank}}</strong>
											</div>
										</div>
										<div class="clearfix"></div>
										<div class="item">
											<div class="col1">
												<h4>Transfer Mode</h4>
											</div>
											<div class="col2">
												<strong>{{$transaction->transfer_type}}</strong>
											</div>
										</div>
										<div class="clearfix"></div>
										
										<div class="item">
											<div class="col1">
												<h4>Identity</h4>
											</div>
											<div class="col2">
												<strong>{{$transaction->identity}}</strong>
											</div>
										</div>
										<div class="clearfix"></div>
										<div class="item">
											<div class="col1">
												<h4>Account No</h4>
											</div>
											<div class="col2">
												<strong>{{$transaction->account_number}}</strong>
											</div>
										</div>
										<div class="clearfix"></div>
										
										<div class="item">
											<div class="col1">
												<h4>--</h4>
											</div>
											<div class="col2">
												<strong>--</strong>
											</div>
										</div>
										<div class="clearfix"></div>
										
									</div>
								</div>
									<div class="clearfix"></div>
<!---signature-->
									<div class="newrow">
										<div class="companysign">
											<strong>Staff Signature</strong>
										</div>
										<div class="custsign">
											<strong>Sender Signature</strong>
										</div>
									</div>
									<div class="clearfix"></div>
							</div>
			</div>
			<hr>
						
						
		</div>
		
		<div id="dup"></div>
		<div class="newrow">
			<div class="print noprint"><button class="btn btn-primary" onclick="window.print();">PRINT</button>
			<span class="print noprint"><input type="button" id="movehome" value="Home" onclick="window.location = 'prevTrans.php';"></span>
		</div>
		
		</div>
</div>
 <script src="{{asset('js/jquery-1.9.1.min.js')}}"></script>
<script>
	$(function(){
		$("#centerit").clone().insertAfter("#dup:last");
	})
		


</script>