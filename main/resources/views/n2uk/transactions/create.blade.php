@extends('layouts.layout')
@section('content')
	<div class="panel panel-primary">
		<div class="panel-heading">
			<div id="output1"></div>
			<h4 class = "text-center">Send Money N2UK<i class="glyphicon glyphicon-send"></i></h4>
		</div>
		<div class="panel-body">
			
		{!! Form::open(['method'=>'POST', 'action'=>'N2UK\TransactionsController@store','file'=>true]) !!}
			{!! Form::hidden('myurl',URL::to('/'),['id'=>'myurl']) !!}
			{!! Form::hidden('sender_name',null,['id'=>'sender_name']) !!}
			{!! Form::hidden('receiver_name',null,['id'=>'receiver_name']) !!}
			{!! Form::hidden('note',null,['id'=>'note']) !!}
			{!! Form::hidden('identity',null,['id'=>'identity']) !!}
			{!! Form::hidden('type',@Auth::user()->type) !!}
			{!!Form::hidden
					('agent_commission',null,['id'=>'agent_commission']) !!}
			{!! Form::hidden('credit',null,['id'=>'note']) !!}
			{!! Form::hidden('receiver_id',$receiverId,['id'=>'receiver_id']) !!}
				{!! Form::hidden('sender_id',$senderId,['id'=>'sender_id']) !!}
				{!! Form::hidden('user_id',Auth::id(),['id'=>'sender_id']) !!}
							
							
							

		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="form-group">	
					{!! Form::label('sender','Sender') !!}
					<span class="essential">*</span>
					{!! Form::select('senders',$sender,
									null,['class'=>'form-control',
									'id'=>'senders']) !!}
							
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="form-group">
					{!! Form::label('SReceiver','Select Receiver') !!}
					<span class="essential">*</span>
						{!! Form::select('receivers',
									$receiver,null,['class'=>'form-control',
									'id'=>'receivers']) !!}	
				</div>		
								
			</div>
		</div>
<!--Receivers-->
		<div class="well">
			<div class="row">
				<div class="col-md-4 col-md-offset-1">
					<div class="form-group form-group-sm">
						{!! Form::label('Rfirstname',' Receiver First Name') !!}
							{!! Form::text('receiver_fname',
											null,['id'=>'receiver_fname', 'class'=>'form-control','placeholder'=>'Receiver First name','readonly'=>'readonly']) !!}
					</div>
				</div>
				<div class="col-md-4 col-md-offset-1">
					<div class="form-group form-group-sm">
						{!! Form::label('Rlastname','Receiver Last Name') !!}
						{!! Form::text('receiver_lname',
										null,['id'=>'receiver_lname', 'class'=>'form-control','placeholder'=>'Receiver Last name','readonly'=>'readonly']) !!}
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-md-offset-1">
					<div class="form-group form-group-sm">
						{!! Form::label('receiver_phone','Receiver Phone') !!}
								{!! Form::text('receiver_phone',
											null,['id'=>'receiver_phone', 'class'=>'form-control','placeholder'=>'Receiver phone','readonly'=>'readonly']) !!}
					</div>
				</div>
				<div class="col-md-4 col-md-offset-1">
					<div class="form-group form-group-sm">
						{!! Form::label('Transfer Option','Transfer Option') !!}
						{!! Form::text('transfer_type',null,
										['id'=>'transfer_type', 'class'=>'form-control','placeholder'=>'transfer_type','readonly'=>'readonly']) !!}
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-md-offset-1">
					<div class="form-group form-group-sm">
						{!! Form::label('bank','Receiver UK Bank') !!}
						{!! Form::text('uk_bank',
									null,['id'=>'uk_bank','class'=>'form-control',
									'placeholder'=>'Receiver Bank','readonly'=>'readonly']) !!}

					</div>
				</div>
				<div class="col-md-2 col-md-offset-1">
					<div class="form-group form-group-sm">
						{!! Form::label('accountno','Account No') !!}
							{!! Form::text('uk_account_no',
											null,['id'=>'uk_account_no',
											 'class'=>'form-control','placeholder'=>'Receiver Account number ','readonly'=>'readonly']) !!}
					</div>
							</div>
					<div class="col-md-2">
						<div class="form-group form-group-sm">
							{!! Form::label('Sort Code','Sort Code') !!}
							{!! Form::text('uk_sort_code',
											null,['id'=>'uk_sort_code',
											 'class'=>'form-control',
											 'placeholder'=>'Receiver Account number ','readonly'=>'readonly']) !!}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4 col-md-offset-1">
						<div class="form-group form-group-sm">
							{!! Form::label('Receiver Nig Bank',
											'Receiver Nig Bank') !!}
							{!! Form::text('ng_bank',
										null,['id'=>'ng_bank', 'class'=>'form-control','placeholder'=>'Receiver Bank','readonly'=>'readonly']) !!}

						</div>
				</div>
				<div class="col-md-2 col-md-offset-1">
					<div class="form-group form-group-sm">
						{!! Form::label('Ng Account No','Ng Account No') !!}
						{!! Form::text('ng_account_no',null,
									['id'=>'ng_account_no', 'class'=>'form-control','placeholder'=>'Receiver Account number ','readonly'=>'readonly']) !!}
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group form-group-sm">
						{!! Form::label('Acct Name','Acct Name') !!}
						{!! Form::text('ng_account_name',
								null,['id'=>'ng_account_name', 'class'=>'form-control','placeholder'=>'Receiver Account number ','readonly'=>'readonly']) !!}
					</div>
				</div>
			</div>
			</div>
<!--Transaction operation here-->
			
				<div class="row">
					<div class="col-md-5 col-md-offset-3">
						<div class="form-group form-group-sm">
						{!! Form::label('Amount £',
									'Amount £',['id'=>'amount_label']) !!}
						<span class="essential">*</span>
						{!! Form::text('amount_pounds',null,['id'=>'amount_pounds','class'=>'form-control','placeholder'=>'Amount £']) !!}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-5 col-md-offset-3">
						<div class="form-group form-group-sm">
							{!! Form::label('Amount &#8358; ','Amount &#8358;') !!}
							{!! Form::text('amount_naira',null,['id'=>'amount_naira','class'=>'form-control','placeholder'=>'Amount &#8358;']) !!}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-5 col-md-offset-3">
						<div class="form-group form-group-sm">
							{!! Form::label('Sold Rate','Sold Rate') !!}
							{!! Form::text('sold_rate',
									App\N2UK\Rate::sold_rate(),
									['id'=>'soldRate','class'=>'form-control','placeholder'=>'Sold Rate','readonly'=>'readonly']) !!}
						</div>
					</div>
				</div>
				<div class="row">	
					<div class="col-md-5 col-md-offset-3">
						<div class="form-group form-group-sm">
							{!! Form::label('Bou Rate',
										'Bou Rate',['id'=>'bou_rate']) !!}
							{!! Form::text('bou_rate',
											App\N2UK\Rate::bou_rate(),
											['id'=>'bouRate','class'=>'form-control','placeholder'=>'Bou Rate','readonly'=>'readonly']) !!}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-5 col-md-offset-3">
						<div class="form-group form-group-sm">
							<div class="form-group form-group-sm">
								{!! Form::label('Margin','Margin') !!}
								{!! Form::text('margin',
									App\N2UK\Rate::bou_rate() - App\N2UK\Rate::sold_rate(),
									['id'=>'margin','class'=>'form-control',
									'placeholder'=>'Margin',
									'readonly'=>'readonly']) !!}
						</div>
						</div>
					</div>
				</div>
				<div class="row">
							{!! Form::submit('Submit',['class'=>'btn btn-primary col-md-12 btn-block']) !!}
				</div>
				
			</form>
		</div>
		
	</div>			
@stop

@section('script')	
	$.getScript("{{asset('js/n2uk/create-transaction.js')}}", function() {
   console.log('script running')
});	
@stop