@extends('layouts.layout')
@section('content')
	
	<br/>
	 <div class="row">
					<div class= "col-md-12">
						<h4>Transactions N2UK</h4>
					</div>
								
	</div>

<!--------------------------------------Search Panel-------------------------------------------- -->

<div class="row well" >
{!! Form::open(['method'=>'POST', 'action'=>'N2UK\TransactionsController@post_search','file'=>true]) !!}
							<div class="col-md-12" >
								<div class="form-group input-group form-group-sm">
									<span class="input-group-addon"><i class="fa fa-filter"></i></span>
									{!! Form::text('query_string',null,['class'=>'form-control',
										'id'=>'getTransactions','placeholder'=>'Search WE£446GF,0796643,John']) !!}
								</div>
							</div>

							<div class="col-md-6">
								{!! Form::label('Start Date', 'Start Date')!!}
								<div class="form-group input-group form-group-sm">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									{!! Form::date('from', \Carbon\Carbon::now(), ['class'=>'form-control','id'=>'datefrom'])!!}
								</div>
							</div>

							<div class="col-md-6">
								{!! Form::label('End Date', 'End Date')!!}
								<div class="form-group input-group form-group-sm">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									{!! Form::date('to',\Carbon\Carbon::now(), ['class'=>'form-control','id'=>'dateto'])!!}
								</div>
							</div>
							
							
							{!! Form::submit('Apply',['class'=>'btn btn-danger col-md-12 btn-block']) !!}
			{!! Form::close() !!}
	</div>


<!----------------------------------------------------------------search ends here---------------------------- !-->

	
	<div class="row">
		<div class="col-md-12">
			 <table id="sort-table" class="table table-striped table-bordered tablesorter">
				<thead>
							<tr>
								<th>No </th>
								<th>Date <i class=""></i></th>
								<th>Tcode  </th>
								<th>Sender <i class=""></i></th>
								<th>Receiver </th>
								<th>Amt(£)</th>
								<th>Amt(&#8358;)</th>
								<th>Bou</th>
								<th>Sold</th>
								<th>Margin</th>
								<th>Profit</th>
								<th>Actions</th>
								
								
							</tr>
					</thead>
					<tbody class="text-center">
						<?php $x=0;$tamt_send=0;$totalAmount=0;$totalLocalAmount=0;$totalAgentCommission=0;$totalBusinessCommission=0; $tTotal=0; ?>

			@foreach($transactions as $transaction)
						<tr>
							<td>{{$x=$x+1}}</td>
							<!-- <td>{{ $transaction->created_at->diffForhumans()}}</td> -->
							<td>{{ $transaction->created_at}}</td>
							<td>{{ucfirst($transaction->transfer_code)}}</td>
							<td>{{ $transaction->customer->name}}</td>
							<td>{{ $transaction->receiver->fname}}</td>
							<td>{{ number_format($transaction->amount_pounds, 2)}}</td>
							<td>{{$transaction->amount_naira}}</td>
							<td>{{$transaction->bou_rate}}</td>
							<td>{{$transaction->sold_rate}}</td>
							<td>{{$transaction->margin}}</td>
							<td>{{number_format((($transaction->amount_pounds * $transaction->bou_rate) -
								($transaction->amount_pounds * $transaction->sold_rate)),2) }}</td>
							<td> 
								<a href="{{route('transactions.status',
								$transaction->id)}}"class="btn btn-default">{{$transaction->status}}</a>

									
									
							
								<a href="{{route('transactions_n2uk.edit',$transaction->id)}}"
								 class="btn btn-default">Edit</a>

							 
								<a href="{{route('transactions_n2uk.receipt',$transaction->id)}}" 
								class="btn btn-default"><i class="fa fa-print" aria-hidden="true"></i>
								Receipt</a>

								{!! Form::open(['method'=>'DELETE', 'action'=>[ 'N2UK\TransactionsController@destroy',
								$transaction->id], 'files'=>true]) !!}
								{!! Form::submit('Delete',['class'=>'btn btn-danger']) !!}
								{!! Form::close() !!}
							</td>

							
								
									
									
					 </tr>
							 @endforeach

						<tr>
								<td colspan="5"><strong>Total</td>
								<td><strong>&pound;{{number_format(($transactions->sum('amount')),2)}}</strong></td>
								<td><strong>&#8358;{{number_format($totalLocalAmount,2)}}</strong></td>
								<td><strong>&pound;{{ number_format($totalAgentCommission,2)}}</strong></td>
								<td><strong>&pound;{{ number_format($totalBusinessCommission,2)}}</strong></td>
								<td><strong>&pound;{{ number_format($tTotal,2) }}</strong></td>
								</strong>
							</tr>
						</tbody>
			</table>
				<div class="row">
					<div class="col-md-5 col-md-offset-3 text-center">
						{{$transactions->render()}}
					</div>
				</div>
		</div>
	</div>
@endsection

@section('script')
	$(function(){
<!--Trigger Autocomplete --> 
				 $('#clicksearch').on('click', function(e) {
				      $('#datesearch').toggle();
				      e.preventDefault();
				    });
				 url  = $('#myurl').val() + '/transaction/search'  
<!--Autocomplete --> 
				 $( "#getTransactions" ).autocomplete({	
				 	max:10,
					//source:'quickSearch.php',
					
						 source: function (request, response) {
							 $.ajax({
								 url:  "{{route('transactions.autocompletetips',"")}}"+"/" + request.term ,
								 data: { term: request.term },
								 dataType: "json",
								 success: response,
								 error: function () {
									 response([]);
								 }
							 });
						 },
					select: function( event, ui ) {
						var add=url;		
					window.location.href = add+'/id/'+ui.item.id;
					}
    });
   <!--Date Search --> 
   					$('#dateto').on('change', function (e) {
   					value = $('#datefrom').val() + ',' + $('#dateto').val()
   					window.location.href = url +'/date/'+ value
   				});
				
			});
@endsection