@extends('layouts.layout')
@section('content')
	<div class="row">
	<div class="col-md-12">
					<div class="ui-widget">
						<input type="text" id="getcustomer" class="form-control" placeholder=" Ben...." >
					</div>
				</div>
				<div class="col-md-6">
					<h3>Customers N2UK</h3>
				</div>
	</div>
	
	<br/>
<a href= {{route('customers_n2uk.create')}} class = "btn btn-primary">New Customer</a>
	 <div class="col-md-12">
		<div class="panel panel-default">	
			<div class=" panel-body">
					<table class="table table-striped table-bordered tablesorter">
			<thead>
				
					<tr>
						<th>No </th>
						<th>Name <i class=""></i></th>
						<th colspan="6">General Quick View</th>
									
					</tr>
				</thead>
				
				<tbody>
					<?php $avater="noavatar.jpg";$x=0; 
					foreach($customers as $customer): ?>
					<tr>
						<td>
							<input type="checkbox" 
								id="check_list" name="check_list" value="">
							<?php echo $x=$x+1; ?>					
						</td>
										
						<td> {{str_limit(ucfirst($customer->fname." ".$customer->lname),10)}} </td>
<!----View Profile---->										
						<td>
							<a href="{{route('customers_n2uk.edit',$customer->id)}}">
							<i class="fa fa-user" aria-hidden="true"> </i> Edit Profile </a>
						</td>
						<td>
						 	{!! Form::open(['method'=>'DELETE', 'action'=>[ 'N2UK\CustomersController@destroy', $customer->id ], 'files'=>true]) !!}	
						 	{!! Form::submit('Delete', ['class'=>'btn btn-danger btn-block'])!!}
						 	{!! Form::close() !!}
						 </td>
						<td>
							<a href="{{route('receivers_n2uk.create',$customer->id)}}">
							 Create Receivers </a>
						</td>
<!----Customers---->											
						<td>
							<a href="{{route('receivers_n2uk.receivers',$customer->id)}}">
								Receivers  
								{{App\N2UK\Receiver::whereSender_id($customer->id)->count()}}
							</a>																															
						</td>
<!----Peniding Transaction---->											
						<td>
							<a href=
								"{{route('transactions_n2uk.list',$customer->id)}}"
									class="btn btn-success"><i class="fa fa-money" aria-hidden="true"> 
											 </i> Transactions	{{App\N2UK\Transaction::whereSender_id($customer->id)->count()}}  
											</a>																											
						</td>
										
<!----send---->		<td>
							<a href="
									{{route('transactions_n2uk.transfer',
									[$customer->id,'0'])}}"
									class="btn btn-danger"> Send Now</a>																											
						</td>
									
					</tr>
									
										<?php endforeach; ?>
					
										
			</table>
					<div class="row">
					<div class="col-md-5 col-md-offset-3 text-center">
						{{$customers->render()}}
					</div>
				</div>
						
			</div>
		</div>
	</div>
@endsection





@section('script')
	$(function(){
				 $( "#getcustomer" ).autocomplete({	
				 	max:10,
					//source:'quickSearch.php',
					
						 source: function (request, response) {
							 $.ajax({
								 url:  "{{route('customer_n2uk.autocomplete')}}" ,
								 data: "",
								 dataType: "json",
								 success: response,
								 error: function () {
									 response([]);
								 }
							 });
						 },
					select: function( event, ui ) {
						var add="{{route('customers_n2uk.show',"")}}";		
					window.location.href = add+'/'+ui.item.id;
					}
    });
				
			});
@endsection