@extends('layouts.layout')
@section('content')


<!--------------------------------------search payment-------------------------------------------- -->


<div class="col-md-12 well" >
{!! Form::open(['method'=>'POST', 'action'=>'PaymentsController@post_search','file'=>true]) !!}
{!! Form::hidden('type','commission') !!}		

							<div class="col-md-6">
								{!! Form::label('Start Date', 'Start Date')!!}
								<div class="form-group input-group form-group-sm">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									{!! Form::date('from', \Carbon\Carbon::now(), ['class'=>'form-control','id'=>'datefrom'])!!}
								</div>
							</div>

							<div class="col-md-6">
								{!! Form::label('End Date', 'End Date')!!}
								<div class="form-group input-group form-group-sm">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									{!! Form::date('to',\Carbon\Carbon::now(), ['class'=>'form-control','id'=>'dateto'])!!}
								</div>
							</div>
							
							
							{!! Form::submit('Apply',['class'=>'btn btn-danger col-md-12 btn-block']) !!}
			{!! Form::close() !!}
	</div>

<!----------------------------------------------------------------search payment-------------------------- !-->

	
	 <div class="col-md-12 text-center">
		<h4>  {{$user}} Payment History</h4>	
				
			
					<table class="table table-striped table-bordered tablesorter">
			<thead>
					<tr>
						<th></th>
						<th>No </th>
						<th>Date</th>	
						<th class = "text-center">Amount </th>
						<th class = "text-center">Fully Paid </th>			
					</tr>
				</thead>
				
				
				<tbody>
						<?php $avater="noavatar.jpg";$x=0; ?>
							@foreach($payments as $key=> $payment)
					<tr>
						<td>
							<input type="checkbox" id="check_list" name="check_list" value="">
						</td>
						<td>
							{{$key +1}}					
						</td>
										
						<td> {{$payment->created_at->format('Y-m-d')}} </td>
						
						<td>
								{{number_format($payment->amount,2)}}
						</td>
						
						<td>
							@if($payment->fully_paid === 0)
								<span class="text-danger">no</span>
							@else
							<span class="">yes</span>
							@endif
						</td>
					</tr>
									
					@endforeach
					
										
			</table>
					
				{{$payments->render()}}		
			
		</div>
	
@endsection





