@extends('layouts.layout')
@section('content')
	 <div class="col-md-9 col-md-offset-1">
		<div class="panel panel-default">
			<div class=" panel-headingt">
				<h4>Member Status Update</h4>
			</div>
		<div class=" panel-body">
			{!! Form::model($member,['method'=>'PATCH','action'=>['UsersController@updatestatus',$member->id],'file'=>true]) !!}


				<div class="row">
					<div class="col-md-12">
<!--Member Name--><div class="form-group">	
						{!! Form::label('Member','Member') !!}
						{!! Form::text('name',null,['id'=>'no', 'class'=>'form-control','placeholder'=>'Receipt Number','readonly'=>'readonly']) !!}
						</div>
					</div>
				</div>
				<div class="row">
						<div class="col-md-12">
	<!--is Active--><div class="form-group">	
							{!! Form::label('Status','Status') !!}
							{!! Form::select('is_active',['0' =>'Suspended','1'=>'Active'],
							null,['class'=>'form-control','id'=>'Status']) !!}
							</div>
						</div>
				</div>
				<div class="form-group">
							{!! Form::label('Member Type','Member Type') !!}<span class="essential">*</span>
							{!! Form::select('type',['customer' =>'customer','agent'=>'agent','admin'=>'admin'],
							null,['class'=>'form-control','id'=>'Status']) !!}
							
				</div>
				<div class="form-group">
							{!! Form::label('Note','Note') !!}
							{!! Form::textarea('body',
							null,['class'=>'form-control','placeholder'=>'Note to Member','id'=>'body']) !!}
							
				</div>
				<hr>

				<div class="row">
						<div class="col-md-12">
	<p></p>
	<!--Authenticate--><div class="form-group text-center">	
							{!! Form::label('Password','Password') !!}<span class="essential">*</span>
							{!! Form::password('pass',['id'=>'no', 'class'=>'form-control']) !!}
							</div>
						</div>
				</div>
				<div class="row">
							{!! Form::submit('Submit',['class'=>'btn btn-primary col-md-12 btn-block']) !!}
				</div>
			{!! Form::close() !!}

		</div>
		</div>
	</div>
		
		@if($notes)
<!-- Notes History-->
								
			<div class = "col-md-11 col-md-offset-1">
			<h4>{{$member->name}} Note History</h4>
			<table class="text-center table table-striped table-bordered tablesorter">
				<thead>
					<tr>
								<th>Date </th>
								<th>Notes </th>			
					</tr>
				</thead>
				<tbody>
						@foreach($notes as $key=>$note)
					<tr>
						<td>
								{{$note->created_at->format('d/m/Y')}}					
						</td>

						<td>
								{{$note->body}}					
						</td>
					</tr>
					@endforeach
			</tbody>
		</table>
									{{$notes->render()}}
			</div>
	
	@endif
@endsection