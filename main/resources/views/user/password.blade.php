@extends('layouts.layout')
@section('content')
<div class="row">
    	<div class="col-sm-9 col-sm-offset-1">
    		<div class="panel panel-primary">
				<div class="panel-heading">
						<h3>Modify Password</h3>
				</div>
			<div class=" panel-body">
				{!! Form::open(['method'=>'PATCH', 'action'=> ['UsersController@passwordUpdate',@Auth::id()], 'files'=>true]) !!}
				{!! Form::hidden('user_id',Auth::id()) !!}
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
					{!! Form::label('password', 'Existing Password')!!}
					{!! Form::password('password',['class'=>'form-control'])!!}
								</div>
				</div>
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
					{!! Form::label('password', 'New Password')!!}
					{!! Form::password('new_password',['class'=>'form-control'])!!}
								</div>
				</div>
				<div class="row">
					<div class="col-md-8 col-md-offset-2"><p></p>
					 {!! Form::submit('submit',['class'=>'btn btn-primary btn-block'])!!}
								</div>
				</div>
						{!! Form::close() !!}	
				</div>
			</div>
	</div>
</div>
					
			

@endsection