@extends('layouts.layout')
@section('content')

					<div class="row">
						<div class="col-md-4">
							<small class="essential">Areas Marked with * are important</small>
						</div>
						<div class="col-md-4">	
							<ol class="breadcrumb">
							  <li><a href="index.php">Home</a></li>
							  <li><a href="senders.php">Senders</a></li>
							  <li class="active">New Senders</li>
							</ol>
						</div>
					</div><h3>Register New Sender</h3>
<!-- Form -->
		{!! Form::model($user,['method'=>'PATCH', 'action'=> ['UsersController@update',$user->id], 'files'=>true]) !!}
		{!! Form::hidden('photo_id', null, ['class'=>'form-control'])!!}
		{!! Form::hidden('address_id', null, ['class'=>'form-control'])!!}
					 <div class="panel panel-default">
						<div class="panel-heading">
							
						</div>
					<div class="panel-body">
						<div class="row">
								<div class="col-md-3 col-md-offset-1">
										<div class="form-group form-group-sm">
											{!! Form::label('title', 'Title:') !!}
							                {!! Form::select('title', ['mr'=>'Mr','miss'=>'Miss','mrs'=>'Mrs'] , null, ['placeholder'=>'Choose Category','class'=>'form-control'])!!}
										</div>
								</div>
						@if(Auth::user()->isAdmin())
								<div class="col-md-4 ">
											<div class="form-group form-group-sm">
												<span class="essential">*</span>
												{!! Form::label('Type', 'Type')!!}
												{!! Form::select('type', ['customer'=>'customer','agent'=> 'agent','manager'=>'manager','admin'=>'admin'],null,['placeholder'=>'Choose Member Category','class'=>'form-control'])!!}
															
											</div>
								</div>

								<div class="col-md-4 ">
											<div class="form-group form-group-sm">
												<span class="essential">*</span>
												{!! Form::label('Status', 'Status')!!}
												{!! Form::select('is_active', ['1'=>'active','0'=> 'suspended'],null,['placeholder'=>'Choose Member Status','class'=>'form-control'])!!}
															
											</div>
								</div>
						@endif
						</div>														
						</div>
						<div class="row">
							<div class="col-md-5 col-md-offset-1">
									<div class="form-group form-group-sm">
									{!! Form::label('Currency Code','Currency Code') !!}
									{!! Form::select('currency_id',$currencies,null,['class'=>'form-control','required'=>'required','id'=>'currency_id']) !!}
									</div>
							</div>
						</div>
						
							<div class="row">																					
								<div class="col-md-5 col-md-offset-1 ">
									<div class="form-group form-group-sm">
										<span class="essential">*</span>
										{!! Form::label('fname', 'First Name')!!}
										{!! Form::text('fname', null, ['class'=>'form-control'])!!}
													
									</div>
								</div>
										
								<div class="col-md-5">
										<div class="form-group">
										
										{!! Form::label('mname', 'Middle Name')!!}
										{!! Form::text('mname', null, ['class'=>'form-control'])!!}
													
									</div>
								</div>
							</div>
							
							<div class="row">																				
								<div class="col-md-5 col-md-offset-1">
										<div class="form-group form-group-sm">
											<span class="essential">*</span>
											{!! Form::label('lname', 'Last Name')!!}
											{!! Form::text('lname', null, ['class'=>'form-control'])!!}
														
										</div>
								</div>
										
										<div class="col-md-5">
												{!! Form::label('dob', 'Date Of Birth')!!}
												<div class="form-group input-group form-group-sm">
													 <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
													{!! Form::Date('dob', null, ['class'=>'form-control','id'=>'date1'])!!}
												</div>
										</div>
							</div>
							<div class="row">
							
									<div class="title_header col-md-12">Contact Information</div>
									
							</div>
							<div class="row">	
								<div class="col-md-5 col-md-offset-1">
									<div class="form-group form-group-sm">
										
										{!! Form::label('phone', 'Phone Number')!!}
										{!! Form::text('phone', null, ['class'=>'form-control'])!!}
														
									</div>
								</div>
										
								<div class="col-md-5">
									<div class="form-group form-group-sm">
										<span class="essential">*</span>
										{!! Form::label('mobile', 'Mobile Number')!!}
											@if($user->validPhone->is_valid)
											<i class ='fa fa-check text-success'></i>
											@else
												@if($user->validPhone->code )
													<i><a href="#" data-toggle="modal" data-target="#validate"  class="text-danger" style="font-size:12px;">Please enter code </a></i>
												@else
													<i><a href = "{{route('message.validate_mobile',['user',$user->id])}}" class="text-danger" style="font-size:12px;">Please click to confirm with sms </a></i>
												@endif
											@endif
										{!! Form::text('mobile', null, ['class'=>'form-control'])!!}
														
									</div>
								</div>
							</div>
							
							<div class="row">													
								<div class="col-md-5 col-md-offset-1">
									<div class="form-group form-group-sm">
										<span class="essential">*</span>
										{!! Form::label('email', 'Email')!!}
										{!! Form::text('email', null, ['class'=>'form-control'])!!}
														
									</div>
								</div>
								<div class="col-md-5">
									<div class="form-group form-group-sm">
										<span class="essential">*</span>
										{!! Form::label('cemail', 'Confirm Email')!!}
										{!! Form::text('cemail', null, ['class'=>'form-control'])!!}
										
									</div>
								</div>
							</div>
							<div class ="row">
								<div class="col-md-4 col-md-offset-2">
									<div class="form-group">
							            {!! Form::label('identity_label', 'Upload Identity') !!}
							            {!! Form::file('identity_proof', null,['class'=>'form-control'])!!}
							            <br/>
							            <img height="50" src="{{$user->photo ? asset('images').'/'.$user->photo->identity_proof : 'http://placehold.it/400x400' }} " alt="">
							        </div>
								</div>
										
								<div class="col-md-6">
									<div class="form-group">
							            {!! Form::label('address_label', 'Upload Proof Of Address') !!}
							            {!! Form::file('address_proof', null,['class'=>'form-control'])!!}
							            <br/>
							            <img height="50" src="{{$user->photo ? asset('images').'/'.$user->photo->address_proof : 'http://placehold.it/400x400' }} " alt="">
							        </div>
								</div>
							</div>
							
							<div class="row">
											<div class="title_header">Residence</div>
										<div class="col-md-7 col-md-offset-2">
											{!! Form::label('Address Postocode', 'Address / Postcode')!!}<span class="essential">*</span>
										</div>
										<div class="col-md-7 col-md-offset-2">
											<div class="form-group form-group-sm">
												<input name = "search" type=text required
													  id="autocomplete" class="form-control"  onFocus="geolocate()"
													 value="{{$user->address->address?$user->address->address:''}}">
												</div>
												
   										 </div>
							</div>
											
								
							<br/><br/>
							<div class="row">
								<div class="col-md-2 col-md-offset-2">
									<div class="form-group form-group-sm">
										{!! Form::label('No', 'Number')!!}
										{!! Form::text('number',$user->address->address?explode(' ',$user->address->address)[0]:'-',
										  ['class'=>'form-control','id'=>'street_number'])!!}
										  
									</div>		
								</div>
								<div class="col-md-7">
									<div class="form-group form-group-sm">
										{!! Form::label('Address', 'Address')!!}
										{!! Form::text('address1', $user->address->address, ['class'=>'form-control','id'=>'route','readonly'=>'readonly'])!!}
									</div>		
								</div>
										
										
							</div>
							<div class="row">
										<div class="col-md-5 col-md-offset-1 ">
											<div class="form-group form-group-sm">
												{!! Form::label('postcode', 'Post Code')!!}
												{!! Form::text('postcode',$user->address->postcode, ['class'=>'form-control','id'=>'postal_code'])!!}
											</div>
										</div>
										
										<div class="col-md-5">
												<div class="form-group form-group-sm">
												{!! Form::label('town', 'Town')!!}
												{!! Form::text('town',$user->address->town, ['class'=>'form-control','id'=>'locality','readonly'=>'readonly'])!!}
											</div>
										</div>
							</div>																		
							
							<div class="row">
										<div class="col-md-5 col-md-offset-1">
												<div class="form-group form-group-sm">
												{!! Form::label('county', 'County')!!}
												{!! Form::text('county',$user->address->county, ['class'=>'form-control','id'=>'administrative_area_level_1','readonly'=>'readonly'])!!}
											</div>
										</div>
										
											<div class="col-md-5">
												<div class="form-group form-group-sm">
												{!! Form::label('country', 'Country')!!}
												{!! Form::text('country',$user->address->country, ['class'=>'form-control','id'=>'country','readonly'=>'readonly'])!!}
											</div>
											</div>
							</div>
							
										<div class="row">
											<div class="col-md-5 col-md-offset-1">
												<div class="title_header"><span class="essential">*</span>Terms And Conditions</div>
											</div>
										</div>
										<div class="row">	
												
												<div class="col-md-12">
														<p><strong>Clicking the submit button you have  read and accepted the <a href="terms.php">TERMS AND CONDITIONS</a> and Your information will be held securely 
														with our data protection and management policy?</strong><p>
												</div>																																
										
										</div>
									
										<div class="row">
												<div class="col-md-6">
													{!! Form::submit('Update', ['class'=>'btn btn-primary col-md-12'])!!}					
												
												</div>
						{!! Form::close() !!}	
												<div class="col-md-6">
													{!! Form::open(['method'=>'DELETE', 'action'=> ['UsersController@destroy',$user->id]]) !!}

													{!! Form::submit('Delete', ['class'=>'btn btn-danger col-md-12'])!!}	

													{!! Form::close() !!}			
												</div>
													
										</div>	
					</div>

			
<!-- Validation code -->
<div class="modal fade" id="validate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Enter Validation Code</h4>
      </div>
      <div class="modal-body">
        {!!Form::open(['method' =>'post','action'=>'MessageController@confirm_code']) !!}
			{!!Form::hidden('type','user') !!}
			{!!Form::hidden('id',$user->id) !!}
			{!!Form::text('code',null,['class'=>'form-control','placeholder'=>'Enter Code fom sms']) !!}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {{Form::submit('Submit',['class'=>'btn btn-primary'])}}
      </div>
	  {!! Form::close() !!}
    </div>
  </div>
</div>
			

@endsection