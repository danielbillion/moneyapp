@extends('layouts.layout')
@section('content')
	<div class="row well" >
		{!! Form::open(['method'=>'POST', 'action'=> 'TransactionsController@post_search', 'files'=>true]) !!}	
		{!! Form::hidden('search','new') !!}
							<div class="col-md-7" >
								<div class="form-group input-group form-group-sm">
									<span class="input-group-addon"><strong> Search receipt | phone </strong><i class="fa fa-filter"></i></span>
									{!! Form::text('query_string',null,['class'=>'form-control',
										'id'=>'getTransactions','placeholder'=>'Search WE£446GF,0796643,John']) !!}
								</div>
							</div>

							<div class="col-md-4">
								<div class="form-group input-group">
									<span class = "input-group-addon"><strong>Destination</strong></span>
										{!! Form::select('currency_id',$currencies,null,['class'=>'form-control','required'=>'required','id'=>'currency_id']) !!}
									</div>
								</div>
					
						
							<div class="col-md-6">
								{!! Form::label('Start Date', 'Search Start Date')!!}
								<div class="form-group input-group form-group-sm">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									{!! Form::date('from', \Carbon\Carbon::now(), ['class'=>'form-control','id'=>'datefrom'])!!}
								</div>
							</div>

							<div class="col-md-6">
								{!! Form::label('End Date', 'End Date')!!}
								<div class="form-group input-group form-group-sm">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									{!! Form::date('to',\Carbon\Carbon::now(), ['class'=>'form-control','id'=>'dateto'])!!}
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="form-group input-group">
										<span class = "input-group-addon"><strong>Status</strong></span>
									{!! Form::select('status',['pending'=>'pending','paid'=>'paid','suspended'=>'suspended'],
									null,['class'=>'form-control']) !!}
								</div>
							</div>
							{!! Form::submit('Apply',['class'=>'btn btn-danger col-md-6 btn-block']) !!}
							
							<div class="col-md-12 text-center" style = "margin-top:8px">
								<div class="row">	
									<div class="col-md-3 col-md-offset-3">
										<strong><u>Quick Search </u><i class="fa fa-search-plus fa-1x"></i></strong>
									</div>
									<div class="col-md-5">
										<a href="{{route('transactions.search',
										['type' =>'today','value'=>'today'])}}">Today | </a>&nbsp;
										<a href="{{route('transactions.search',
										['type' =>'yesterday','value'=>'yesterday'])}}"> Yesterday | </a>
										<a href="{{route('transactions.search',
										['type' =>'month','value'=>'month'])}}">A Month Ago</a>
								</div>
							</div>
			
			{!! Form::close() !!}
				
		

				</div>
	</div>
	
	<br/>
	 <div class="row">
					<div class="col-md-3">
						<ol class="breadcrumb">
						<li class="active">{{$header==$header?$header:""}} Transactions</li>
						</ol>
					</div>
	@if(Auth::user()->type == 'admin')			
					<div class="col-md-4 col-md-offset-2">
						<ol class="breadcrumb">
							<li class="active"><a href="{{route('transactions.customer')}}">Customer</a></li>
							<li class="active"><a href="{{route('transactions.agent')}}">Agent</a></li>
						</ol>
					</div>			
	</div>
	@endif
	@if(count($transactions) > 0 )
	<div class="row">
		<div class="col-md-12">
		
			 <table id="sort-table" class="table table-striped table-bordered tablesorter">
				<thead>
							<tr>
								<th>No </th>
								<th>Date <i class=""></i></th>
								@if(Auth::user()->isAdmin())
								<th>Type</th>
								<th>Agent</th>
								@endif
								<th>Tcode  </th>
								<th>Sender <i class=""></i></th>
								<th>Receiver </th>
								<th>Amt</th>
								<th>Local Pay</th>
								<th>C_B / C-ag</th>
								<th>Total</th>
								<th><i class="fa fa-sorting-alpha-up"></i> Status</th>
								<th colspan= "3"></i> Actions</th>
								
							</tr>
					</thead>
					<tbody>
						<?php $x=0;$tamt_send=0;$totalAmount=0;$totalLocalAmount=0;$totalAgentCommission=0;$totalBusinessCommission=0; $tTotal=0; ?>

				@foreach($transactions as $transaction)
							<tr>
							<td>{{$x=$x+1}}</td>
							<!--<td>{{ $transaction->created_at->diffForhumans()}}</td> -->
							<td>{{\Carbon\Carbon::parse($transaction->created_at)->format('d/m/Y')}}</td>
							@if(Auth::user()->isAdmin())
							<td>{{ucfirst(str_limit($transaction->type,5))}}</td>
								@if($transaction->type==='agent')
								<td>{{ucfirst(str_limit($transaction->user->name,11))}}</td>
								@else <td>--</td>
							@endif
							@endif
							<td>{{ucfirst($transaction->receipt_number)}}</td>
							<td>{{ ucfirst(str_limit($transaction->sender->name,11))}}</td>
							<td>{{ ucfirst($transaction->receiver->name)}}</td>
							<td>{{ number_format($transaction->amount, 2)}}</td>
							<td>{{$transaction->local_payment}}</td>
							<td>{{$transaction->business_commission}} / 
								{{$transaction->agent_commission}}</td>
							<td>{{$transaction->total}}</td>
							<td> <a class = "{{$transaction->body?'blink_me':''}}"
								href={{Auth::user()->type==='admin'?route('transactions.status',$transaction->id):'#'}}
								class="{{$transaction->status ==='pending'?'btn btn-danger':'btn btn-primary'}}">
								{{$transaction->status}}</a></td>
	
									
							<td colspan = "3"> 
								<a href="{{route('transactions.edit',$transaction->id)}}" class="btn btn-default"><strong>Edit</strong></a>
								
								@if(Auth::user()->isAdmin() || Auth::user()->isManager() )
									{!! Form::open(['method'=>'DELETE', 'action'=>[ 'AgentTransactionsController@destroy',
										$transaction->id], 'files'=>true,'class'=>'delete']) !!}
										{!! Form::submit('Delete',['class'=>'btn btn-danger']) !!}
									{!! Form::close() !!}
								@else
								@endif	
									<a href="{{route('transactions.receipt',$transaction->id)}}" class="btn btn-default">
									<i class="fa fa-print" aria-hidden="true"></i><strong>Receipt</strong></a>
								</td>		
									
								 </tr>
					 @endforeach
				
			 
						</tbody>
			</table>
			
			<div class="row">
					<div class="col-md-3 text-center">
						[ Total Amount:<strong> &pound;{{number_format($transactions->sum('amount'),2)}} </strong> ]
					</div>
					<div class="col-md-3 text-center">
					[ Total Amount + Total Com: <strong>&pound;{{number_format(
						$transactions->sum('amount') + $transactions->sum('commission'),2)}}</strong> ]
					</div>
					<div class="col-md-3 text-center">
					[ Agent Com:<strong> &pound;{{number_format($transactions->sum('agent_commission'),2)}}</strong> ]
					</div>
					<div class="col-md-3 text-center">
					[ Business Com:<strong> &pound;
						{{number_format(($transactions->sum('commission')-$transactions->sum('agent_commission')),2)}}</strong> ]
					</div>
				<div class="row">
					<div class="col-md-12 text-center">
						{{$transactions->render()}}
					</div>
				</div>
			
		</div>
		
	</div>
	@else
		<p class = "text-center text-danger"> <strong>Search Result Unavailable</strong></p>
		
		@endif

@endsection

@section('script')
	$(function(){

				
				
<!--confirm Delete --> 
					$(".delete").on("submit", function(){
					return confirm("Are you sure?");
						});


<!--Trigger Autocomplete --> 
				 $('#clicksearch').on('click', function(e) {
				      $('#datesearch').toggle();
				      e.preventDefault();
				    });
				 url  = $('#myurl').val() + '/transaction/search'  


<!--Autocomplete --> 
				 $( "#getTransactions" ).autocomplete({	
				 	max:10,
					//source:'quickSearch.php',
					
						 source: function (request, response) {
							 $.ajax({
								 url:  "{{route('transactions.autocompletetips',"")}}"+"/" + request.term ,
								 data: { term: request.term },
								 dataType: "json",
								 success: response,
								 error: function () {
									 response([]);
								 }
							 });
						 },
					select: function( event, ui ) {
						var add=url;		
					window.location.href = add+'/id/'+ui.item.id;
					}
    });
   <!--Date Search --> 
   console.log('we here')
   					$('#dateto').on('change', function (e) {
						  
   					value = $('#datefrom').val() + ',' + $('#dateto').val()
   					window.location.href = url +'/date/'+ value
   				});
				
			});
@endsection

