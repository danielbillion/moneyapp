
<table width = "100%"  width = "100%" border = "1" bordercolor = "#F0F0F0" cellspacing = 0 cellpadding = 0>
    <tr ><td >
        <table  align = "center"  >
            <header>
                <tr><td align="center" ><h2>{{str_limit($business->name, $limit = 15, $end = '...')}}</h2></td></tr>
                <tr><td align="center" > <h2>{{$business->address}}</h3></td></tr>
                <hr>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <i class="fa fa-phone-square" aria-hidden="true"></i>
                        <tr><td align="center"><strong>{{$business->phone}}{{$business->mobile}}<strong></td></tr>
                        <i class="fa fa-envelope" aria-hidden="true"></i>
                        <tr><td align="center"><strong> {{$business->email}}</strong></td></tr>,
                    </div>
                </div>
                <tr><td align="center">
                    <i class="fa fa-globe" aria-hidden="true"></i>
                    <span>{{$business->website}}</span>
                </td></tr>
            </header>
            <tr><td align="center">
                    <strong>{{$transaction->created_at->format('j-F-Y, H:i:s')}}</strong>
            </td></tr><hr>

            <section id = "senderreceiver">
               
                        <!-- --------sender------------->
                  <tr >  
                    <td><table cellspacing = "4" cellpadding = "4"><tr><td>
                            <h3><u>Sender</u></h3>
                            <tr>
                                <td>
                                    <strong>SENDER NAME</strong>
                                </td>
                                <td>
                                    <strong>{{str_limit($transaction->sender->name, $limit = 14, $end = '...')}}</strong>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>RESIDENCE</strong>
                                </td>
                                <td class="col-md-5 col-md-offset-1">
                                    <strong>{{str_limit($transaction->address, $limit = 30, $end = '...')}}</strong>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>POST CODE</strong>
                                </td>
                                <td>
                                    <strong>{{$transaction->sender->postcode}}</strong>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>PHONE NUMBER</strong>
                                </td>
                                <td>
                                    <strong>{{$transaction->sender->phone}}</strong>
                                </td>
                            </tr>
                         
                    </td></tr></table></td>
                   
                <!-- --------Receiver------------->
				<td><table cellspacing = "4" cellpadding = "4"><tr><td>
                        <h3><u>Receiver</u></h3>
                        <tr>
                            <td>
                                <strong> NAME</strong>
                            </td>
                            <td>
                                <strong>{{str_limit($transaction->receiver->name, $limit = 14, $end = '...')}}</strong>
                            </td>
                        </tr>
                        <tr>
                                <td>
                                     <strong> PHONE No</strong>
                                 </td>
                                <td>
                                    <strong>{{$transaction->receiver->phone}}</strong>
                                </td>
                        </tr>
                        <tr>
                                <td>
                                     <strong>DESTINATION</strong>
                                </td>
                                 <td class="col-md-5 col-md-offset-1">
                                <strong>{{$transaction->currency->destination}}</strong>
                                </td>
                        </tr>
						<tr>
                                <td>
									-
								</td>
								<td>
									-
								</td>
                        </tr>
                        
                </td></tr></table></td>
            </tr>
            </section>
		
            <!--
            ========================== ===============! -->
            <section id = "transactionpayment">
                <div class="row">
        <tr>
            <td><table cellspacing = "4" cellpadding = "4"><tr><td>
                    <div class="text-left sender col-md-6">
                        <h3><u>Transaction</u></h3>
                         <tr>
                                <td>
									<strong>SENDING</strong>
								</td>
								<td class="col-md-5 col-md-offset-1">
									<strong>£
									{{number_format($transaction->amount,2)}}</strong>
								</td>
                        </tr>
                         <tr>
                                <td>
									<strong>NAIRA EQUIV</strong>
								</td>
								<td>
									<strong>&#8358;
										{{$transaction->localpayment}}
                                </td>
                         </tr>
                        </div>
                         <tr>
                                <td>
									<strong>COMMISSION</strong>
								</td>
								<td>
									<strong>£{{$transaction->commission}}</strong>
								</td>
                        </tr>
                         <tr>
                                <td>
									<strong>TOTAL AMOUNT</strong>
								</td>
                            <td>
                                <strong>£{{$transaction->total}}</strong>
                            </td>
                        </tr>
                         <tr>
                                <td>
									<strong>TRANSFER CODE</strong>
								</td>
								 <td>
									<strong>{{$transaction->receipt_number}}</strong>
								 </td>
                        </tr>
                         <tr>
                                <td>
									<strong>AGENT NAME</strong>
								</td>
								 <td>
									<strong>-</strong>
								</td>
                        </tr>
                        
                    </div>
                </td></tr></table></td>
               
                <td><table cellspacing = "4" cellpadding = "4"><tr><td>
            <!-- --------Payment------------->
                   
                        <h3><u>Payment</u></h3>
                         <tr>
                                <td>
									<strong> RATE</strong>
								</td>
                            <td>
                                <strong>£{{$transaction->exchange_rate}}</strong>
                            </td>
                        </tr>
                         <tr>
                                <td>
									<strong>BANK</strong>
								</td>
                            <td>
                                <strong>{{str_limit($transaction->receiver->bank, $limit = 12, $end = '...')}}</strong>
                            </td>
                        </tr>
                         <tr>
                                <td>
									<strong>TRANSFER MODE</strong>
								</td>
								<td>
									<strong>{{$transaction->receiver->transfer_type}}</strong>
								</td>
                        </tr>
                         <tr>
                                <td>
									<strong>IDENTITY</strong>
								</td>
								<td>
									{{$transaction->receiver->identity}}
								</td>
                        </tr>
                         <tr>
                                <td>
									<strong>ACCOUNT NO</strong>
								</td>
								<td>
									{{$transaction->receiver->account_number}}
								</td>
                        </tr>
						 <tr>
                                <td>
									-
								</td>
								<td>
									-
								</td>
                        </tr>
                     
                </td></tr></table></td>
            </tr>
            </section>
            <!-- End here
                ======================================-->
        </div>
        <p></p>
        </table>
    </td></tr>		
</table>
<p align="center">If you have any questions or remarks please reply to this email.

Best Regards
{{env('APP_NAME')}}</p>