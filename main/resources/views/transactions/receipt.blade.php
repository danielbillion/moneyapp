@extends('layouts.receipt_layout')
@section('content')
<div id="main" class="invoice">
	<header>
		@if($transaction->body)
			<small class = "text-danger blink_me">{{$transaction->body}}</small>
		@endif
		
		<h1>{{str_limit($business->name, $limit = 18, $end = '...')}}</h2>
		<h2>{{$business->address}}</h3>
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<i class="fa fa-phone-square" aria-hidden="true"></i>
				<strong>{{$business->phone}}{{$business->mobile}}<strong><br/>
				<i class="fa fa-envelope" aria-hidden="true"></i>
				<strong> {{$business->email}}</strong>,
			</div>
		</div>
		<h4>
			<i class="fa fa-globe" aria-hidden="true"></i>
			<span>{{$business->website}}</span>
		</h4>
	</header>
		<h5 class = "text-right">
			<strong>{{$transaction->created_at->format('j-F-Y, H:i:s')}}</strong></h5>
	<section id = "senderreceiver">
		<div class="row">
			<div class="text-left sender col-md-6">
				<h3>Sender</h3>
				<div class="row">
					<div class="col-md-5">
						<strong>SENDER NAME</strong>
					</div>
					<div class="col-md-5 col-md-offset-1">
						<strong>{{str_limit($transaction->sender->name, $limit = 19, $end = '...')}}</strong>
					</div>
				</div>
				<div class="row">
					<div class="col-md-5">
						<strong>RESIDENCE</strong>
					</div>
					<div class="col-md-5 col-md-offset-1">
						<strong>{{str_limit($transaction->address, $limit = 35, $end = '...')}}</strong>
					</div>
				</div>
				<div class="row">
					<div class="col-md-5">
						<strong>POST CODE</strong>
					</div>
					<div class="col-md-5 col-md-offset-1">
						<strong>{{$transaction->sender->postcode}}</strong>
					</div>
				</div>
				<div class="row">
					<div class="col-md-5">
						<strong>PHONE NUMBER</strong>
					</div>
					<div class="col-md-5 col-md-offset-1">
						<strong>{{$transaction->sender->phone}}</strong>
					</div>
				</div>
				
			</div>
	<!-- --------Receiver------------->
			<div class="text-left sender col-md-6">
				<h3>Receiver</h3>
				<div class="row">
					<div class="col-md-5">
						<strong> NAME</strong>
					</div>
					<div class="col-md-5 col-md-offset-1">
						<strong>{{str_limit($transaction->receiver->name, $limit = 19, $end = '...')}}</strong>
					</div>
				</div>
				<div class="row">
					<div class=" col-md-5">
						<strong> PHONE No</strong>
					</div>
					<div class="col-md-5 col-md-offset-1">
						<strong>{{$transaction->receiver->phone}}</strong>
					</div>
				</div>
				<div class="row">
					<div class="col-md-5">
						<strong>DESTINATION</strong>
					</div>
					<div class="col-md-5 col-md-offset-1">
						<strong>{{$transaction->currency->destination}}</strong>
					</div>
				</div>
			
				
			</div>
		</div>
	</section>

	<!--
	 ========================== ===============! -->
	<section id = "transactionpayment">
		<div class="row">
			<div class="text-left sender col-md-6">
				<h3>Transaction</h3>
				<div class="row">
					<div class="col-md-5">
						<strong>SENDING</strong>
					</div>
					<div class="col-md-5 col-md-offset-1">
						<strong>£
						{{number_format($transaction->amount,2)}}</strong>
					</div>
				</div>
				<div class="row">
					<div class="col-md-5">
						<strong>NAIRA EQUIV</strong>
					</div>
					<div class="col-md-5 col-md-offset-1">
						<strong>&#8358;
							{{$transaction->localpayment}}
						</strong>
					</div>
				</div>
				<div class="row">
					<div class="col-md-5">
						<strong>COMMISSION</strong>
					</div>
					<div class="col-md-5 col-md-offset-1">
						<strong>£{{$transaction->commission}}</strong>
					</div>
				</div>
				<div class="row">
					<div class="col-md-5">
						<strong>TOTAL AMOUNT</strong>
					</div>
					<div class="col-md-5 col-md-offset-1">
						<strong>£{{$transaction->total}}</strong>
					</div>
				</div>
				<div class="row">
					<div class="col-md-5">
						<strong>TRANSFER CODE</strong>
					</div>
					<div class="col-md-5 col-md-offset-1">
						<strong>{{$transaction->receipt_number}}</strong>
					</div>
				</div>
				<div class="row">
					<div class="col-md-5">
						<strong>AGENT NAME</strong>
					</div>
					<div class="col-md-5 col-md-offset-1">
						<strong>08765678</strong>
					</div>
				</div>
				
			</div>
	<!-- --------Payment------------->
			<div class="text-left sender col-md-6">
				<h3>Payment</h3>
				<div class="row">
					<div class="col-md-5">
						<strong> RATE</strong>
					</div>
					<div class="col-md-6 col-md-offset-1">
						<strong>£{{$transaction->exchange_rate}}</strong>
					</div>
				</div>
				<div class="row">
					<div class="col-md-5">
						<strong>BANK</strong>
					</div>
					<div class="col-md-6 col-md-offset-1">
						<strong>{{str_limit($transaction->receiver->bank, $limit = 12, $end = '...')}}</strong>
					</div>
				</div>
				<div class="row">
					<div class="col-md-5">
						<strong>TRANSFER MODE</strong>
					</div>
					<div class="col-md-6 col-md-offset-1">
						<strong>{{$transaction->receiver->transfer_type}}</strong>
					</div>
				</div>
				<div class="row">
					<div class="col-md-5">
						<strong>IDENTITY</strong>
					</div>
					<div class="col-md-6 col-md-offset-1">
						{{$transaction->receiver->identity}}
					</div>
				</div>
				<div class="row">
					<div class="col-md-5">
						<strong>ACCOUNT NO</strong>
					</div>
					<div class="col-md-6 col-md-offset-1">
						{{$transaction->receiver->account_number}}
					</div>
				</div>
				
			</div>
		</div>
	</section>
	<!-- End here
		======================================-->
</div>
<p></p>
		@if(App\Setting_transaction::orderBy('id', 'desc')->first()->two_copies === 1)
			<div id="duplicate"></div>
		@endif
<div class = "row">
			<div class="col-md-6">
				<button 
					class="btn btn-primary btn-block" id = "printInvoice"
					onclick="window.print();">PRINT</button>
			</div>
			<div class="col-md-6">
				<a 
					class="btn btn-danger btn-block"
					href={{URL::to('/')}}>
					Return Home</a>
			</div>
</div>
@stop


@section('script')	
	$("#main").clone().insertAfter("#duplicate:last");

	$('#printInvoice').click(function(){
            Popup($('.invoice')[0].outerHTML);
            function Popup(data) 
            {
                window.print();
                return true;
            }
        });
@stop

