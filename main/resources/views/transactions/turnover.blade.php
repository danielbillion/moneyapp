@extends('layouts.layout')
@section('content')
	
	
	
	<div class="row">
		<div class="col-md-12">
        <h6>Turnover</h6>
            <div class=" panel panel-primary">
                <table id="sort-table" class="table table-striped table-bordered tablesorter text-center">
                    <thead>
                                <tr class="bg-primary text-center">
                                    <th>No </th>
                                    <th>Year / Month <i class=""></i></th>
                                    <th>Volume  </th>
                                    <th>Total <i class=""></i></th>
                                    <th>Agent Commission</th>
                                    <th>Commission</th>
                                    
                                </tr>
                        </thead>
                        <tbody>
                    
                            @foreach($turnovers as $key => $turnover)
                                <tr>
                                    <td>{{$key + 1}}</td>
                                    <td><strong>{{ $turnover->year}} </strong>
                                     [ {{date("F", mktime(0, 0, 0, $turnover->month, 1))}} ]
                                    </td>
                                    <td>{{ $turnover->counted}} </td>
                                    <td>{{ $turnover->total}}</td>
                                    <td>{{ $turnover->agent_commission}}</td>
                                    <td>{{ $turnover->commission}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                </table>
            </div>
            <div class = "row">
                <div class="col-md-4 col-md-offset-3">
                        {{$turnovers->render()}}
                </div>
            </div>
			<canvas id="bar" ></canvas>
            <canvas id="doughnut" ></canvas>
			
	</div>
@endsection


@section('script')

var ctx = document.getElementById("bar").getContext('2d');
var ctx2 = document.getElementById("doughnut").getContext('2d');

        draw(ctx,'bar');
        draw(ctx2,'doughnut');
    function draw(ctx,type){
                    var myChart = new Chart(ctx, {
                type: type,
                data: {
                    labels: [{{$chart->label}}],
                    datasets: [{
                        label: 'Your Activites',
                        data: [{{$chart->data}}],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                }
            });
    }


   console.log('script running')

@endsection

