@extends('layouts.layout')
@section('content')
	 <div class="col-md-9 col-md-offset-1">
		<div class="panel panel-default">
			<div class=" panel-headingt">
				<h4>Transaction Status</h4>
			</div>
		<div class=" panel-body">
			{!! Form::model($transaction,['method'=>'PATCH','action'=>['TransactionsController@updatestatus',$transaction->id],'file'=>true]) !!}


				<div class="row">
					<div class="col-md-12">
<!--sender details--><div class="form-group">	
						{!! Form::label('Sender','Sender') !!}
						{!! Form::text('sender_name',null,['id'=>'no', 'class'=>'form-control','placeholder'=>'Receipt Number','readonly'=>'readonly']) !!}
						</div>
					</div>
				</div>
				<div class="row">
						<div class="col-md-12">
	<!--sender details--><div class="form-group">	
							{!! Form::label('Receipt','Receipt') !!}
							{!! Form::text('receipt_number',null,['id'=>'no', 'class'=>'form-control','placeholder'=>'No of Transactions','readonly'=>'readonly']) !!}
							</div>
						</div>
				</div>
				<div class="row">
						<div class="col-md-12">
	<!--Amount--><div class="form-group">	
							{!! Form::label('Amount','Amount') !!}
							{!! Form::text('amount',null,['id'=>'no', 'class'=>'form-control','placeholder'=>'No of Transactions','readonly'=>'readonly']) !!}
							</div>
						</div>
				</div>
				<div class="form-group">
							{!! Form::label('Status','Status') !!}<span class="essential">*</span>
							{!! Form::select('status',['paid' =>'paid','pending'=>'pending','suspended'=>'suspended'],null,['class'=>'form-control','placeholder'=>'Status','id'=>'Status']) !!}
							
				</div>
<!--Message-->
				<div class="form-group">
							{!! Form::label('Note','Note') !!}
							{!! Form::textarea('body',
							null,['class'=>'form-control','placeholder'=>'Note to Member','id'=>'body']) !!}
							
				</div>

				<hr>

				<div class="row">
						<div class="col-md-12">
	<p></p>
	<!--Authenticate--><div class="form-group text-center">	
							{!! Form::label('Password','Password') !!}<span class="essential">*</span>
							{!! Form::password('password',['id'=>'no', 'class'=>'form-control']) !!}
							</div>
						</div>
				</div>
				<div class="row">
							{!! Form::submit('Submit',['class'=>'btn btn-primary col-md-12 btn-block']) !!}
				</div>
			{!! Form::close() !!}

		</div>
		</div>
	</div>
@endsection