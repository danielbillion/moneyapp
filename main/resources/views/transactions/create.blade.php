@extends('layouts.layout')
@section('content')
	<style>
		.currency_commission,.currency_profit{
			display:none;
		}
	</style>
	<div class="panel panel-default">
		<div class="panel-heading">
			<div id="output1"></div>
			<h4>Send Money<i class="glyphicon glyphicon-send"></i></h4>
		</div>
	<div class="panel-body">
			
		{!! Form::open(['method'=>'POST', 'action'=>'TransactionsController@summary','file'=>true]) !!}
		{!! Form::hidden('myurl',URL::to('/'),['id'=>'myurl']) !!}
		{!! Form::hidden('sender_name',null,['id'=>'sender_name']) !!}
		{!! Form::hidden('receiver_name',null,['id'=>'receiver_name']) !!}
		{!! Form::hidden('note',null,['id'=>'note']) !!}
		{!! Form::hidden('identity',null,['id'=>'identity']) !!}
		{!! Form::hidden('type',@Auth::user()->type,['id'=>'user_type']) !!}
		{!! Form::hidden('currency_id',1,['id'=>'currency_id']) !!}
		{!! Form::hidden('currency_income','commission',['id'=>'currency_income']) !!}
		{!! Form::hidden('destination_currency','none',['id'=>'destination_currency']) !!}
		{!!Form::hidden('agent_commission',null,['id'=>'agent_commission']) !!}
		{!! Form::hidden('credit',null,['id'=>'note']) !!}
		{!! Form::hidden('user_name',@Auth::user()->name,['id'=>'user_name']) !!}
		{!! Form::hidden('user_id',@Auth::id(),['id'=>'user_id']) !!}
		{!!Form::hidden('receiver_id',$receiverId,['id'=>'receiver_id']) !!}
		{!! Form::hidden('sender_id',$senderId,['id'=>'sender_id']) !!}
							
							
							

				<div class="row">
					<div class="col-md-6 col-md-offset-3">
<!--sender details--><div class="form-group">	
						{!! Form::label('sender','Sender') !!}<span class="essential">*</span>
						{!! Form::select('senders',[],null,['class'=>'form-control','id'=>'senders']) !!}	
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<div class="form-group">
							{!! Form::label('SReceiver','Select Receiver') !!}<span class="essential">*</span>
							{!! Form::select('receivers',[],null,['class'=>'form-control','id'=>'receivers']) !!}
						</div>		
								
					</div>
				</div>
<!--Receivers--><div class="well">
					<div class="row">
						<div class="col-md-4 col-md-offset-1">
							<div class="form-group form-group-sm">
								{!! Form::label('Rfirstname',' Receiver First Name') !!}
								{!! Form::text('receiver_fname',null,['id'=>'receiver_fname', 'class'=>'form-control','placeholder'=>'Receiver First name','readonly'=>'readonly']) !!}
							</div>
							</div>
						<div class="col-md-4 col-md-offset-1">
							<div class="form-group form-group-sm">
								{!! Form::label('Rlastname','Receiver Last Name') !!}
								{!! Form::text('receiver_lname',null,['id'=>'receiver_lname', 'class'=>'form-control','placeholder'=>'Receiver Last name','readonly'=>'readonly']) !!}
							</div>
							</div>
					</div>
					<div class="row">
							<div class="col-md-4 col-md-offset-1">
								<div class="form-group form-group-sm">
									{!! Form::label('receiver_phone','Receiver Phone') !!}
									{!! Form::text('receiver_phone',null,['id'=>'receiver_phone', 'class'=>'form-control','placeholder'=>'Receiver phone','readonly'=>'readonly']) !!}
								</div>
							</div>
							<div class="col-md-4 col-md-offset-1">
								<div class="form-group form-group-sm">
									{!! Form::label('Transfer Option','Transfer Option') !!}
									{!! Form::text('transfer_type',null,['id'=>'Transfer_type', 'class'=>'form-control','placeholder'=>'transfer_type','readonly'=>'readonly']) !!}
								</div>
							</div>
					</div>
					<div class="row">
							<div class="col-md-4 col-md-offset-1">
								<div class="form-group form-group-sm">
									{!! Form::label('bank','Receiver Bank') !!}
									{!! Form::text('bank',null, ['class'=>'form-control','placeholder'=>'Receiver Bank','readonly'=>'readonly']) !!}

								</div>
							</div>
							<div class="col-md-4 col-md-offset-1">
								<div class="form-group form-group-sm">
									{!! Form::label('accountno','Receiver Account Number') !!}
									{!! Form::text('account_number',null,['id'=>'account_number', 'class'=>'form-control','placeholder'=>'Receiver Account number ','readonly'=>'readonly']) !!}
								</div>
							</div>
				</div>
			</div>
<!--Transaction operation here-->
					
				<div class="row">
					<div class="col-md-5 col-md-offset-3">
						<div class="form-group form-group-sm">
						{!! Form::label('Amount To Send','Amount To Send £',['id'=>'amount_label']) !!}<span class="essential">*</span>
						{!! Form::text('amount',null,['id'=>'amount','class'=>'form-control','placeholder'=>'Enter Amount','maxlength'=>'10']) !!}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-5 col-md-offset-3">
						<div class="form-group form-group-sm">
							{!! Form::label('A-C','Amount To Collect Naira &#8358;',['id'=>'local_label']) !!}<span class="essential">*</span>
							{!! Form::text('local_payment',null,['id'=>'local_payment','class'=>'form-control','placeholder'=>'Amount To Collect','maxlength'=>'13']) !!}
						</div>
					</div>
				</div>
				<div class="row currency_commission" >
					<div class="col-md-5 col-md-offset-3">
						<div class="form-group form-group-sm">
							{!! Form::label('Commission','Commission') !!}
							{!! Form::text('commission',null,['id'=>'commission','class'=>'form-control','placeholder'=>'Commission','readonly'=>'readonly']) !!}
						</div>
					</div>
				</div>
				<div class="row currency_commission">
					<div class="col-md-5 col-md-offset-3">
						<div class="form-group form-group-sm">
							{!! Form::label('Total','Total') !!}
							{!! Form::text('total',null,['id'=>'total','class'=>'form-control','placeholder'=>'Total','readonly'=>'readonly']) !!}
						</div>
					</div>
				</div>
				
				<div class="row currency_commission">
					<div class="col-md-5 col-md-offset-3">
						<div class="form-group form-group-sm">
							<div class="form-group form-group-sm">
							{!! Form::label('Exchange Rate','Exchange Rate') !!}
							{!! Form::text('exchange_rate',Auth::user()->rate()->rate,['id'=>'exchange_rate','class'=>'form-control','placeholder'=>'Exchange Rate','readonly'=>'readonly']) !!}
						</div>
						</div>
					</div>
				</div>
				<div class="row currency_profit">
					<div class="col-md-5 col-md-offset-3">
						<div class="form-group form-group-sm">
							<div class="form-group form-group-sm">
							{!! Form::label('Bou Rate','Bou Rate') !!}
							{!! Form::text('bou_rate',Auth::user()->rate()->bou_rate,['id'=>'bou_rate','class'=>'form-control','placeholder'=>' Rate','readonly'=>'readonly']) !!}
						</div>
						</div>
					</div>
				</div>
				<div class="row currency_profit">
					<div class="col-md-5 col-md-offset-3">
						<div class="form-group form-group-sm">
							<div class="form-group form-group-sm">
							{!! Form::label('Sold Rate','Sold Rate') !!}
							{!! Form::text('sold_rate',Auth::user()->rate()->sold_rate,['id'=>'sold_rate','class'=>'form-control','placeholder'=>'Exchange Rate','readonly'=>'readonly']) !!}
						</div>
						</div>
					</div>
				</div>
				<div class="row">
					{!! Form::submit('Submit',['class'=>'btn btn-primary 
									col-md-12 btn-block']) !!}
				</div>
				
			{!! Form::close() !!}
		</div>
		
	</div>	
		
@stop



@section('script')	

	$.getScript("{{asset('js/transaction/create.js')}}", function() {
   console.log('script running')
});
	
$.getScript("{{asset('js/transaction/converter.js')}}", function() {
   console.log('converter running')
});	
@stop