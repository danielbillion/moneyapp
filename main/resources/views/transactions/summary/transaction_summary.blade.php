
	@extends('layouts.layout')
@section('content')
<style>
	input{
		border:0;
		text-transform:uppercase;
	}
</style>

{!! Form::open(['method'=>'POST', 'action'=>'TransactionsController@store','file'=>true]) !!}
		{!! Form::hidden('myurl',URL::to('/'),['id'=>'myurl']) !!}
		{!! Form::hidden('note',$transaction['note'],['id'=>'note']) !!}
		{!! Form::hidden('identity',$transaction['identity'],['id'=>'identity']) !!}
		{!! Form::hidden('type',@Auth::user()->type,['id'=>'user_type']) !!}
		{!!Form::hidden('agent_commission',$transaction['agent_commission'],['id'=>'agent_commission']) !!}
		{!! Form::hidden('credit',$transaction['agent_commission'],['id'=>'note']) !!}
		{!! Form::hidden('user_name',@Auth::user()->name,['id'=>'user_name']) !!}
		{!! Form::hidden('user_id',@Auth::id(),['id'=>'user_id']) !!}
		{!!Form::hidden('receiver_id',$transaction['receiver_id'],['id'=>'receiver_id']) !!}
		{!! Form::hidden('sender_id',$transaction['sender_id'],['id'=>'sender_id']) !!}
		{!! Form::hidden('receiver_fname',$transaction['receiver_fname'],['id'=>'sender_id']) !!}
		{!! Form::hidden('receiver_lname',$transaction['receiver_lname'],['id'=>'sender_id']) !!}
		{!! Form::hidden('currency_id',$transaction['currency_id'],['id'=>'currency_id']) !!}
		{!! Form::hidden('currency_income',$transaction['currency_income'],['id'=>'currency_income']) !!}
		{!! Form::hidden('bou_rate',$transaction['bou_rate'],['id'=>'bou_rate']) !!}
		{!! Form::hidden('sold_rate',$transaction['sold_rate'],['id'=>'sold_rate']) !!}
		{!! Form::hidden('allow_payment',App\Setting_transaction::setting()->allow_payment,['id'=>'sold_rate']) !!}
		

	<div class="panel panel-danger col-md-10 col-md-offset-1" id = "summary" style = "display:show;text-transform:uppercase;line-height:2.5em;">
			<h4>Transaction Summary <i class="fa fa-shopping-cart fa-2x"></i></h4>
		<div class="panel-body">
				
					<div class="row ">
							<div class="col-md-4 col-md-offset-1">
								<strong>Sender Name</strong>
							</div>
						<div class="col-md-5 ">
							<strong>{!! Form::text('sender_name',$transaction['sender_name']) !!}</strong>
						</div>
					</div>
				
					<div class="row ">
							<div class="col-md-4 col-md-offset-1">
								<strong>Receiver Name</strong>
							</div>
						<div class="col-md-5 ">
							<strong>{!! Form::text('receiver_name',$transaction['receiver_name']) !!}</strong>
						</div>
					</div>
					<div class="row ">
							<div class="col-md-4 col-md-offset-1">
								<strong>Receiver Phone</strong>
							</div>
						<div class="col-md-5 ">
						<strong>{!! Form::text('receiver_phone',isset($transaction['receiver_phone'])?$transaction['receiver_phone']:'unavailable') !!}</strong>
						</div>
					</div>

						<div class="row ">
							<div class="col-md-4 col-md-offset-1">
								<strong>Bank Name</strong>
							</div>
						<div class="col-md-5 ">
							<strong>{!! Form::text('bank',$transaction['bank']) !!}</strong>
						</div>
					</div>

						<div class="row">
							<div class="col-md-4 col-md-offset-1">
								<strong>Account NUmber</strong>
							</div>
						<div class="col-md-5 ">
							<strong>{!! Form::text('account_number',$transaction['account_number']) !!}</strong>
						</div>
					</div>

						<div class="row">
							<div class="col-md-4 col-md-offset-1">
								<strong>Transfer Option</strong>
							</div>
						<div class="col-md-5 ">
							<strong>{!! Form::text('transfer_type',$transaction['transfer_type']) !!}</strong>
						</div>
					</div>
		<p/><p/>
					<div class="row ">
							<div class="col-md-4 col-md-offset-1">
								<strong>Sending</strong>
							</div>
						<div class="col-md-5 ">
							<strong>{!! Form::text('amount',$transaction['amount']) !!}</strong>
						</div>
					</div>

					<div class="row ">
							<div class="col-md-4 col-md-offset-1">
								<strong>Commission</strong>
							</div>
						<div class="col-md-5 ">
							<strong>{!! Form::text('commission',$transaction['commission']) !!}</strong>
						</div>
					</div>

					<div class="row ">
							<div class="col-md-4 col-md-offset-1">
								<strong>Local Currency</strong>
							</div>
						<div class="col-md-5 ">
							<strong>{!! Form::text('local_payment',$transaction['local_payment']) !!}</strong>
						</div>
					</div>

					<div class="row ">
							<div class="col-md-4 col-md-offset-1">
								<strong>Exchange Rate</strong>
							</div>
						<div class="col-md-5 ">
							<strong style = "display:{{$transaction['currency_income']==='commission'?'show':'none'}}">{!! Form::text('exchange_rate',$transaction['exchange_rate']) !!}</strong>
							<strong style = "display:{{$transaction['currency_income']==='profit'?'show':'none'}}">{!! Form::text('bou_rate',$transaction['bou_rate']) !!}</strong>
						</div>
					</div>

					<div class="row ">
							<div class="col-md-4 col-md-offset-1">
								<strong>Total</strong>
							</div>
						<div class="col-md-5 ">
							<strong>{!! Form::text('total',$transaction['total']) !!}</strong>
						</div>
					</div>
					<div class="row ">
							<div class="col-md-4 col-md-offset-1">
								<strong>Destination</strong>
							</div>
						<div class="col-md-5 ">
							<strong>{!! Form::text('destination_currency',$transaction['destination_currency']) !!}</strong>
						</div>
					</div>
						
						<div class="row ">
							<div class="col-md-4 col-md-offset-1">
							{!! Form::submit('Submit',['class'=>'btn btn-primary 
									col-md-12 btn-block']) !!}
							</div>
						<div class="col-md-5 ">
							<a href = {{URL::to('/')}} class="btn btn-success btn-block">Cancel</a>
						</div>
					</div>
			
		</div>
</div>	
@stop

