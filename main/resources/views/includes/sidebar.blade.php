
	{!! Form::hidden('myurl',URL::to('/'),['id'=>'myurl']) !!}
	     <div class = "sidebar"> 
		  	<div class="panel panel-primary">
				<div class="panel-heading">
					<p class="text-center text-uppercase "><strong>		Account:{{auth::user()->type }}</strong></p>
				</div>
	        </div>
	        <div class="panel-body">
	        	
				<div id="user">
					<p class="text-Capitalize text-center"> <strong >Todays Rate!</strong></p>
				</div>
				<div class="panel-border">
					<div id="quickinfo" class="well well-lg text-danger">
					@foreach(App\Currency::rates() as $rate)
					<div class="row blink_me">
						<strong class="col-md-2 col-md-offset-1">£1 </strong>
						<strong class="col-md-1">= </strong>
						<strong class="col-md-7">{{$rate->rate}} {{$rate->destination_symbol}} </strong>
					</div><br>
					@endforeach
								
					</div>
				</div>
			      <div class="list-group">
					<a href={{route('members.home')}} class="list-group-item active"  >
						<span style="color:white;"><i class="glyphicon glyphicon-home"></i> Home</span>
					</a>
					@if(Auth::user()->isAdmin())
						 <a href="{{route('members.index')}}" class="list-group-item"><i class="fa fa-users"></i> Members</a>
					@endif
					<a href="{{route('transactions.index')}}" class="list-group-item"><i class="fa fa-briefcase"></i> Transactions</a>

					 @if(Auth::user()->isAgent() || Auth::user()->isCustomer() )
						<a href="{{route('transactions.create')}}" class="list-group-item"><i class="glyphicon glyphicon-send"></i> Send Money </a>
					@endif
					 @if(Auth::user()->isAgent())
						<a href="{{route('senders.create')}}" class="list-group-item"><i class="glyphicon glyphicon-plus"></i> New Sender</a>
												
						<a href="{{route('receivers.create')}}" class="list-group-item"><i class="glyphicon glyphicon-tags"></i> New Receivers</a>

						<a href="{{route('senders.index')}}" class="list-group-item"><i class="glyphicon glyphicon-user"></i> Senders List</a>
						<a href="{{route('outstandings.commissions',[Auth::id(),'all'])}}" class="list-group-item"><i class="glyphicon glyphicon-user"></i> Commissions</a>
						<a href="{{route('outstandings.transactions',[Auth::id(),'all'])}}" class="list-group-item"><i class="glyphicon glyphicon-user"></i> Outstanding</a>
						
												
						
					@endif

					@if(Auth::user()->type == 'customer')
					 <a href="{{route('receivers.create')}}" class="list-group-item"><i class="glyphicon glyphicon-plus"></i> New Receiver</a>
									 	 
					 <a href="{{route('receivers.index')}}" class="list-group-item"><i class="glyphicon glyphicon-user"></i> Receivers</a>
									
					@endif
					
					@if(Auth::user()->isManager() || Auth::user()->isAdmin())	
						<a href="{{route('outstandings.index')}}" class="list-group-item"><i class="fa fa-briefcase"></i> Outstandings Balance</a>
					@endif		
					@if(Auth::user()->isAdmin())
						<a href="{{route('rate.index')}}" class="list-group-item"><i class="fa fa-unlock-alt"></i> Modify Rate</a>
						<a href="{{route('commission.index')}}" class="list-group-item"><i class="fa fa-money"></i> Modify Commission</a>
						<a href="{{route('bank.index')}}" class="list-group-item"><i class="fa fa-institution"></i> Banks</a>
						<a href="{{route('setup-business.index')}}" class="list-group-item"> Business Setting</a>
						<a href="{{route('setup-transaction.index')}}" class="list-group-item"> Transaction Setting</a>
						<a href="{{route('payments.index')}}" class="list-group-item"> Payments</a>
						<a href="{{route('transactions.turnover',Auth::id())}}" class="list-group-item"><i class="fa fa-briefcase"></i> Turnover</a>
					<!-- <a href="{{route('managerpayments.index')}}" class="list-group-item"><i class="fa fa-briefcase"></i> Managers Payments</a> -->
<!--N2UK-------------- -->
						<div class="dropdown">
							<button class="btn-primary btn-block"
										  id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										     Nigeria to UK Transfer
										    <span class="caret"></span>
							</button>
							<ul class="dropdown-menu" aria-labelledby="dLabel">

								 <a href="{{route('transactions_n2uk.index')}}" class="list-group-item"><i class="fa fa-institution"></i> Transaction</a>

								<a href="{{route('transactions_n2uk.transfer',['0','0'])}}" class="list-group-item"><i class="fa fa-institution"></i> New Transfer</a>

								<a href="{{route('customers_n2uk.index')}}" class="list-group-item"><i class="fa fa-institution"></i>  Customer</a>

								<a href={{route('customers_n2uk.create')}} class="list-group-item"><i class="fa fa-institution"></i> New Customer</a>

								<a href="{{route('rates_n2uk.index')}}" class="list-group-item"><i class="fa fa-institution"></i> Bou / Sold Rate</a>

								<a href="{{route('banks_n2uk.index')}}" class="list-group-item"><i class="fa fa-institution"></i> Receiver Bank</a>

								<!-- <a href="{{route('managerpayments.index')}}" class="list-group-item"><i class="fa fa-institution"></i> Search</a>

								 <a href="{{route('managerpayments.index')}}" class="list-group-item"><i class="fa fa-institution"></i> TurnOver</a> -->
								
							</ul>
						</div>
						@endif				 				 
		         </div>
		      </div>
	</div>