<nav class="navbar navbar-expand-lg navbar-light bg-primary">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<div id="logo_head">
						<img  src="{{asset('images/logo.png')}}" width="40px" height="40px> 
						<a class="navbar-brand" href="#"><strong> {{config('app.name')}}</strong></a>
					</div>
				</div>

				<div class="collapse navbar-collapse" >
					<ul class="nav navbar-nav" style = "color:#ffffff">
						<li><a  href={{route('members.home')}}><i class="fa fa-home fa-2x"></i></a></li>
						<li class="nav-item dropdown">
							@if(Auth::user()->isAgent())
								<a href="#" class=" dropdown-toggle" data-toggle="dropdown">Senders</a>
									<ul class="dropdown-menu">
										<li ><a href="{{route('senders.index')}}" class="dropdown-item">Senders List</a></li>
										<li><a href="{{route('senders.create')}}" class="dropdown-item">New Senders</a></li>
										<li ><a href="{{route('receivers.create')}}">New Receiver</a></li>
									</ul>
								@endif
							
							@if(Auth::user()->isCustomer())
								<a href="#"  dropdown-toggle" data-toggle="dropdown">Receivers</a>
									<ul class="dropdown-menu">
										<li ><a href="
										{{route('receivers.create')}}"
										 class="dropdown-item">New Receiver</a></li>
										<li><a href="
										{{route('receivers.sender',@Auth::id())}}"
											class="dropdown-item">View Receiver</a></li>
									</ul>
									
							@endif
						</li>
						@if(Auth::user()->isCustomer() || Auth::user()->isAgent())
							<li ><a href="{{route('transactions.create')}}">Send Money</a></li>
						@endif
						<li class="dropdown">
							<a href="#" class="dropdown-toggle"  data-toggle="dropdown">Transactions </a>
									
							<ul class="dropdown-menu">
									<li><a href={{route('transactions.index')}}>Previous Transaction</a></li>
									@if(Auth::user()->isAgent())
									<li><a href="{{route('outstandings.commissions',[Auth::id(),'all'])}}"> Commissions</a></li>
									<li><a href="{{route('outstandings.transactions',[Auth::id(),'all'])}}"> Outstanding</a></li>
									@endif
									
									<li><a href={{route('transactions.turnover',Auth::id())}}>Turnover</a></li>
									
							</ul>
						</li>
							@if(Auth::user()->isAdmin())
							<li class="dropdown">
								<a href="#" class="dropdown-toggle"  data-toggle="dropdown">Nigeria To UK </a>
								<ul class="dropdown-menu" >

									<li> <a href="{{route('transactions_n2uk.index')}}" > Transaction</a></li>

									<li><a href="{{route('transactions_n2uk.transfer',['0','0'])}}"> New Transfer</a></li>

									<li><a href="{{route('customers_n2uk.index')}}" > Customer</a></li>

									<li><a href={{route('customers_n2uk.create')}} > New Customer</a></li>

									<li><a href="{{route('rates_n2uk.index')}}" > Bou / Sold Rate</a></li>

									<li><a href="{{route('banks_n2uk.index')}}" > Receiver Bank</a></li>

							<!-- <li> <a href="{{route('managerpayments.index')}}" > Search</a></li>

							<li> <a href="{{route('managerpayments.index')}}" > TurnOver</a></li> -->
									
								</ul>
							</li>
						@endif				 				 
		         
				</ul>
				<small  style = "text-transform:capitalize; margin-top:14px;"> 
							Welcome {{str_limit(auth::user()->name,10)}}!</small>
					<ul class="nav navbar-nav navbar-right">
			@if(Auth::user()->type==='admin')
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">General</a>
								<ul class="dropdown-menu">
									<li><a href={{route('currencies.index')}}>Currency Setup</a></li>
									<li><a href={{route('outstandings.index')}}>Outstandings</a></li>
									<li><a href={{route('payments.index')}}> Payments</a></li>
									<li><a href={{route('rate.index')}}>Rate Setup</a></li>
									<li><a href={{route('commission.index')}}>Commission Setup</a></li>
									<li><a href={{route('bank.index')}}>Bank Setup</a></li>
									<li><a href={{route('setup-business.index')}}>Businss Setting</a></li>
									<li><a href={{route('setup-transaction.index')}}>Transaction Setting</a></li>
									<li><a href={{route('subscription.index')}}>My Subscription</a></li>
									<li><a href="{{route('transactions.turnover',Auth::id())}}">Turnover</a></li>
								</ul>
							</li>
					@endif	
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account</a>
								<ul class="dropdown-menu">
									<li><a href={{route('members.password',@Auth::id())}}>Modify Password</a></li>
									<li><a href={{route('members.edit',@Auth::id())}}>Modify Profile</a></li>
								</ul>
							</li>
						
						<li><a href={{ url('/logout') }}><i class="fa fa-lock fa-2x"></i></a></li>
					</ul>
		</div>

	</nav>

