<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php //echo binfo()['name1']; ?></title>
    <!-- Bootstrap core CSS -->
		
    
	 <link href="{{asset('images/fvr.png')}}" rel="shortcut icon" type="images/jpeg">
	 
    <!-- Custom styles for this template -->

	<link href="https://fonts.googleapis.com/css?family=Droid+Serif|Montserrat|PT+Serif|Roboto" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
	<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
	<link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">
	<link href="{{asset('css/receipt.css')}}" rel="stylesheet">  
  </head>
  <style>
	  .blink_me {
		animation: blinker 1s linear infinite;
	  }
  
 		@keyframes blinker {
		50% {
		  opacity: 0;
		}
	  }
	</style>

<body>
  
      <div class="container text-center">
	        <div class="row">
	        	<div class="col-md-12">
	        		<!-- Main Content -->
	        		@yield('content')
	        	</div>
	     
			</div>
       </div>
					

					

   
 <script src="{{asset('js/jquery-1.9.1.min.js')}}"></script>
<script src="{{asset('js/bootstrap.js')}}"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    
    
	 <script>
		@yield('script')
	</script>
	
</html>