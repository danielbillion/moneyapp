<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{config('app.name')}}</title>
    <!-- Bootstrap core CSS -->
	 <link href="{{asset('images/fvr.png')}}" rel="shortcut icon" type="images/jpeg"> 
    <!-- Custom styles for this template -->
	<link href="https://fonts.googleapis.com/css?family=Droid+Serif|Montserrat|PT+Serif|Roboto" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
	<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
	<link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">
	
    

	<link href="{{asset('css/googleaddresslookup.css')}}" rel="stylesheet">
	<link href="{{asset('css/font-awesome.css')}}" rel="stylesheet">
	<link href="{{asset('css/datepicker.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
	 <link href="{{asset('css/style.css')}}" rel="stylesheet">
	  
  </head>
<body>
    <!-- Navbar Header -->
		@include('includes.nav_header')
<!-- general message -->
		@include('includes.message')
<!-- Error messages -->
		@include('includes.form_error')

    
      <div class="container">
	        <div class="row">
	        	<div class="col-md-3 sidebar">
	 			 <!-- Side Bar -->
					@include('includes.sidebar')
	        	</div>
	        	<div class="col-md-9">
	        		<!-- Main Content -->
					@yield('content')
					@yield('summary')
	        	</div>
	     
			</div>
       </div>
					

					


		<!--Start of Tawk.to Script-->
		<script type="text/javascript">
		var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
		(function(){
		var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
		s1.async=true;
		s1.src='https://embed.tawk.to/5c0b86cc67f72462651f9e58/default';
		s1.charset='UTF-8';
		s1.setAttribute('crossorigin','*');
		s0.parentNode.insertBefore(s1,s0);
		})();
		</script>
		<!--End of Tawk.to Script-->



	<footer class="footer">
      <div class="container">
        <span class=""> <p>&copy;Copyright <?php //echo date('Y'); ?>, All Rights Reserved <a href="http://www.computing24x7.co.uk">COMPUTING24X7 LTD</a></p>.</span>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
  
   
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="{{asset('js/bootstrap-datepicker.js')}}"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="{{asset('js/jquery.tablesorter.js')}}"></script>
    <script src="{{asset('js/tablesorter.js')}}"></script>
     <script src="{{asset('js/googleaddresslookup.js')}}" ></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGi3_lQwfN86wd0Pw5TsQxkTcjPldlpiY&libraries=places&callback=initAutocomplete"
		async defer></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
		<script src="{{asset('node_modules/axios/dist/axios.js')}}" ></script>	
		
		
	
    
    
	 <script>
		@yield('script')
	</script>

	
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="{{asset('js/bootstrap-datepicker.js')}}"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="{{asset('js/jquery.tablesorter.js')}}"></script>
    <script src="{{asset('js/tablesorter.js')}}"></script>
     <script src="{{asset('js/googleaddresslookup.js')}}" ></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC7wCPAm3j79Sef5jdLiKpycI8kELZiNBM&libraries=places&callback=initAutocomplete"
		async defer></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
		<script src="{{asset('node_modules/axios/dist/axios.js')}}" ></script>	
   		

	 </body>
</html>