@extends('layouts.receipt_layout')
@section('content')
Hello <i>{{ $mail->receiver }}</i>,
<h4>{{ $mail->subject }}</h4>
 
<div class = "well well-lg">
    <p>{{ $mail->body }}</p>
</div>

 
Thank You,
<br/>
<i>{{ $mail->sender }}</i>
+
<div class="bg-primary">
    <p>© {{ now()->year }} {{env('MAIL_FROM_NAME')}}. All rights reserved.</p>
</div>
@stop