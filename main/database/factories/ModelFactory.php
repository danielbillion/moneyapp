<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
// $factory->define(App\User::class, function (Faker\Generator $faker) {
//     static $password;

//     return [
//         'name' => $faker->name,
//         'email' => $faker->unique()->safeEmail,
//         'password' => $password ?: $password = bcrypt('secret'),
//         'remember_token' => str_random(10),


//          'name' => $faker->name,
//         'fname' => $faker->name,
//         'lname' => $faker->name,
//         'mname' => $faker->name,
//         'phone' => $faker->phoneNumber,
//         'photo_id' =>$faker->randomDigit,
//         'address_id' =>$faker->randomDigit,
//         'mobile' => $faker->phoneNumber,
       
//         'dob' => $faker->phoneNumber,
//         'is_active' => $faker->numberBetween(1,0),
//         'title' => $faker->sentence(7,11),
//         'email' => $faker->unique()->safeEmail,
//         'password' => $password ?: $password = bcrypt('secret'),
//         'remember_token' => str_random(10),
//     ];
// });

$factory->define(App\N2UK_Customer::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'fname' => $faker->name,
        'lname' => $faker->name,
        'mname' => $faker->name,
        'phone' => $faker->phoneNumber,
        'photo_id' =>$faker->randomDigit,
        'address_id' =>$faker->randomDigit,
        'mobile' => $faker->phoneNumber,
       'postcode' => $faker->sentence(7,11),
        'dob' => $faker->phoneNumber,
        'title' => $faker->sentence(7,11),
        'phone_id' => $faker->randomDigit,
        'password' => $password ?: $password = bcrypt('secret'),
       
    ];
});
