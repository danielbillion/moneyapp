<?php

use Illuminate\Database\Seeder;

class N2UKCustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('laravel__n2uk_customers')->insert([
            'name' => str_random(10),
            'email' => str_random(10).'@gmail.com',
            
        ]);
    }
}
