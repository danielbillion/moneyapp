<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHasAgentPaysCommission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laravel_agent_pays_transaction', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transaction_id');
            $table->integer('agent_id');
            $table->integer('manager_id');
            $table->integer('admin_id');
            $table->integer('paid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laravel_agent_pays_commission');
    }
}
