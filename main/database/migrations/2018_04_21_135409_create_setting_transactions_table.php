<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laravel_setting_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('two_copies')->default(1);
            $table->integer('allow_credit')->default(0);
            $table->integer('allow_calculator')->default(0);;
            $table->integer('agentname_onreceipt')->default(0);;
            $table->integer('cap_transaction')->default(0);;
            $table->double('max_transaction')->default(0);
            $table->integer('allow_sms')->default(0);;
            $table->string('sms_email');
            $table->string('sms_hash');
            $table->integer('allow_payment')->default(0);;
            $table->double('max_payment')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laravel_setting_transactions');
    }
}
