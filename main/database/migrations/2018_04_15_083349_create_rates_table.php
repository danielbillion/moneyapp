<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laravel_rates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rate')->default('0');
            $table->string('bou_rate')->default('0');
            $table->string('sold_rate')->default('0');
            $table->integer('user_id')->index()->default('0');
            $table->integer('currency_id')->index()->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laravel_rates');
    }
}
