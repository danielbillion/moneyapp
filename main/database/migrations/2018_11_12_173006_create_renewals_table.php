<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRenewalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laravel_renewals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id')->default('null');
            $table->string('amount')->default('null');
            $table->dateTime('next_payment');
            $table->string('payment-mode')->default('null');
            $table->integer('completed')->default('0');
            $table->integer('status')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laravel_renewals');
    }
}
