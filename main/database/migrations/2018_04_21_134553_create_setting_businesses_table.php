<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingBusinessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laravel_setting_businesses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slogan');
            $table->string('abbreviation');
            $table->string('email');
            $table->string('email2');
            $table->string('phone');
            $table->string('mobile');
            $table->string('address');
            $table->string('postcode');
            $table->string('website');
            $table->string('type');
            $table->string('status')->default('active');
            $table->integer('active_notice')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laravel_setting_businesses');
    }
}
