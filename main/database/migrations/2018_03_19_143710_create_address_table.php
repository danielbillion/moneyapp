<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laravel_addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id')->default('null');;
             $table->string('agent_customer_id')->default('null');;
            $table->string('postcode');
            $table->string('town');
            $table->string('county');
            $table->string('country');
            $table->string('address');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laravel_address');
    }
}
