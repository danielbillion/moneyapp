<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laravel_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('receipt_number');
            $table->string('type');
            $table->string('user_id');
            $table->string('sender_id');
            $table->string('receiver_id');
            $table->string('receiver_phone');
            $table->double('amount');
            $table->double('commission');
            $table->double('agent_commission');
            $table->double('exchange_rate');
            $table->integer('currency_id')->index()->default('1');
            $table->string('currency_income')->default('commission');;
            $table->string('bou_rate')->default('0');;
            $table->string('sold_rate')->default('0');;
            $table->string('note')->default('unavailable');
            $table->string('status')->default('pending');
            $table->string('agent_payment_id')->default('none');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laravel_transaction');
    }
}
