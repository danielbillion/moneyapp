<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentManagersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laravel_payment_managers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('manager_id');
            $table->string('payment_type')->default('remittance');
            $table->string('manager_name');
            $table->double('amount');
            $table->double('balance');
            $table->double('total');
            $table->integer('fully_paid')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laravel_payment_managers');
    }
}
