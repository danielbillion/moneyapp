<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laravel_phones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('phoneable_id');
            $table->integer('mobile')->nullable();
            $table->integer('phone')->nullable();
            $table->string('phoneable_type');
            $table->string('code')->nullable();
            $table->integer('is_valid')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laravel_phones');
    }
}
