<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateN2UKTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laravel__n2uk_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('transfer_code');
            $table->string('user_id');
            $table->string('sender_id');
            $table->string('receiver_id');
            $table->string('receiver_phone');
            $table->string('sender_phone');
            $table->double('amount_pounds');
            $table->double('amount_naira');
            $table->double('bou_rate');
            $table->double('sold_rate');
            $table->double('margin');
            $table->string('transfer_type');
            $table->string('identity');
            $table->string('uk_bank');
            $table->string('uk_account_no');
            $table->string('uk_sort_code');
            $table->string('ng_account_name');
            $table->string('ng_bank');
            $table->string('ng_account_no');
           
            $table->string('note')->default('unavailable');
            $table->string('status')->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laravel__n2uk_transactions');
    }
}
