$(function(){
	//sender ajax request
		url  = $('#myurl').val() + '/transactions/'  //url from hidden input field
			$('#senders').on('change', function (e) {
				
				    var optionSelected = $("option:selected", this); //not much relevance
				    var valueSelected = this.value;
				    $('#receivers').find('option').not(':nth-child(1), :nth-child(2)').remove();
		<!-- ajax call-->
				    $.ajax({
				    type:'GET',
				    url: url + 'sender/'+valueSelected +'/receivers',
				    data:valueSelected,
				    error:function(request, status, error){
				    	console.log(error)
					},
					success:function(response){
						console.log(response)
						$.each(response,function(receiver){
						    $('#receivers')
						    .append($("<option></option>")
			                    .attr("value",response[receiver].id)
			                    .text(response[receiver].fname + " "+ response[receiver].lname)); 
						});
					},

					})
			})

//Receiver ajax request
			$('#receivers').on('change', function (e) {
				    var optionSelected = $("option:selected", this); //not much relevance
				    var receiverSelected = this.value;
				    // '2' meaning new receiver
		<!-- ajax call-->
				if(receiverSelected == 2){
						 	$('#receiver_fname').val("").removeAttr("readonly");
						 	$('#receiver_lname').val("").removeAttr("readonly");
						  	$('#receiver_phone').val("").removeAttr("readonly")
							$('#bank').val("").removeAttr("readonly")
							$('#Transfer_type').replaceWith("<select name = 'transfer_type'  id = 'Transfer_type' class='form-control'><option>Pick Up</option><option>Bank Account</option></select>")
							$('#account_number').val("").removeAttr("readonly")
				}
					else{
						$.ajax({
						    type:'GET',
						    url: url + 'receiver/' + receiverSelected,
						    data:receiverSelected,
						    error:function(request, status, error){
						    	console.log(error)
							},
							success:function(response){
								console.log(response)
								$('#receiver_fname').val(response.fname).attr('readonly', true)
								$('#receiver_lname').val(response.lname).attr('readonly', true)
								$('#receiver_phone').val(response.phone).attr('readonly', true)
								$('#bank').val(response.bank).attr('readonly', true)
								$('#Transfer_type').val(response.transfer_type).attr('readonly', true)
								$('#account_number').val(response.account_number).attr('readonly', true)
								$('#note').val('unavailable')
							
			/* jquery, fill  the hidden inputvalue
					=========================================== */
								$('#receiver_name').val($("#receivers option:selected").text());
								$('#sender_name').val($("#senders option:selected").text());
								$('#identity').val(identity=response.identity==null?'None':response.identity)
								$('#note').val("unavailable	")
		//fill text-boxes
								},
							})

					}
						    

			})

//Transactions


			$('#amount').on('keyup', function() {
					amount  = parseFloat($('#amount').val())
					rate  = parseFloat($('#exchange_rate').val())
					 option  = String($('input[type=radio]:checked').val())
					amount = option =='pounds'?amount:parseFloat(amount/rate)
					
					console.log(`option = ${option}, amount = ${amount}`)	
						$.ajax({
							 type:'GET',
								    url: url + amount +'/findratecommission',
								    data:amount,
								    error:function(request, status, error){
								    	console.log(error)
									},
									success:function(response){
										rate = response.rate
										console.log(response)
										commission =parseFloat(response.commission.value)
										agent_commission =parseFloat(response.commission.agent_quota)/100 * commission
										$('#agent_commission').val(agent_commission)
										commission = commission > 1?commission:commission*amount //check decimal
										
										if(option == 'pounds'){
											$('#commission').val(commission)
											$('#total').val(commission + parseFloat(amount) )
											$('#local_payment').val(parseFloat(rate) * parseFloat(amount) )
										}else if (option =='naira') {
									//change dom identification of input,label element
											$('#amount').attr('name', 'local_payment');
											$('#local_payment').attr('name', 'amount');
											$('#local_label').text('Amount To Send');
											$('#amount_label').text('Local Payout');
											

											$('#commission').val(commission)
											$('#total').val(amount + commission  )
											$('#local_payment').val(amount)
											}
											
									},

						})
       
   			 });
    
	});

