$(function(){
 //url from hidden input field
		url  = $('#myurl').val() + '/transactions/'		 

/* Sender List / Empty
		====================== */
			if(parseInt($('#sender_id').val()) === 0){
					$.ajax({
					method:'get',
					url:$('#myurl').val() + '/api/n2uk/customers',
					success:function(response){
					$('#senders').append($("<option>Select Sender</option>"))
						$.each(response,function(sender){
							$('#senders').append($("<option></option>")
											.attr("value",response[sender].id)
											.text(response[sender].name))
						})
					},
					error:function(error){
						console.log(error)
					}
				})
			}
			if(parseInt($('#sender_id').val()) > 0){
			 	findReceivers($('#sender_id').val())
			}

			if(parseInt($('#receiver_id').val()) > 0){
			 	findOneReceiver($('#receiver_id').val())
			}
/* Sender onSelect
====================== */
			$('#senders').on('change', function(e) {
				    var optionSelected = $("option:selected", this); //not much relevance
				    var senderSelected = this.value;
				    findReceivers(senderSelected)
			})

	

	/* Receiver onSelect
	====================== */
			$('#receivers').on('change', function (e) {
				    var optionSelected = $("option:selected", this); //not much relevance
				    var receiverSelected = this.value;
				    findOneReceiver(receiverSelected)
				     
			})

		/* Transaction onkeyup
		====================== */


				$('#amount_pounds').on('keyup', function() {
					
					amount_pounds  = parseFloat($('#amount_pounds').val())
					bou_rate  = parseFloat($('#bouRate').val())
					$('#amount_naira').val((amount_pounds * bou_rate).toFixed(2))
					
				})

				$('#amount_naira').on('keyup', function() {
					amount_naira  = parseFloat($('#amount_naira').val())
					bou_rate  = parseFloat($('#bouRate').val())
					$('#amount_pounds').val((amount_naira / bou_rate).toFixed(2))
					
				})
	
    
		})






/* --------------------METHODS
===================================================================== */


//SenderList

var senderLists = function(){
			
}

/* OnSelectSender Method
====================== */

	 function findReceivers (senderSelected){
				<!-- ajax call-->
				    $.ajax({
				    type:'GET',
				    url:$('#myurl').val() + "/api/n2uk/customer/" +senderSelected +'/receivers',
				    error:function(request, status, error){
				    	console.log(error)
					},
					success:function(response){
						$('#receivers').find('option').remove();
						$('#sender_id').val(senderSelected)
						$('#receivers').append($("<option value='0'>New Receiver</option>"))
						console.log(response)
						$.each(response,function(receiver){
						    $('#receivers')
						    .append($("<option></option>")
			                    .attr("value",response[receiver].id)
			                    .text(response[receiver].fname + " "+ response[receiver].lname)); 
						});
					},

					})
			}


/* OnSelectReceiver Method
====================== */

 function findOneReceiver(receiverSelected){
	<!-- ajax call-->
				if(receiverSelected == 0){
					$('#receiver_fname').val("").removeAttr("readonly");
					$('#receiver_lname').val("").removeAttr("readonly");
					$('#receiver_phone').val("").removeAttr("readonly")
					$('#transfer_type').val("").removeAttr("readonly")
					$('#uk_bank').val("").removeAttr("readonly")
					$('#uk_account_no').val("").removeAttr("readonly")
					$('#uk_sort_code').val("").removeAttr("readonly")
					$('#ng_bank').val("").removeAttr("readonly")
					$('#ng_account_name').val("").removeAttr("readonly")
					$('#ng_account_no').val("").removeAttr("readonly")
					$('#bank').val("").removeAttr("readonly")
					$('#Transfer_type').replaceWith("<select name = 'transfer_type'  id = 'Transfer_type' class='form-control'><option>Pick Up</option><option>Bank Account</option></select>")
							$('#account_number').val("").removeAttr("readonly")
				}
					else{
						$.ajax({
						    type:'GET',
						    url: $('#myurl').val() 
						    	+ "/api/n2uk/receiver/"+ receiverSelected,
						    data:receiverSelected,
						error:function(request, status, error){
						    	console.log(error)
							},
						success:function(response){
								console.log(response)
						$('#receiver_id').val(receiverSelected)
						$('#receiver_fname').val(response.fname)
											.attr('readonly', true)
						$('#receiver_lname').val(response.lname)
											.attr('readonly', true)
						$('#receiver_phone').val(response.phone)
											.attr('readonly', true)
						$('#uk_bank').val(response.uk_bank)
									.attr('readonly', true)
						$('#uk_sort_code').val(response.uk_sort_code)
											.attr('readonly', true)
						$('#uk_account_no').val(response.uk_account_no)
											.attr('readonly', true)
						$('#Transfer_type').val(response.transfer_type)
											.attr('readonly', true)
						$('#ng_bank').val(response.ng_bank)
									.attr('readonly', true)
						$('#ng_account_no').val(response.ng_account_no)
									.attr('readonly', true)
						$('#ng_account_name').val(response.ng_account_name)
									.attr('readonly', true)
							
/* jquery, fill  the hidden inputvalue
					=========================================== */
								$('#receiver_name').val($("#receivers option:selected").text());
								$('#sender_name').val($("#senders option:selected").text());
								$('#identity').val(identity=response.identity==null?'None':response.identity)
								$('#note').val("unavailable	")
		//fill text-boxes
								},
							})

					}
}