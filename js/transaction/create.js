$(function(){
 //url from hidden input field
		url  = $('#myurl').val() + '/transactions/'	
		api_url  = $('#myurl').val() + '/api/'	
		user_type  = $('#user_type').val() 
		user_id  = $('#user_id').val() 
		rate  = parseFloat($('#exchange_rate').val() )
	

	
/* Sender List / Sender List Empty
		====================== */
		if(user_type ==='agent'){
				if(parseInt($('#sender_id').val()) === 0){
					sendersList($('#user_id').val());
				}
			if(parseInt($('#sender_id').val()) > 0){
				sender($('#sender_id').val());
				findReceivers($('#sender_id').val())
				findSenderCurrencyId($('#sender_id').val())
			}


			if(parseInt($('#receiver_id').val()) > 0){
				findOneReceiver($('#receiver_id').val())
			}

		}else{
					$('#senders').append($("<option></option>")
						.attr("value",$('#user_id').val())
						.text($('#user_name').val())
					)
					findReceivers($('#user_id').val())
					
					findSenderCurrencyId($('#user_id').val())

					if(parseInt($('#receiver_id').val()) > 0){
						findOneReceiver($('#receiver_id').val())
					}

		}
					
/* Sender onSelect
====================== */
			$('#senders').on('change', function(e) {
				    var optionSelected = $("option:selected", this); //not much relevance
				    var senderSelected = this.value;
					findReceivers(senderSelected)
					findSenderCurrencyId(senderSelected)
			})

	

	/* Receiver onSelect
	====================== */
			$('#receivers').on('change', function (e) {
				    var optionSelected = $("option:selected", this); //not much relevance
				    var receiverSelected = this.value;
				    findOneReceiver(receiverSelected)
				     
			})

	
	
    
		})






/* --------------------METHODS
===================================================================== */


//SendersList

		function sendersList(userId){
			$.ajax({
				method:'get',
				url:api_url + userId + '/senders',
				success:function(response){
					$('#senders').append(
						$("<option>Select Sender</option>"))
						$.each(response,function(sender){
							$('#senders').append($("<option></option>")
								.attr("value",response[sender].id)
								.text(response[sender].name))
								})
					},
				error:function(error){
						console.log(error)
						}
				})
			}
	function sender(senderId){
			$.ajax({
				method:'get',
				url:api_url +  'senders/' + senderId ,
				success:function(response){
						$.each(response,function(sender){
							$('#senders').append($("<option></option>")
								.attr("value",response[sender].id)
								.text(response[sender].name.toUpperCase()))
								})
					},
				error:function(error){
						console.log(error)
						}
				})
			}


/* Currency_id for senders

====================== */

	function findSenderCurrencyId(id){
		$.ajax({
			type:'GET',
			url:$('#myurl').val() + '/currency/sender/' + id,
			error:function(error){
				console.log(error)
			},
			success:function(response){
				fillRates(response.id)  //fill rate input box
				$('#currency_id').val(response.id)
				$('#currency_income').val(response.income_category)
				$('#local_label').text("Amount To Collect " + response.destination_symbol)
				$('#destination_currency').val( response.destination)
				if(response.income_category === 'profit'){
					$('.currency_profit').show()
					$('.currency_commission').hide()
				}else{
					$('.currency_commission').show()
					$('.currency_profit').hide()
				}
			},
		})
	}

	function fillRates(currency_id){
		$.ajax({
			type:'GET',
				url:api_url + 'currency/'+ currency_id + '/rate',
				error:function(error){console.log(error)},
				success:function(response){
					$('#rate').val(response.rate)
					$('#bou_rate').val(response.bou_rate)
					$('#sold_rate').val(response.sold_rate)
				}
		})
	}

/* OnSelectSender Method
====================== */

	 function findReceivers (senderSelected){
	 		
				    $.ajax({
				    type:'GET',
				    url:api_url + senderSelected + '/' + user_type + '/receivers',
				    error:function(request, status, error){
				    	console.log(error)
					},
					success:function(response){
						$('#receivers').find('option').remove();
						$('#sender_id').val(senderSelected)
						$('#receivers').append($("<option value='0'>New Receiver</option>"))
						console.log(response)
						$.each(response,function(receiver){
						    $('#receivers')
						    .append($("<option></option>")
			                    .attr("value",response[receiver].id)
			                    .text(response[receiver].fname + " "+ response[receiver].lname)); 
						});
					},

					})
			}


/* OnSelectReceiver Method
====================== */

 function findOneReceiver(receiverSelected){

				if(receiverSelected == 0){
					$('#receiver_fname').val("").removeAttr("readonly");
					$('#receiver_lname').val("").removeAttr("readonly");
					$('#receiver_phone').val("").removeAttr("readonly")
					$('#transfer_type').val("").removeAttr("readonly")
					$('#bank').val("").removeAttr("readonly")
					$('#account_number').val("").removeAttr("readonly")
					banks()
													
							

					$('#Transfer_type').replaceWith("<select name = 'transfer_type'  id = 'Transfer_type' class='form-control'><option>Pick Up</option><option>Bank Account</option></select>")
							$('#account_number').val("").removeAttr("readonly")
				}
					else{
						$.ajax({
						    type:'GET',
						    url:api_url + "receivers/"+ receiverSelected,
						    data:receiverSelected,
						error:function(request, status, error){
						    	console.log(error)
							},
						success:function(response){
								console.log(response)
						$('#receiver_id').val(receiverSelected)
						$('#receiver_fname').val(response.fname)
											.attr('readonly', true)
						$('#receiver_lname').val(response.lname)
											.attr('readonly', true)
						$('#receiver_phone').val(response.phone)
											.attr('readonly', true)
						$('#bank').val(response.bank)
									.attr('readonly', true)
						$('#account_number').val(response.account_number)
									.attr('readonly', true)
						$('#identity').val(response.identity)
											.attr('readonly', true)
						$('#Transfer_type').val(response.transfer_type)
											.attr('readonly', true)
						
						
							
/* jquery, fill  the hidden inputvalue
					=========================================== */
								$('#receiver_name').val($("#receivers option:selected").text());
								$('#sender_name').val($("#senders option:selected").text());
								$('#identity').val(identity=response.identity==null?'None':response.identity)
								$('#note').val("unavailable	")
		//fill text-boxes
								},
							})

					}
}


function banks(){
	list = $('#bank').remove().append()
				$.each(['joe','ema'],function(sender){
							$(this).append($("<option></option>")
							.attr("value",sender)
							.text(sender))
				})
							
		   
	

												return list
}
	