	/* Transaction onkeyup
		====================== */
       
        

        $('#amount').on('keyup', function() {
                 amount = parseFloat($('#amount').val())
           if($('#currency_income').val() === 'commission'){
               
                findCommission(amount).then(response=>{
                    $('#local_payment').val(rate * amount)
                    fillTransaction(response.data.value,response.data.agent_quota,amount)
                })	
           }else{
             $('#local_payment').val(parseFloat($('#bou_rate').val())* amount)
                fillTransaction(0,0,0)
            }
                             
        })

        $('#local_payment').on('keyup', function() {
            amount = parseFloat($('#local_payment').val()) / rate

                    findCommission(amount).then(response=>{
                        console.log(response)
                        $('#amount').val( amount)
                        fillTransaction(response.data.value,response.data.agent_quota,amount)
                    })
            
        })

        	/* Methods- Transactions
		====================== */
		function findCommission (amount){
            currencyId =$('#currency_id').val()
            
            return  axios.get(api_url + 'user/' + user_id + '/currency/' + currencyId + '/amount/' + amount + '/commission')
             .catch(err=>console.log('error found',err.message))          
   }
        function fillTransaction(commission,agentQuota,amount){
            console.log('agent_quota=', agentQuota)
            currencyIncome= $('#currency_income').val()
           
            if(currencyIncome === 'commission'){
                 $('#commission').val(commission)
                 $('#total').val(+commission +  +amount)
                 $('#bou_rate').val(0)
                 $('#sold_rate').val(0)

                agent_commission =parseFloat(agentQuota)/100 * commission
                $('#agent_commission').val(agent_commission)
            }else{
                $('#commission').val(0)
                $('#exchange_rate').val(0) 
                $('#total').val($('#local_payment').val())
                $('#agent_commission').val(0)
            }
            
   }

        function banks (amount){
            return  axios.get(api_url + 'banks')
                .catch(err=>console.log('error found',err.message))
          
}