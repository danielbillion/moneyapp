let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');


   mix.styles([
    '../css/style.css',
    '../css/bootstrap.css',
    '../css/font-awesome.css',
    '../css/receipt.css',
    '../css/googleaddresslookup.css',
    '../css/datepicker.css',
    '../css/sb-admin-2.css'

], '../css/libs.css');

   

mix.scripts([
    'resources/assets/js/jquery.js',
    'resources/assets/js/bootstrap.js',
    'resources/assets/js/sb-admin-2.js',
    'resources/assets/js/jquery.js',
    'resources/assets/js/libs/scripts.js'

], 'public/js/libs.js');
