Alter table laravel_payments RENAME TO laravel_partpayments;








-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 15, 2019 at 08:43 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `laravel_money`
--

-- --------------------------------------------------------

--
-- Table structure for table `laravel_messages`
--

CREATE TABLE `laravel_messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `messageable_id` int(11) NOT NULL,
  `messageable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `body` varchar(800) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `laravel_payments`
--

CREATE TABLE `laravel_payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(191) NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paymentable_id` int(11) NOT NULL,
  `paymentable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ref` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double NOT NULL,
  `for` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'cash',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `laravel_phones`
--

CREATE TABLE `laravel_phones` (
  `id` int(10) UNSIGNED NOT NULL,
  `phoneable_id` int(100) NOT NULL,
  `phoneable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_valid` int(11) DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `laravel_subscriptions`
--

CREATE TABLE `laravel_subscriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `start_at` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ref` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double NOT NULL,
  `others` double DEFAULT NULL,
  `payment_mode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'cash',
  `paid` int(11) NOT NULL,
  `months_duration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notice` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `laravel_subscriptions`
--

INSERT INTO `laravel_subscriptions` (`id`, `user_id`, `start_at`, `ref`, `amount`, `others`, `payment_mode`, `paid`, `months_duration`, `notice`, `created_at`, `updated_at`) VALUES
(1, 0, '15-01-2019', 'YKGlAG2019', 350, 0, 'cash', 0, '24', 0, '2019-01-15 12:06:05', '2019-01-15 12:06:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `laravel_messages`
--
ALTER TABLE `laravel_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laravel_payments`
--
ALTER TABLE `laravel_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laravel_phones`
--
ALTER TABLE `laravel_phones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laravel_subscriptions`
--
ALTER TABLE `laravel_subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `laravel_messages`
--
ALTER TABLE `laravel_messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `laravel_payments`
--
ALTER TABLE `laravel_payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `laravel_phones`
--
ALTER TABLE `laravel_phones`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `laravel_subscriptions`
--
ALTER TABLE `laravel_subscriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;
















ALTER TABLE `laravel_agent_customers` CHANGE `phone` `phone` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL;
ALTER TABLE `laravel_agent_customers` CHANGE `mname` `mname` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL;
ALTER TABLE `laravel_agent_customers` CHANGE `email` `email` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL;

ALTER TABLE `laravel_users` CHANGE `mname` `mname` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL;
ALTER TABLE `laravel_users` CHANGE `phone` `phone` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL;

ALTER TABLE `laravel_receivers` CHANGE `transfer_type` `transfer_type` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL;

INSERT INTO laravel_phones(phoneable_id,phoneable_type,mobile,phone)
    SELECT   id,'App\\User',mobile,phone FROM laravel_users;

ALTER TABLE `laravel_setting_transactions` CHANGE `allow_calculator` `send_email` INT(11) NOT NULL DEFAULT '0';

INSERT INTO laravel_phones(phoneable_id,phoneable_type,mobile,phone)
    SELECT id,'App\\AgentCustomer',mobile,phone FROM laravel_agent_customers;