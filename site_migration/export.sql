-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 09, 2018 at 10:10 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `laravel_money`
--

-- --------------------------------------------------------

--
-- Table structure for table `authentication_log`
--

CREATE TABLE `authentication_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `authenticatable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `authenticatable_id` bigint(20) UNSIGNED NOT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `login_at` timestamp NULL DEFAULT NULL,
  `logout_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `laravel_addresses`
--

CREATE TABLE `laravel_addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `agent_customer_id` int(11) DEFAULT NULL,
  `postcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `town` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `county` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `laravel_agent_customers`
--

CREATE TABLE `laravel_agent_customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `old_id` int(10) unsigned NOT NULL,
`agid` int(10) unsigned NOT NULL,
 `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
`town` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
`county` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
`country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
`postcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_id` int(11) NOT NULL DEFAULT '1',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_id` int(11) NOT NULL,
  `photo_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `laravel_apis`
--

CREATE TABLE `laravel_apis` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'none',
  `hash` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'none',
  `key1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'none',
  `key2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'none',
  `key3` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'none',
  `max_payment` double NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `laravel_banks`
--

CREATE TABLE `laravel_banks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `laravel_commissions`
--

CREATE TABLE `laravel_commissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `currency_id` int(11) NOT NULL DEFAULT '1',
  `start_from` double NOT NULL,
  `end_at` double NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agent_quota` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '50',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `laravel_currency`
--

CREATE TABLE `laravel_currency` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `origin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `origin_symbol` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `destination` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `destination_symbol` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `income_category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'commission',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `laravel_outstandings`
--

CREATE TABLE `laravel_outstandings` (
  `id` int(10) UNSIGNED NOT NULL,
  `agid` int(11) NOT NULL,
   `cid` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `agent_commission` double NOT NULL,
  `manager_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `transaction_paid` int(11) NOT NULL,
  `commission_paid` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `laravel_payments`
--

CREATE TABLE `laravel_payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `agid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
   `agent_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `manager_id` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_payment_id` int(191) NOT NULL DEFAULT '0',
  `payment_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'transaction',
  `amount` double NOT NULL,
  `fully_paid` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `laravel_payment_managers`
--

CREATE TABLE `laravel_payment_managers` (
  `id` int(10) UNSIGNED NOT NULL,
  `manager_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'remittance',
  `manager_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double NOT NULL,
  `balance` double NOT NULL,
  `total` double NOT NULL,
  `fully_paid` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `laravel_photos`
--

CREATE TABLE `laravel_photos` (
  `id` int(10) UNSIGNED NOT NULL,
  `identity_proof` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_proof` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `laravel_rates`
--

CREATE TABLE `laravel_rates` (
  `id` int(10) UNSIGNED NOT NULL,
  `rate` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `bou_rate` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `sold_rate` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `currency_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `laravel_receivers`
--

CREATE TABLE `laravel_receivers` (
  `id` int(10) UNSIGNED NOT NULL,
  `old_id_4cust` int(10) UNSIGNED NOT NULL,
 `old_id_4agent` int(10) UNSIGNED NOT NULL,
`agid` int(10) UNSIGNED NOT NULL,
`agcid` int(10) UNSIGNED NOT NULL,
`cid` int(10) UNSIGNED NOT NULL,
 `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `sender_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transfer_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `identity_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `laravel_renewals`
--

CREATE TABLE `laravel_renewals` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `next_payment` datetime NOT NULL,
  `payment-mode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `completed` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `laravel_roles`
--

CREATE TABLE `laravel_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `laravel_role_user`
--

CREATE TABLE `laravel_role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `laravel_setting_businesses`
--

CREATE TABLE `laravel_setting_businesses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slogan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abbreviation` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `activity_notice` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `laravel_setting_transactions`
--

CREATE TABLE `laravel_setting_transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `two_copies` int(11) NOT NULL DEFAULT '0',
  `allow_credit` int(11) NOT NULL DEFAULT '0',
  `allow_calculator` int(11) NOT NULL DEFAULT '0',
  `agentname_onreceipt` int(11) NOT NULL DEFAULT '0',
  `cap_transaction` int(11) NOT NULL DEFAULT '0',
  `max_transaction` double NOT NULL DEFAULT '1500',
  `allow_sms` int(11) NOT NULL DEFAULT '0',
  `sms_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sms_hash` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allow_payment` int(11) DEFAULT NULL,
  `max_payment` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `laravel_transactions`
--

CREATE TABLE `laravel_transactions` (
  `id` int(10) UNSIGNED NOT NULL,
`agid` int(10) unsigned NOT NULL,
`agcid` int(10) unsigned NOT NULL,
`agcrid` int(10) unsigned NOT NULL,
`cid` int(10) unsigned NOT NULL,
  `receipt_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `receiver_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `receiver_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `commission` double NOT NULL,
  `agent_commission` double NOT NULL,
  `exchange_rate` double NOT NULL,
  `currency_id` int(11) NOT NULL DEFAULT '1',
  `currency_income` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'commission',
  `bou_rate` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `sold_rate` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `note` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'unavailable',
  `agent_payment_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'none',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `laravel_users`
--

CREATE TABLE `laravel_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `old_id_agent` int(10) unsigned NOT NULL,
  `old_id_cust` int(10) unsigned NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
`postcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
`town` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
`county` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
`country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_id` int(11) NOT NULL DEFAULT '1',
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'customer',
  `credit` double NOT NULL DEFAULT '0',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `address_id` int(11) NOT NULL DEFAULT '0',
  `is_active` int(11) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `laravel__n2uk_banks`
--

CREATE TABLE `laravel__n2uk_banks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `laravel__n2uk_customers`
--

CREATE TABLE `laravel__n2uk_customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `custid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `laravel__n2uk_rates`
--

CREATE TABLE `laravel__n2uk_rates` (
  `id` int(10) UNSIGNED NOT NULL,
  `bou` double NOT NULL DEFAULT '0',
  `sold` double NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `laravel__n2uk_receivers`
--

CREATE TABLE `laravel__n2uk_receivers` (
  `id` int(10) UNSIGNED NOT NULL,
  `custid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transfer_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `identity_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uk_bank` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uk_account_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uk_sort_code` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ng_account_name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ng_bank` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ng_account_no` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `laravel__n2uk_transactions`
--

CREATE TABLE `laravel__n2uk_transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `custid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transfer_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `receiver_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount_pounds` double NOT NULL,
  `amount_naira` double NOT NULL,
  `bou_rate` double NOT NULL,
  `sold_rate` double NOT NULL,
  `margin` double NOT NULL,
  `note` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'unavailable',
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `authentication_log`
--
ALTER TABLE `authentication_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `authentication_log_authenticatable_type_authenticatable_id_index` (`authenticatable_type`,`authenticatable_id`);

--
-- Indexes for table `laravel_addresses`
--
ALTER TABLE `laravel_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laravel_agent_customers`
--
ALTER TABLE `laravel_agent_customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laravel_apis`
--
ALTER TABLE `laravel_apis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laravel_banks`
--
ALTER TABLE `laravel_banks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laravel_commissions`
--
ALTER TABLE `laravel_commissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `commissions_user_id_index` (`user_id`);

--
-- Indexes for table `laravel_currency`
--
ALTER TABLE `laravel_currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laravel_outstandings`
--
ALTER TABLE `laravel_outstandings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laravel_payments`
--
ALTER TABLE `laravel_payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_fully_paid_index` (`fully_paid`);

--
-- Indexes for table `laravel_payment_managers`
--
ALTER TABLE `laravel_payment_managers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laravel_photos`
--
ALTER TABLE `laravel_photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laravel_rates`
--
ALTER TABLE `laravel_rates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rates_user_id_index` (`user_id`);

--
-- Indexes for table `laravel_receivers`
--
ALTER TABLE `laravel_receivers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `receivers_user_id_index` (`user_id`);

--
-- Indexes for table `laravel_renewals`
--
ALTER TABLE `laravel_renewals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laravel_roles`
--
ALTER TABLE `laravel_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laravel_role_user`
--
ALTER TABLE `laravel_role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_user_id_index` (`user_id`),
  ADD KEY `role_user_role_id_index` (`role_id`);

--
-- Indexes for table `laravel_setting_businesses`
--
ALTER TABLE `laravel_setting_businesses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laravel_setting_transactions`
--
ALTER TABLE `laravel_setting_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laravel_transactions`
--
ALTER TABLE `laravel_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laravel_users`
--
ALTER TABLE `laravel_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `laravel__n2uk_banks`
--
ALTER TABLE `laravel__n2uk_banks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laravel__n2uk_customers`
--
ALTER TABLE `laravel__n2uk_customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laravel__n2uk_rates`
--
ALTER TABLE `laravel__n2uk_rates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `laravel_n2uk_rates_user_id_index` (`user_id`);

--
-- Indexes for table `laravel__n2uk_receivers`
--
ALTER TABLE `laravel__n2uk_receivers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laravel__n2uk_transactions`
--
ALTER TABLE `laravel__n2uk_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `authentication_log`
--
ALTER TABLE `authentication_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `laravel_addresses`
--
ALTER TABLE `laravel_addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `laravel_agent_customers`
--
ALTER TABLE `laravel_agent_customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `laravel_apis`
--
ALTER TABLE `laravel_apis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `laravel_banks`
--
ALTER TABLE `laravel_banks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `laravel_commissions`
--
ALTER TABLE `laravel_commissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `laravel_currency`
--
ALTER TABLE `laravel_currency`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `laravel_outstandings`
--
ALTER TABLE `laravel_outstandings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `laravel_payments`
--
ALTER TABLE `laravel_payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `laravel_payment_managers`
--
ALTER TABLE `laravel_payment_managers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `laravel_photos`
--
ALTER TABLE `laravel_photos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `laravel_rates`
--
ALTER TABLE `laravel_rates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `laravel_receivers`
--
ALTER TABLE `laravel_receivers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `laravel_renewals`
--
ALTER TABLE `laravel_renewals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `laravel_roles`
--
ALTER TABLE `laravel_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `laravel_role_user`
--
ALTER TABLE `laravel_role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `laravel_setting_businesses`
--
ALTER TABLE `laravel_setting_businesses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `laravel_setting_transactions`
--
ALTER TABLE `laravel_setting_transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `laravel_transactions`
--
ALTER TABLE `laravel_transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `laravel_users`
--
ALTER TABLE `laravel_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `laravel__n2uk_banks`
--
ALTER TABLE `laravel__n2uk_banks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `laravel__n2uk_customers`
--
ALTER TABLE `laravel__n2uk_customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `laravel__n2uk_rates`
--
ALTER TABLE `laravel__n2uk_rates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `laravel__n2uk_receivers`
--
ALTER TABLE `laravel__n2uk_receivers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `laravel__n2uk_transactions`
--
ALTER TABLE `laravel__n2uk_transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;


INSERT INTO `laravel_currency` (`id`, `code`, `origin`, `origin_symbol`, `destination`, `destination_symbol`, `user_id`, `income_category`, `created_at`, `updated_at`) VALUES
(1, 'UK_NG', 'United Kingdom', 'UK', 'Nigeria', 'NG', 1, 'commission', '2018-12-27 00:00:00', NULL);

UPDATE laravel_rates SET currency_id = 1;