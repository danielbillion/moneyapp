UPDATE agent_cust_transaction inner join agent_cust_receiver 
on agent_cust_transaction.r_name =
			 CONCAT(agent_cust_receiver.b_fname,' ',agent_cust_receiver.b_lname)
SET agent_cust_transaction.agcrid = agent_cust_receiver.id
 
===================================================================




INSERT INTO laravel_users(old_id_agent,name, email, password,
        fname,lname, phone,mobile,is_active,dob,title,mname,credit,type,address,postcode,town,county,country,created_at)
SELECT id,CONCAT(`fname`,'-',`lname`),email,password,fname,lname,pnumber,mnumber,active,dob,title,mname,credit,type,address,postcode,town,county,country,date_reg
FROM agent;





INSERT INTO laravel_users(old_id_customer,name, email, password,
        fname,lname, phone,mobile,is_active,dob,title,mname,credit,address,postcode,town,county,country,created_at)
SELECT id,CONCAT(`fname`,'-',`lname`),email,password,fname,lname,pnumber,mnumber,active,dob,title,mname,credit,address,postcode,town,county,country,date_reg
FROM new_customer;



update laravel_users set password='$2y$10$lN9jV10xYOBawgzzkt7Eb.9M/OXS601WmMQ1t86kfxy.KiHW.zXcW';


INSERT INTO laravel_addresses(user_id,address,postcode,town,county,country,created_at) SELECT id,address,postcode,town,county,country,created_at FROM 
	laravel_users;



INSERT INTO laravel_agent_customers(old_id,agid,agent_email,title,name,fname,mname,lname,dob,email,phone,mobile,postcode,address,town,county,country,created_at)
	SELECT id,agid,agent_email,title,CONCAT(`fname`,'-',`lname`),fname,mname,lname,dob,email,pnumber,mnumber,postcode,address,town,county,country,date_reg
FROM agent_new_customer;


INSERT INTO laravel_addresses(agent_customer_id,address,postcode,town,county,country,created_at) SELECT id,address,postcode,town,county,country,created_at FROM 
	laravel_agent_customers;



INSERT INTO laravel_receivers(old_id_4agent,agid,agcid,agent_email,
			agent_customer_email,fname,lname,phone,transfer_type,identity_type,
			bank,account_number,created_at)
	SELECT `id`,agid,agcid,agent_email,agent_cust_email,b_fname,b_lname,b_phone,
						b_transfer,b_idtype,b_abank,b_actno,`date`
FROM `agent_cust_receiver`;

UPDATE laravel_receivers set type = 'agent';


INSERT INTO laravel_receivers(old_id_4cust,cid,customer_email,fname,lname,phone,transfer_type,identity_type,bank,account_number,created_at)
	SELECT id,cid,sender_email,b_fname,b_lname,b_phone,b_transfer,b_idtype,b_abank,b_actno,date
FROM receiver;

UPDATE laravel_receivers set type = 'customer' WHERE type = '' ;




INSERT INTO laravel_rates(rate,created_at) SELECT rate1,date FROM todays_rate;

INSERT INTO laravel_commissions (`start_from`,`end_at`,`value`,`agent_quota`,`created_at`) SELECT `from`,`to`,`value`,`pa`,`date` FROM agent_cr;

INSERT INTO laravel_banks(name,status) SELECT bank,status FROM  bank;

INSERT INTO laravel_setting_businesses(name,slogan,abbreviation,email,email2,phone,mobile,address,postcode,website,created_at) 
SELECT name1, slogan1,code,email,email2,tel1,tel2,address,postcode,web1,date from busy;

INSERT INTO  laravel_setting_transactions
			(two_copies,allow_credit,allow_calculator,agentname_onreceipt,cap_transaction,max_transaction,allow_sms)
		VALUE('0','0','0','0','0','1500','0');


n2UK
==========


INSERT INTO laravel__n2uk_banks(name,type) SELECT bank,status FROM  trans2_bank;


INSERT INTO laravel__n2uk_rates(bou,sold,created_at) SELECT bou,sold,date FROM trans2_rate;




UPDATE laravel_agent_customers
	INNER JOIN laravel_users 
        ON laravel_users.old_id_agent = laravel_agent_customers.agid
SET laravel_agent_customers.user_id=laravel_users.id WHERE laravel_users.type = 'agent';

UPDATE laravel_agent_customers
	INNER JOIN laravel_users 
        ON laravel_users.email = laravel_agent_customers.agent_email
SET laravel_agent_customers.user_id=laravel_users.id WHERE laravel_users.type = 'agent';







UPDATE receivers - user_id / sender_id
===================================

UPDATE laravel_receivers 
	INNER JOIN laravel_users ON laravel_users.old_id_agent = laravel_receivers.agid
SET laravel_receivers.user_id = laravel_users.id WHERE laravel_users.type = 'agent';

UPDATE laravel_receivers 
	INNER JOIN laravel_users ON laravel_users.email = laravel_receivers.agent_email
SET laravel_receivers.user_id = laravel_users.id WHERE laravel_users.type = 'agent';




UPDATE laravel_receivers 
	INNER JOIN laravel_users ON laravel_users.old_id_customer = laravel_receivers.cid
SET laravel_receivers.user_id = laravel_users.id WHERE laravel_users.type = 'customer';

UPDATE laravel_receivers 
	INNER JOIN laravel_users ON laravel_users.email = laravel_receivers.customer_email
SET laravel_receivers.user_id = laravel_users.id WHERE laravel_users.type = 'customer';



UPDATE laravel_receivers 
	INNER JOIN laravel_users ON laravel_users.old_id_customer = laravel_receivers.cid
SET laravel_receivers.sender_id = laravel_users.id WHERE laravel_users.type = 'customer';

UPDATE laravel_receivers 
	INNER JOIN laravel_users ON laravel_users.email = laravel_receivers.customer_email
SET laravel_receivers.sender_id = laravel_users.id WHERE laravel_users.type = 'customer';


 
 Heavy Ops
==========
UPDATE laravel_receivers 
	INNER JOIN laravel_agent_customers ON laravel_agent_customers.old_id = laravel_receivers.agcid
SET laravel_receivers.sender_id = laravel_agent_customers.id WHERE laravel_receivers.type = 'agent';

UPDATE laravel_receivers 
	INNER JOIN laravel_agent_customers ON laravel_agent_customers.email = laravel_receivers.agent_customer_email
SET laravel_receivers.sender_id = laravel_agent_customers.id WHERE laravel_receivers.type = 'agent';


laravel_outstandings
================== 


INSERT INTO laravel_outstandings(agid,transaction_id,amount,agent_commission,created_at)
            SELECT agid,id,amt_send,com_a,dtime from agent_cust_transaction;

INSERT INTO laravel_outstandings(cid,transaction_id,amount,agent_commission,created_at)
            SELECT cid,id,amt_send,com_a,dtime from cust_transaction;

 UPDATE laravel_outstandings INNER JOIN 
        laravel_users on laravel_outstandings.agid = laravel_users.old_id_agent
 SET laravel_outstandings.user_id = laravel_users.id WHERE laravel_users.type = 'agent';

 UPDATE laravel_outstandings INNER JOIN 
        laravel_users on laravel_outstandings.cid = laravel_users.old_id_customer
 SET laravel_outstandings.user_id = laravel_users.id WHERE laravel_users.type = 'customer';

 UPDATE laravel_outstandings INNER JOIN agent_cust_transaction 
            ON laravel_outstandings.transaction_id =agent_cust_transaction.id
    SET transaction_paid = 1 WHERE agent_cust_transaction.clear = 'cl';
    
UPDATE laravel_outstandings INNER JOIN agent_cust_transaction 
            ON laravel_outstandings.transaction_id =agent_cust_transaction.id
    SET commission_paid = 1 WHERE agent_cust_transaction.agent_ps = 'yp';

INSERT INTO laravel_payments(agid,agent_email,amount,payment_type) SELECT id,email,sta,'transaction' from agent;
    
UPDATE laravel_payments INNER JOIN agent ON laravel_payments.agid = agent.id
         SET laravel_payments.user_id = agent.id;
 
    INSERT INTO laravel_payments(agid,agent_email,payment_type,amount,created_at) 
    SELECT agid,agent_email,js,amt_send,date from clear_trans;

 UPDATE laravel_payments INNER JOIN agent ON laravel_payments.agent_email = agent.email
         SET laravel_payments.user_id = agent.id;

UPDATE laravel_payments SET payment_type =  'commission',
fully_paid =1 WHERE payment_type =  'com_cl';
UPDATE laravel_payments SET payment_type = 'transaction',
 fully_paid = 1 where payment_type = 'CLA' || payment_type = 'CL1';

  
  
  transactions INSERT
==========================


INSERT INTO laravel_transactions(agid,agcid,agcrid,receipt_number,
			receiver_phone,amount,commission,agent_commission,exchange_rate,
			status,note,created_at)
SELECT agid,agcid,agcrid,receiptno,
r_phone,amt_send,commission,com_a,exchange_rate,status,
note,date FROM agent_cust_transaction;


UPDATE laravel_transactions SET type = 'agent';


INSERT INTO laravel_transactions(agid,agcid,agcrid,receipt_number,
			receiver_phone,amount,commission,agent_commission,exchange_rate,
			status,note,created_at)
SELECT cid,cid,crid,receiptno,
r_phone,amt_send,commission,com_a,exchange_rate,status,note,date FROM cust_transaction;



UPDATE laravel_transactions SET type = 'customer' WHERE type = '';




 transactions UPDATE
==========================

UPDATE laravel_transactions 
	INNER JOIN laravel_users ON laravel_transactions.agid = laravel_users.old_id_agent
SET laravel_transactions.user_id=laravel_users.id WHERE
 laravel_transactions.type = 'agent' && laravel_users.type ='agent';

 UPDATE laravel_transactions 
	INNER JOIN laravel_users ON laravel_transactions.agent_email = laravel_users.email
SET laravel_transactions.user_id=laravel_users.id WHERE
 laravel_transactions.type = 'agent' && laravel_users.type ='agent';


 
UPDATE laravel_transactions 
	INNER JOIN laravel_users ON laravel_transactions.cid = laravel_users.old_id_customer
SET laravel_transactions.user_id=laravel_users.id,
 laravel_transactions.sender_id=laravel_users.id
 WHERE laravel_transactions.type = 'customer' && laravel_users.type = 'customer';




Heavy
======


SET SQL_BIG_SELECTS=1;
UPDATE laravel_transactions
	INNER JOIN laravel_agent_customers ON 
		laravel_agent_customers.old_id = laravel_transactions.agcid
SET laravel_transactions.sender_id = laravel_agent_customers.id WHERE 
	laravel_transactions.type = 'agent';

	UPDATE laravel_transactions INNER JOIN laravel_receivers
		ON laravel_transactions.agcrid = laravel_receivers.old_id_4cust
	SET laravel_transactions.receiver_id = laravel_receivers.id 
	WHERE laravel_transactions.type = 'customer' && laravel_receivers.type = 'customer';



	UPDATE laravel_transactions INNER JOIN 
		laravel_receivers ON laravel_transactions.agcrid = laravel_receivers.old_id_4agent
	SET laravel_transactions.receiver_id = laravel_receivers.id 
	WHERE laravel_transactions.type = 'agent' && laravel_receivers.type = 'agent';

	

	



N2UK
==============

	INSERT laravel__n2uk_customers(custid,title,name,fname,lname,mname,dob,email,phone,mobile,postcode,address)
	SELECT custid,title,CONCAT(`fname`,'-',`lname`),fname,lname,mname,dob,email,pnumber,mnumber,postcode,address
	FROM trans2_customer;

	INSERT laravel__n2uk_receivers(custid,fname,lname,phone,transfer_type,
	identity_type,uk_bank,uk_account_no,
			uk_sort_code,ng_account_name,ng_bank,ng_account_no,created_at)
	SELECT `custid`,`b_fname`,`b_lname`,`b_phone`,`b_transfer`,`b_idtype`,
	`b_abank`,`b_actno`,`sortcode`,`act_name`,`nbank`,`nbankno`,`date` FROM `trans2_receiver`;


	INSERT laravel__n2uk_transactions
				(custid,transfer_code,
				amount_pounds,bou_rate,sold_rate,margin,status,created_at)
	SELECT custid,tcode,amt_p,bou_r,sold_r,margin,`status`,`date` FROM trans2;

UPDATES


	UPDATE laravel__n2uk_receivers INNER JOIN laravel__n2uk_customers
	ON laravel__n2uk_receivers.custid = laravel__n2uk_customers.custid
	SET laravel__n2uk_receivers.sender_id = laravel__n2uk_customers.id;

	UPDATE laravel__n2uk_transactions INNER JOIN laravel__n2uk_customers
	ON laravel__n2uk_transactions.custid = laravel__n2uk_customers.custid
	SET laravel__n2uk_transactions.sender_id = laravel__n2uk_customers.id;

---->find admin id
 UPDATE  laravel__n2uk_receivers SET `user_id` = '1'; 


 UPDATE  laravel__n2uk_transactions SET `user_id` = '1';







INSERT INTO `laravel_currency` (`id`, `code`, `origin`, `origin_symbol`, `destination`, `destination_symbol`, `user_id`, `income_category`, `created_at`, `updated_at`) VALUES
(1, 'UK_NG', 'United Kingdom', 'UK', 'Nigeria', 'NG', 1, 'commission', '2018-12-27 00:00:00', NULL);

UPDATE laravel_rates SET currency_id = 1;


Cleanups
==========
alter table laravel_users drop old_id_agent,drop old_id_customer;

ALTER TABLE `laravel_users` DROP `address`,
 DROP `postcode`, DROP `town`,DROP `county`,DROP `country`;

alter table laravel_agent_customers
    drop old_id,drop agid,drop agent_email,
	drop town,drop county,drop country;
	
	alter table laravel_outstandings
    drop agid,drop cid;
	alter table laravel_payments
    drop agent_email,drop agid;
	
	alter table laravel_receivers
    drop old_id_4agent,drop old_id_4cust,drop agid,
	drop agcid,drop cid,drop `type`,
	drop agent_email,drop agent_customer_email,drop customer_email;
	
	Alter table laravel_transactions
    drop agid,drop agcid,drop agcrid,drop cid,
	drop agent_email,drop agent_customer_email,drop customer_email;
	Alter table laravel__n2uk_customers
    drop custid;
	Alter table laravel__n2uk_receivers
    drop custid;
	Alter table laravel__n2uk_transactions
    drop custid;
